<?php defined('BASEPATH') or exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
    public $CI;
    function __construct($config = array())
    {
        parent::__construct($config);
        $this->CI =& get_instance();
    }

    function error_array()
    {
        if (count($this->_error_array) === 0)
            return FALSE;
        else
            return $this->_error_array;
    }

    public function valid_date($date) {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    public function exists($str, $value) {
        list($table, $column) = explode('.', $value, 2);    
        $str = $this->CI->db->escape($str);
        $query = $this->CI->db->query("SELECT COUNT(*) AS count FROM $table WHERE $column = $str");
        $row = $query->row();
        return ($row->count > 0) ? true : false;
    }

    // function run($module = '', $group = '') { 
    //     (is_object($module)) AND $this->CI = &$module; return parent::run($group); 
    // } 

}