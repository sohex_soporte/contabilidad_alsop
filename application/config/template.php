<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template 
| group to make active.  By default there is only one group (the 
| "default" group).
|
*/
$template['active_template'] = 'template';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land. 
|   You may also include default markup, wrappers and attributes here 
|   (though not recommended). Region keys must be translatable into variables 
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We 
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the 
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/

$template['login']['template'] = 'template/estructura_login';
$template['login']['regions'] = array(
    'header',
    'sidebar',
    'title',
    'title_info',
    'content',
    'footer'
);
$template['login']['parser'] = 'parser';
$template['login']['parser_method'] = 'parse';
$template['login']['parse_template'] = FALSE;

/*
|--------------------------------------------------------------------------
| Principal Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/

$template['template']['template'] = 'template/estructura';
$template['template']['regions'] = array(
    'header',
    'sidebar',
    'title',
    'content',
    'footer'
);
$template['template']['parser'] = 'parser';
$template['template']['parser_method'] = 'parse';
$template['template']['parse_template'] = FALSE;

/*
|--------------------------------------------------------------------------
| Template Configuration Not Sidebar (adjust this or create your own)
|--------------------------------------------------------------------------
*/

$template['template_iframe']['template'] = 'template/estructura_iframe';
$template['template_iframe']['regions'] = array(
    'header',
    'content',
    'footer'
);
$template['template_iframe']['parser'] = 'parser';
$template['template_iframe']['parser_method'] = 'parse';
$template['template_iframe']['parse_template'] = FALSE;

/* End of file template.php */
/* Location: ./system/application/config/template.php */