<?php defined('BASEPATH') or exit('No direct script access allowed');

#------------------------------------------------------------
# PRODUCCION
#------------------------------------------------------------
$config['produccion'] = array(
    'desarrollo'    => false,
    'host'          => 'https://iplaneacion.com.mx/dms/contabilidad_queretaro/',

    'bd_hostname'   => '35.227.134.110',
    'bd_database'   => 'contabilidad_queretaro',
    'bd_user'       => 'root',
    'bd_password'   => 'Passw0rd$$'
);



#------------------------------------------------------------
# DESARROLLO
#------------------------------------------------------------
$config['desarrollo'] = array(
    'desarrollo'    => true,
    'host'          => 'http://localhost/contabilidad_queretaro/',

    'bd_hostname'   => '35.227.134.110',
    'bd_database'   => 'contabilidad_general',
    'bd_user'       => 'root',
    'bd_password'   => 'Passw0rd$$'
);

return (object) $config;

