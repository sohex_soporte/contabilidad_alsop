<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuentas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
        $crud = new grocery_CRUD();
        //$crud->set_theme('tablestrap');
        // $crud->setTable('tblItems');
        // $crud->setSubject('Item', 'Items');

        $crud->set_table('cuentas_dms');
        $crud->set_theme('datatables');

        $crud->callback_delete(array($this,'delete_row'));
        $crud->where('deleted_at is null', null,false);
        $crud->columns(['cuenta','decripcion']);
        $crud->display_as("decripcion", "Descripción");

        $crud->unset_jquery();
        $crud->unset_jquery_ui();
        $crud->unset_bootstrap();
        $crud->unset_print();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_clone();
        
        //$crud->columns(['id','fecha', 'rencia', 'nombre', 'concepto', 'verificado', 'debe', 'haber', 'saldo']);
        
		$output = $crud->render();
        $this->render($output);
	}

    public function delete_row($primary_key)
    {
        return $this->db->update('cuentas_dms',array('deleted_at' => utils::now() ),array('id' => $primary_key));
    }

    public function render($output){ 
        // $this->load->helper(array('form', 'html', 'validation', 'url'));
        // utils::pre($output);
        $output = array(
            'crud' => (array)$output
        );
        $this->blade->render('/cuentas/render',$output);
    }
}
