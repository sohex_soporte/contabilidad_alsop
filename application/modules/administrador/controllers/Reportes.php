<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Reportes extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->blade->render('reportes/index', false);
  }

  public function contenido()
  {
    $this->load->model('CaReportes_model');
    $this->load->model('CaCuentas_model');
    $reporte_id = $this->input->get('reporte_id');
    if (!$reporte_id) {
      redirect('administrador/reportes/index');
    }
    
    $data['datos'] = $this->CaReportes_model->get(['id' => $reporte_id]);
    $this->blade->render('reportes/contenido', $data);
  }
}
