@layout('layout')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    BALANZA COMPROBACIÓN
</h4>

<?php if(is_array($balanzas)){ ?>
<?php foreach($balanzas as $balanza){ ?>
<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

                <h4 class="card-title "><b>A&Ntilde;O <?php echo $balanza['anio']; ?></b></h4>
                <p class="card-text">
                    <div class="table-responsive">
                        <table class="table table-bordered ">
                            <tbody>
                                <tr class="">
                                    <?php if(is_array($balanza)){ ?>
                                    <?php foreach($balanza['mes'] as $balanza_mes){ ?>
                                    <td>
                                        <div class="row">
                                            <div class="col-12">
                                                <center>
                                                    <h5><b><?php echo $balanza_mes['letra']; ?></b></h5>
                                                </center>
                                            </div>
                                            <div class="col-12">
                                                <center>
                                                    <a class="btn btn-primary btn-sm btn-block mb-2"
                                                        href="<?php echo site_url('balanza_comprobacion/visor_simple?mes='.base64_encode($balanza_mes['mes']).'&anio='.base64_encode($balanza['anio'])); ?>"
                                                        class="card-link">
                                                        <i class="fas fa-list-ul"></i> Ver Balanza Simple
                                                    </a>

                                                    <a class="btn btn-secondary btn-sm btn-block"
                                                        href="<?php echo site_url('balanza_comprobacion/visor?mes='.base64_encode($balanza_mes['mes']).'&anio='.base64_encode($balanza['anio'])); ?>"
                                                        class="card-link">
                                                        <i class="fas fa-table"></i> Ver Balanza Completa
                                                    </a>
                                                </center>
                                            </div>
                                        </div>

                                    </td>
                                    <?php } ?>
                                    <?php } ?>
                                </tr>
                                <tr>

                            </tbody>
                        </table>
                    </div>
                </p>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php } ?>

@endsection

@section('style')
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }
</style>
@endsection