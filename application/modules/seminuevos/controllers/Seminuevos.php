<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Seminuevos extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('SeminuevosModel', 'ms', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');
  }
  public function reporte()
  {

    #parche quien saque que queria hacer pita
    $data_process = [];
    for ($i = 1; $i <= 12; $i++) {
      $data_process[$i] = array();
    }

    $data = $this->ms->getAllData();
    
    foreach ($data as $d => $dato) {
      $data_process[$dato->mes][] = $dato;
    }
    // debug_var($data_process);
    // die();

    $data['nombres'] = [
      '1' => 'No. UNIDS VEND.',
      '2' => 'TOTAL DE VENTAS',
      '3' => 'COSTO DE VENTAS',
      '4' => 'UTILIDAD BRUTA',
      '5' => '% ÚTIL. BRUTA.',
      '6' => 'GASTOS DE VENTA:',
      '7' => 'SUELDO Y COMISION GERENTE',
      '8' => 'SUELDOS OTROS',
      '9' => 'COMISIONES',
      '10' => 'PRESTACIONES',
      '11' => 'IMSS, SAR E INFONAVIT',
      '12' => '2% SOBRE NOMINA',
      '13' => 'PUBLICIDAD Y PROMOCION',
      '14' => 'CORTESIAS',
      '15' => 'ACONDICIONAMIENTO',
      '16' => 'PERMISOS Y GESTORIA',
      '17' => 'GASTOS VARIOS',
      '18' => 'TOTAL GASTOS DE VENTA',
      '19' => '% SOBRE VENTAS',
      '20' => 'UTILIDAD DEPARTAMENTAL',
      '21' => '% SOBRE VENTAS',
    ];

    $array_complete = [];
    //UNIDADES VENDIDAS
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['1'][$i] = count($data_process[$i]);
    }
    //TOTAL VENTAS
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['2'][$i] = $this->getVentas($data_process, $i);
    }
    //COSTO VENTAS
    
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['3'][$i] = $this->getCostoVentas($data_process, $i);
    }
    //UTILIDAD BRUTA
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['4'][$i] = (float)$array_complete['2'][$i] - (float)$array_complete['3'][$i];
    }
    //debug_var($array_complete);die();
    //% ÚTIL. BRUTA
    for ($i = 1; $i <= 12; $i++) {
      if((float)$array_complete['2'][$i]==0){
        $array_complete['5'][$i] = '0%';
      }else{
        $porcentaje = ((float)($array_complete['4'][$i]*100)/(float)$array_complete['2'][$i])/100;
        $array_complete['5'][$i] = $porcentaje.'%';
      }
    }
    //GASTOS DE VENTA
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['6'][$i] = 0;
    }
    //SUELDO Y COMISION GERENTE
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['7'][$i] = 0;
    }
    //SUELDOS OTROS
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['8'][$i] = 0;
    }
    //COMISIONES
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['9'][$i] = 0;
    }
    //PRESTACIONES
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['10'][$i] = 0;
    }
    //IMSS, SAR E INFONAVIT
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['11'][$i] = 0;
    }
    //2% SOBRE NOMINA
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['12'][$i] = 0;
    }
    //PUBLICIDAD Y PROMOCION
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['13'][$i] = 0;
    }
    //CORTESIAS
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['14'][$i] = 0;
    }
    //ACONDICIONAMIENTO
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['15'][$i] = 0;
    }
    //PERMISOS Y GESTORIA
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['16'][$i] = 0;
    }
    //GASTOS VARIOS
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['17'][$i] = 0;
    }
    //TOTAL GASTOS DE VENTA
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['18'][$i] = 0;
    }
    //% SOBRE VENTAS
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['19'][$i] = 0;
    }
    //UTILIDAD DEPARTAMENTAL
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['20'][$i] = 0;
    }
    //% VENTAS
    for ($i = 1; $i <= 12; $i++) {
      $array_complete['21'][$i] = 0;
    }
    
    $data['datos'] = $array_complete;
    $this->blade->render('reporte', $data, FALSE);
  }
  public function getVentas($info, $mes)
  {
    $suma = 0;
    foreach ($info[$mes] as $i => $datos) {
      $suma = $suma + (float)$datos->abono;
    }
    return $suma;
  }
  public function getCostoVentas($info, $mes)
  {
    $suma = 0;
    foreach ($info[$mes] as $i => $datos) {
      $suma = $suma + (float)$datos->abono;
    }
    return $suma;
  }
}
