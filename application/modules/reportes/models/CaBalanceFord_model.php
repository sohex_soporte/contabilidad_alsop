<?php

class CaBalanceFord_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getList(){
        $this->db->distinct();
        $this->db->select('*');
        $this->db->from('ca_balance_ford');
        $this->db->order_by('id','asc');

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
}