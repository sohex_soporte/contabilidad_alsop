<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reporte_generico extends MY_Controller
{

    public $reporte_id;

    public function __construct()
    {
        parent::__construct();
        $this->reporte_id = '';
    }

    public function obtener_valor($mes, $anio, $cuenta_id, $listado_balanza)
    {
        $monto = 0;
        if (is_array($listado_balanza) && count($listado_balanza) > 0) {
            foreach ($listado_balanza as $value_balanza) {

                if ($value_balanza['cuenta_id'] == $cuenta_id && $value_balanza['mes'] == utils::folio($mes, 2) && $value_balanza['anio'] == $anio) {
                    $monto = (float)$value_balanza['saldo_mes'];
                }
            }
        }
        return $monto;
    }

    public function generacion_reporte()
    {
        $anio = date('Y');
        $this->construir_reporte($anio);
    }

    public function construir_reporte($anio)
    {

        $listado_balanza = $this->MaBalanza_model->get_list([
            'ma_balanza.anio' => $anio
        ]);

        $reglones = $this->DeReporteReglones_model->get_list([
            'reporte_id' => $this->reporte_id
        ]);

        $this->DeReporteContenido_model->delete([
            'reporte_id' => $this->reporte_id
        ]);

        if (is_array($reglones) && count($reglones) > 0) {
            foreach ($reglones as $key_reglon => $value_reglon) {

                $listado_formulas = $this->DeReglonesFormulas_model->get_list([
                    'reporte_reglon_id' => $value_reglon['id']
                ]);

                $monto_total = 0;

                for ($mes = 1; $mes <= 12; $mes++) {

                    $cuenta_monto = 0;

                    if (is_array($listado_formulas) && count($listado_formulas) > 0) {

                        foreach ($listado_formulas as $key_formula => $value_formula) {

                            $valor_celda = $this->obtener_valor($mes, $anio, $value_formula['cuenta_id'], $listado_balanza);

                            try {
                                switch ($value_formula['tipo_operacion']) {
                                    case '+':
                                        $cuenta_monto = $cuenta_monto + $valor_celda;
                                        break;
                                    case '-':
                                        $cuenta_monto = $cuenta_monto - $valor_celda;
                                        break;
                                    case '/':
                                        $cuenta_monto = $cuenta_monto / $valor_celda;
                                        break;
                                    case '*':
                                        $cuenta_monto = $cuenta_monto * $valor_celda;
                                        break;
                                    default:
                                        $cuenta_monto = $cuenta_monto + $valor_celda;
                                        break;
                                }
                            } catch (ErrorException $e) {
                                $cuenta_monto = 0;
                            }
                        }
                    }
                    $monto_total = $monto_total + $cuenta_monto;
                    $columnas[$mes] = $cuenta_monto;
                }
                $columnas[] = $monto_total;

                $this->DeReporteContenido_model->insert([
                    'reporte_id' => $this->reporte_id,
                    'anio' => $anio,
                    'reporte_reglon_id' => $value_reglon['id'],
                    'enero' => array_key_exists(1, $columnas) ? $columnas[1] : null,
                    'febrero' => array_key_exists(2, $columnas) ? $columnas[2] : null,
                    'marzo' => array_key_exists(3, $columnas) ? $columnas[3] : null,
                    'abril' => array_key_exists(4, $columnas) ? $columnas[4] : null,
                    'mayo' => array_key_exists(5, $columnas) ? $columnas[5] : null,
                    'junio' => array_key_exists(6, $columnas) ? $columnas[6] : null,
                    'julio' => array_key_exists(7, $columnas) ? $columnas[7] : null,
                    'agosto' => array_key_exists(8, $columnas) ? $columnas[8] : null,
                    'septiembre' => array_key_exists(9, $columnas) ? $columnas[9] : null,
                    'octubre' => array_key_exists(10, $columnas) ? $columnas[10] : null,
                    'noviembre' => array_key_exists(11, $columnas) ? $columnas[11] : null,
                    'diciembre' => array_key_exists(12, $columnas) ? $columnas[12] : null,
                    'total' => $monto_total
                ]);
            }

            // $this->calcular_utilidad_bruta($anio);
            // $this->calcular_utilidad_bruta_real($anio);
            // $this->calcular_porcentaje_utilidad_bruta($anio);
            // $this->total_gastos_ventas($anio);
            // $this->total_gastos_ventas_porcentaje($anio);
            // $this->calcular_utilidad_departamental($anio);
            // $this->calcular_utilidad_departamental_porcentaje($anio);
        }
    }

    public function get_contenido_reporte()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('anio', 'Año', 'trim|required');
        $this->form_validation->set_rules('reporte_id', 'reporte_id', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {
            $this->load->model('MaBalanza_model');
            $this->load->model('DeReporteReglones_model');
            $this->load->model('DeReglonesFormulas_model');
            $this->load->model('DeReporteContenido_model');

            $anio = $this->input->post('anio');
            $this->reporte_id = base64_decode($this->input->post('reporte_id'));
            $this->construir_reporte($anio);

            $this->load->model('DeReporteContenido_model');
            $listado = $this->DeReporteContenido_model->get_contenido([
                'de_reporte_contenido.reporte_id' => $this->reporte_id,
                'de_reporte_contenido.anio' => $anio
            ]);

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                '', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE', 'TOTAL'
            ));

            if (is_array($listado) && count($listado) > 0) {
                foreach ($listado as $value_listado) {

                    $clase = '';
                    if ($value_listado['tipo'] == 3) {
                        $clase = 'table-primary ';
                    } else if ($value_listado['tipo'] == 2) {
                        $clase = '';
                    }

                    if ($value_listado['tipo'] == 3) {
                        $cell_0 = array('data' => $value_listado['nombre'], 'class' => $clase, 'colspan' => 14);
                        $this->table->add_row([
                            $cell_0
                        ]);
                    } else {
                        $cell_0 = array('data' => $value_listado['nombre'], 'class' => $clase);
                        $cell_1 = array('data' => utils::format($value_listado['enero'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_2 = array('data' => utils::format($value_listado['febrero'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_3 = array('data' => utils::format($value_listado['marzo'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_4 = array('data' => utils::format($value_listado['abril'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_5 = array('data' => utils::format($value_listado['mayo'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_6 = array('data' => utils::format($value_listado['junio'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_7 = array('data' => utils::format($value_listado['julio'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_8 = array('data' => utils::format($value_listado['agosto'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_9 = array('data' => utils::format($value_listado['septiembre'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_10 = array('data' => utils::format($value_listado['octubre'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_11 = array('data' => utils::format($value_listado['noviembre'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_12 = array('data' => utils::format($value_listado['diciembre'], $value_listado['formato']), 'class' => $clase . ' text-center');
                        $cell_13 = array('data' => utils::format($value_listado['total'], $value_listado['formato']), 'class' => $clase . ' text-center');

                        $this->table->add_row([
                            $cell_0,
                            $cell_1,
                            $cell_2,
                            $cell_3,
                            $cell_4,
                            $cell_5,
                            $cell_6,
                            $cell_7,
                            $cell_8,
                            $cell_9,
                            $cell_10,
                            $cell_11,
                            $cell_12,
                            $cell_13
                        ]);
                    }
                }
            }

            $this->contenido->data = array(
                'tabla' => base64_encode(utf8_decode($this->table->generate()))
            );
        } else {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }

}
