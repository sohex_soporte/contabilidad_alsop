<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apis_generar_contenido extends MY_Controller
{

    public $reporte_id;

    public $listado_balanza = array();
    public $anio;
    public $valor_reglon = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function obtener_valor($mes,$cuenta_id,$listado_balanza){
        $monto = 0;
        if(is_array($listado_balanza) && count($listado_balanza)>0){
            foreach ($listado_balanza as $value_balanza) {
                
                if($value_balanza['cuenta_id'] == $cuenta_id && $value_balanza['mes'] == utils::folio($mes,2) && $value_balanza['anio'] == $this->anio ){
                    $monto = (float)$value_balanza['saldo_mes'];
                }
            }
        }else{
            $reglon = $this->MaBalanza_model->get_list([
                'ma_balanza.anio' => $this->anio,
                'ma_balanza.mes' => utils::folio($mes,2),
                'ma_balanza.cuenta_id' => $cuenta_id,

            ]);
            if(is_array($reglon) && array_key_exists('saldo_mes',$reglon)){
                $monto = $reglon['saldo_mes'];
            }
        }
        return $monto;
    }

    public function obtener_valor_reglon($mes,$reporte_reglon_id){
        
        $valor = 0;
        $mes_nombres = array (1=>'enero',2=>'febrero',3=>'marzo',4=>'abril',5=>'mayo',6=>'junio',7=>'julio',8=>'agosto',9=>'septiembre',10=>'octubre',11=>'noviembre',12=>'diciembre');
        
        if(is_array($this->valor_reglon) && array_key_exists($reporte_reglon_id,$this->valor_reglon)){
            $reglon = $this->valor_reglon[$reporte_reglon_id];

            if(is_array($reglon) && count($reglon)>0){
                $mes_act = $mes_nombres[(int)$mes];
                foreach ($reglon as $key => $value) {
                    if($key == $mes_act){
                        $valor = $value;
                    }
                }
            }
        }else{
            $reglon = $this->DeReporteContenido_model->get([
                'de_reporte_contenido.anio' => $this->anio,
                'de_reporte_contenido.reporte_reglon_id' => $reporte_reglon_id,
            ]);

            if(is_array($reglon) && count($reglon)>0){
                $mes_act = $mes_nombres[(int)$mes];
                $this->valor_reglon[$reporte_reglon_id] = $reglon;
                foreach ($reglon as $key => $value) {
                    if($key == $mes_act){
                        $valor = $value;
                    }
                }
            }
        }

        return $valor;
    }

    public function construir_reporte(){

        $this->reporte_id = $this->input->get_post('reporte_id');
        $this->anio = $this->input->get_post('anio');
        
        $this->load->model('MaBalanza_model');
        $this->listado_balanza = $this->MaBalanza_model->get_list([
            'ma_balanza.anio' => $this->anio
        ]);
        

        $this->load->model('DeReporteReglones_model');
        $reglones = $this->DeReporteReglones_model->get_list([
            'reporte_id' => $this->reporte_id
        ]);

        $this->load->model('DeReporteContenido_model');
        $this->DeReporteContenido_model->delete([
            'reporte_id' => $this->reporte_id
        ]);

        $this->load->model('DeReglonesFormulas_model');
        
        $procesar_posterior = array();
        $procesing = 0;
        $procesing_reglones = 0;

        if(is_array($reglones) && count($reglones)>0){
            foreach ($reglones as $key_reglon => $value_reglon) {

                $listado_formulas = $this->DeReglonesFormulas_model->get_list([
                    'reporte_reglon_id' => $value_reglon['id'],
                    'tipo_calculo' => 'CALCULO_SOBRE_REGLONES'
                ]);

                if( is_array($listado_formulas) && count($listado_formulas)>0 ){
                    $procesar_posterior[] = $value_reglon;
                }else{
                    $listado_formulas = $this->DeReglonesFormulas_model->get_list([
                        'reporte_reglon_id' => $value_reglon['id'],
                    ]);
                    $columnas = $this->recorrido_meses($listado_formulas);
                    $this->agregar_reglon($columnas,$value_reglon['id']);
                    $procesing ++;
                }
               
            }
        }

        if(is_array($procesar_posterior) && count($procesar_posterior)>0){
            $this->listado_balanza = false;
            foreach ($procesar_posterior as $key_reglon => $value_reglon) {
                $listado_formulas = $this->DeReglonesFormulas_model->get_list([
                    'reporte_reglon_id' => $value_reglon['id'],
                ]);
                $columnas = $this->recorrido_meses($listado_formulas);

                $this->agregar_reglon($columnas,$value_reglon['id']);
                $procesing_reglones ++;
            }
        }

        $response = array(
            'status' => 'OK',
            'procesing' => $procesing,
            'procesing_reglones' => $procesing_reglones
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
        exit;
    }

    public function agregar_reglon($columnas,$reglon_id){
        return $this->DeReporteContenido_model->insert([
            'reporte_id' => $this->reporte_id,
            'anio' => $this->anio,
            'reporte_reglon_id' => $reglon_id,
            'enero' => array_key_exists(1,$columnas)? $columnas[1] : null,
            'febrero' => array_key_exists(2,$columnas)? $columnas[2] : null,
            'marzo' => array_key_exists(3,$columnas)? $columnas[3] : null,
            'abril' => array_key_exists(4,$columnas)? $columnas[4] : null,
            'mayo' => array_key_exists(5,$columnas)? $columnas[5] : null,
            'junio' => array_key_exists(6,$columnas)? $columnas[6] : null,
            'julio' => array_key_exists(7,$columnas)? $columnas[7] : null,
            'agosto' => array_key_exists(8,$columnas)? $columnas[8] : null,
            'septiembre' => array_key_exists(9,$columnas)? $columnas[9] : null,
            'octubre' => array_key_exists(10,$columnas)? $columnas[10] : null,
            'noviembre' => array_key_exists(11,$columnas)? $columnas[11] : null,
            'diciembre' => array_key_exists(12,$columnas)? $columnas[12] : null,
            'total' => (array_key_exists(13,$columnas)? $columnas[13] : null)
        ]);
    }

    public function recorrido_meses($listado_formulas){
        $monto_total = 0;
        for ($mes=1; $mes <= 12 ; $mes++) { 
            $cuenta_monto = $this->calculo_mes($listado_formulas,$mes);
            
            $monto_total = $monto_total + $cuenta_monto;
            $columnas[$mes] = $cuenta_monto;
        }
        $columnas[] = $monto_total;
        return $columnas;
    }

    public function calculo_mes($listado_formulas,$mes){
        $cuenta_monto = 0;
        
        if(is_array($listado_formulas) && count($listado_formulas)>0){
                  
            foreach ($listado_formulas as $value_formula) {
               
               if($value_formula['tipo_calculo'] == 'CALCULO_SOBRE_REGLONES'){
                    $valor_celda = $this->obtener_valor_reglon($mes,$value_formula['reporte_reglon_id_ref']);
               }else{
                    $valor_celda = $this->obtener_valor($mes,$value_formula['cuenta_id'],$this->listado_balanza);
               }
                
                try {
                    switch ($value_formula['tipo_operacion']) {
                        case '+':
                            $cuenta_monto = $cuenta_monto + $valor_celda;
                            break;
                        case '-':
                            $cuenta_monto = $cuenta_monto - $valor_celda;
                            break;
                        case '/':
                            $cuenta_monto = $cuenta_monto / $valor_celda;
                            break;
                        case '*':
                            $cuenta_monto = $cuenta_monto * $valor_celda;
                            break;
                        default: 
                            $cuenta_monto = $cuenta_monto + $valor_celda;
                            break;
                    }            
                }catch(ErrorException $e){
                    $cuenta_monto = 0;
                }
            }
        }
        return $cuenta_monto;
    }

    private function calcular_utilidad_bruta($anio){
        
        $this->load->model('DeReporteContenido_model');

        #TOTAL VEHICULOS
        $total_vehiculos = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 1
        ]);

        #COSTO DE VENTAS
        $costos_ventas = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 5
        ]);
        if(is_array($costos_ventas) && is_array($total_vehiculos)){

            $content = [];
            foreach ($costos_ventas as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total_vehiculos[$key] - $costos_ventas[$key];
                }
            }

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 3,
            ]);
        }
    }

    private function calcular_utilidad_bruta_real($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $total = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 3
        ]);

        if(is_array($total)){

            $content = [];
            foreach ($total as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total[$key];
                }
            }

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 4,
            ]);
        }
    }


    private function calcular_porcentaje_utilidad_bruta($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $utilidad_real = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 4
        ]);

         #TOTAL VENTAS VEHICULOS
         $total_ventas = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 1
        ]);

        if(is_array($utilidad_real) && is_array($total_ventas)){

            $content = $this->calcular_procentaje($utilidad_real,$total_ventas);

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 31,
            ]);
        }
    }   

    public function total_gastos_ventas($anio){

        #TOTAL GASTOS DE VENTA
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $this->db->where("reporte_reglon_id BETWEEN 7 AND 26");
        $total = $this->DeReporteContenido_model->get_contenido_suma([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio
        ]);

        if(is_array($total)){

            $content = [];
            foreach ($total as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total[$key];
                }
            }

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 27,
            ]);
        }
    }

    public function total_gastos_ventas_porcentaje($anio){

        $this->load->model('DeReporteContenido_model');

        #TOTAL GASTOS DE VENTA
        $total_gastos = $this->DeReporteContenido_model->get_contenido_suma([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 27
        ]);

        #TOTAL VENTAS VEHICULOS
        $total_ventas = $this->DeReporteContenido_model->get_contenido_suma([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 1
        ]);



        if(is_array($total_ventas) && is_array($total_gastos)){

            $content = $this->calcular_procentaje($total_gastos,$total_ventas);

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 28,
            ]);
        }
    }

    private function calcular_utilidad_departamental($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $total_superior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 4
        ]);
        
        #TOTAL GASTOS DE VENTA
        $total_inferior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 27
        ]);
        if(is_array($total_inferior) && is_array($total_superior)){

            $content = [];
            foreach ($total_inferior as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total_superior[$key] - $total_inferior[$key];
                }
            }

            # UTILIDAD DEPARTAMENTAL
            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 29,
            ]);
        }
    }

    private function calcular_utilidad_departamental_porcentaje($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $total_superior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 4
        ]);
        
        #TOTAL GASTOS DE VENTA
        $total_inferior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 29
        ]);
        if(is_array($total_inferior) && is_array($total_superior)){

            $content = $this->calcular_procentaje($total_inferior,$total_superior);

            # UTILIDAD DEPARTAMENTAL
            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 30,
            ]);
        }
    }

    private function calcular_procentaje($total_inferior,$total_superior){
        $content = [];
        if(is_array($total_inferior) && is_array($total_superior)){

            foreach ($total_inferior as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    try {
                        $content[$key] = 0;

                        $negativo = false;
                        if($total_superior[$key] < 0 && $total_inferior[$key] < 0){
                            $negativo = true;
                        }

                        $val_1 = $total_superior[$key] * 100;
                        if($val_1 <> 0 && $total_inferior[$key] <> 0){
                            $val_1 = $val_1 / $total_inferior[$key];
                            if($val_1 <> 0){
                                $content[$key] = ($negativo == true && ($val_1/100) > 0 )? $val_1/100*-1 : $val_1/100;
                            }
                        }
                        
                    }catch(ErrorException $e){
                        $content[$key] = 0;
                    }
                }
            }
        }
        return $content;
    }

}
