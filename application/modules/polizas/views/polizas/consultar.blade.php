@layout('layout')
@section('style')

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>

@endsection
@section('contenido')

<div class="row mb-2 mt-2">
    <div class="col-sm-12">
        <h3>Datos de la poliza</h3>
    </div>
</div>

<div class="card">
  <div class="card-body">
    <h6 class="card-subtitle mb-2 text-muted">Detalle</h6>
    
    <div class="row mt-4">
        <div class="col-sm-12">
            <form>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Clave:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo $listado_polizas['PolizaNomenclatura_id'].'-'.$listado_polizas['dia']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Concepto:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo $listado_polizas['nomenclatura']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Fecha:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo  substr($listado_polizas['dia'], -2, 2).'/'.$listado_polizas['mes'].'/'.$listado_polizas['ejercicio']; ?>">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr/>
    <h6 class="card-subtitle mb-2 text-muted mt-4 mb-2">Asientos</h6>

    <div class="row">
        <div class="col-sm-12">
            <?php echo $tabla_content; ?>
        </div>
    </div>


  </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <a type="button" href="<?php echo site_url('polizas'); ?>" class="btn btn-secondary mt-4 mb-3"><i class="fa fa-arrow-left"></i> Regresar</a>
    </div>
</div>

@endsection

@section('script')
<script src="{{ base_url('assets/components/DataTables/datatables.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/polizas/polizas/index.js'); ?>"></script>
@endsection