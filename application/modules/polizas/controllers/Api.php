<?php defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    public $type = 'api';

    public $http_status = 200;
    public $data;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->data = array(
            'data' => false,
            'message' => false
        );
    }

    public function nomenclaturas_get()
    {
        // $this->load->model('CaPolizasNomenclaturas_mdl');
        // $this->data['data'] = CaPolizasNomenclaturas_mdl::all();
        // $this->response( $this->data , 201 );

        $this->load->model('CaPolizasNomenclaturas_model');
        $this->data['data'] =  $this->CaPolizasNomenclaturas_model->getAll();

        $this->response( $this->data );
        
    }



    public function polizas_get()
    {
        // $this->load->model('CaPolizasNomenclaturas_mdl');
        // $this->data['data'] = CaPolizasNomenclaturas_mdl::all();
        // $this->response( $this->data , 201 );

        $anio = $this->input->get('anio');
        $mes = $this->input->get('mes');
        $dia = $this->input->get('dia');

        $this->load->model('CaPolizas_model');
        $this->data['data'] = $this->CaPolizas_model->getList([
            'ejercicio' => $anio,
            'mes' => $mes,
            'dia' => $dia
        ]);

        $this->response( $this->data);
        
    }
}