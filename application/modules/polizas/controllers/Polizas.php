<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Polizas extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
       
    }

    public function index(){
        $this->blade->render('/polizas/index');
    }

    // public function guarda(){

    //     $this->form_validation->set_rules('fecha', 'fecha', 'required');
    //     $this->form_validation->set_rules('poliza', 'poliza', 'required');
    //     $this->form_validation->set_rules('concepto', 'concepto', 'required');
  
    //     $response = validate($this);
  
  
    //     if($response['status']){
    //       $data['fecha'] = $this->input->post('fecha');
    //       $data['poliza'] = $this->input->post('poliza');
    //       $data['concepto'] = $this->input->post('concepto');
    //       $data['fecha_creacion'] = date('Y-m-d H:i:s');
    //       $data['id_id'] = get_guid();
    //       $data['folio'] = "0";
    //       $data['cargo'] = "0";
    //       $data['abono'] = "0";
  
    //       $this->Mgeneral->save_register('polizas', $data);
    //     }
    //   //  echo $response;
    //   echo json_encode(array('output' => $response));
    // }

    public function consultar($id){

        $this->load->model('CaPolizas_model');
        $listado_polizas = $this->data =  $this->CaPolizas_model->getDetalle([
            'ca_polizas.id' => $id
        ]);

        $this->load->model('Asientos_model');
        $listado_asientos = $this->data =  $this->Asientos_model->get_detalle([
            'poliza_id' => $id
        ]);

        # FORMATEADO DE TABLA
        $this->load->library('table');

        $template = array(
            'table_open'            => '<table class="table table-striped">'
        );
        $this->table->set_template($template);

        $this->table->set_heading(array('No. asiento', 'Concepto', 'Cuenta','Departamento','Abono','Cargo'));

        $monto_abono = 0;
        $monto_cargo = 0;
        foreach ($listado_asientos as $key => $value) {
            $monto_abono = $monto_abono + $value['abono'];
            $monto_cargo = $monto_cargo + $value['cargo'];

            $this->table->add_row([
                '<b>'.utils::folio($value['asiento_id'],6).'</b>',
                $value['concepto'],
                $value['cuenta'].'<br/><small><b>'.$value['cuenta_descripcion'].'</b></small>',
                $value['departamento_descripcion'],
                utils::format($value['abono']),
                utils::format($value['cargo'])
            ]);
        }

        $cell = array('data' => 'Total',  'colspan' => 4);
        $this->table->add_row($cell, utils::format($monto_abono), utils::format($monto_cargo));

        $tabla = $this->table->generate();


        $data = [
            'listado_polizas' => $listado_polizas,
            'listado_asientos' => $listado_asientos,
            'tabla_content' => $tabla
        ];

        $this->blade->render('/polizas/consultar',$data);
        
    }

    
  
}