@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    <?php echo $title; ?>
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="folio" class="col-form-label">Folio:</label>
                            <input type="number" class="form-control" name="folio" id="folio" value="">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="nomenclatura" class="col-form-label">Nomenclatura:</label>
                            <select class="form-control" id="nomenclatura" name="nomenclatura">
                                <?php if(is_array($nomenclaturas) && count($nomenclaturas)>0){ ?>
                                <option value="">Todos</option>
                                <?php foreach($nomenclaturas as $nomenclatura){ ?>
                                <option value="<?php echo $nomenclatura['id']; ?>">
                                    <?php echo '['.$nomenclatura['id'].'] '.$nomenclatura['descripcion']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="fecha" class="col-form-label">Fecha</label>
                            <input type="date" class="form-control" id="fecha" name="fecha"
                                value="<?php echo utils::get_date(); ?>" max="<?php echo utils::get_date(); ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-sm-12">
                        <button class="btn btn-primary ml-2 my-2 my-sm-0 float-right" onclick="APP.buscar_tabla();"
                            type="button"><i class="fas fa-search"></i> Buscar</button>
                        <button class="btn btn-secondary ml-2 my-2 my-sm-0 float-right" onclick="buscar();"
                            type="button"> Limpiar</button>
                    </div>
                </div>

                <hr />
                <div class="row">
                    <div class=" col-sm-12">
                        <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<?php $this->carabiner->display('datatables','js') ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" ></script>
<script src="<?php echo base_url('assets/js/scripts/asientos/asientos/index.js'); ?>"></script>
@endsection