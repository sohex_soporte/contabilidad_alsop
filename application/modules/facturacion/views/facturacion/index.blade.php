@layout('layout')
@section('style')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/datatables.min.css') }}" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('contenido')
<h1 class="h3 mb-4 text-gray-800">Facturas</h1>
<div class="card">
  <div class="card-body">
    <form action="#!" method="get" id="consultar">
            <div class="row">
            
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Año</label>
                        <input type="date" class="form-control" name="fecha" min="2021-01-01" value="<?php echo utils::get_date(); ?>" max="<?php echo utils::get_date(); ?>">
                    </div>
                </div>
                
            </div>
        </form>
        <div class="row">
            <div class=" col-sm-12">
                <button onclick="APP.buscar_tabla();" class="btn btn-primary ml-2 my-2 my-sm-0 float-right" 
                    type="submit"><i class="fas fa-search"></i> Consultar</button>
            </div>
        </div>

        <hr />
        <div class="row">
            <div class=" col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped  table-bordered" id="tabla_listado">
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('style')

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<script src="{{ base_url('assets/components/DataTables/datatables.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/facturacion/facturacion/index.js'); ?>"></script>
@endsection