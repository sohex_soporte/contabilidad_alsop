<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false
        );
    }

    public function listado_facturacion_get(){
        
        $fecha = $this->input->get('fecha');

        $this->load->model('DeFacturas_model');
        $this->db->where('de_facturas.created_at BETWEEN "'.$fecha. ' 00:00:00" and "'.$fecha. ' 23:59:59" ');
        $this->data['data'] = $this->DeFacturas_model->detalle_list();

        $this->response( $this->data);
    }


    public function uso_cfdi_get()
    {    
        $this->load->model('CaUsoPoliza_model');
        $this->data['data'] = $this->CaUsoPoliza_model->getAll();
        $this->response($this->data);
    }

    public function formas_pago_get()
    {    
        $this->load->model('CaFormasPago_model');
        $this->data['data'] = $this->CaFormasPago_model->getAll();
        $this->response($this->data);
    }

    public function metodos_pago_get()
    {    
        $this->load->model('CaMetodosPago_model');
        $this->data['data'] = $this->CaMetodosPago_model->getAll();
        $this->response($this->data);
    }

    public function tipo_comprobante_get()
    {    
        $this->load->model('CaTipoComprobante_model');
        $this->data['data'] = $this->CaTipoComprobante_model->getAll();
        $this->response($this->data);
    }

    public function crear_factura_post()
    {
        $factura_id = null;
        $cfdi = null;
        
        $this->load->library('form_validation');

        $this->form_validation->set_rules('folio', 'folio', 'trim|required|exists[ca_transacciones.folio]');
        $this->form_validation->set_rules('forma_pago_id', 'forma_pago_id', 'trim|required|exists[ca_formas_pago.dms_id]');
        $this->form_validation->set_rules('metodo_pago_id', 'metodo_pago_id', 'trim|required|exists[ca_metodos_pago.dms_id]');
        $this->form_validation->set_rules('sub_total', 'sub_total', 'trim|required|numeric');
        $this->form_validation->set_rules('total', 'total', 'trim|required|numeric');


        $this->form_validation->set_rules('receptor_rfc', 'receptor_rfc', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('receptor_nombre', 'receptor_nombre', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('receptor_uso_cfdi', 'receptor_uso_cfdi', 'trim|required|exists[ca_uso_cfdi.dms_id]');
        $this->form_validation->set_rules('receptor_email', 'receptor_email', 'trim|max_length[255]');
        $this->form_validation->set_rules('observaciones', 'observaciones', 'trim');
        

        // $this->form_validation->set_rules('fecha', 'fecha', 'trim|required|valid_date');
        // $this->form_validation->set_rules('tipo', 'tipo de movimiento', 'trim|required|in_list[1,2]');
        // $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|required|exists[cuentas_dms.id]');
        // $this->form_validation->set_rules('total', 'total', 'trim|required|numeric');
        // $this->form_validation->set_rules('concepto', 'concepto', 'trim|required');


        // $this->form_validation->set_rules('departamento', 'departamento', 'trim|required|exists[departamentos.id]');
        // $this->form_validation->set_rules('cliente_id', 'cliente_id', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $this->load->model('CaTransacciones_model');
            $transaccion_datos = $this->CaTransacciones_model->get(array(
                'folio' => $this->input->post('folio')
            ));


            if(is_array($transaccion_datos)){

                $this->load->model('DeFacturas_model');
                $factura_datos = $this->DeFacturas_model->get(array(
                    'transaccion_id' => $transaccion_datos['id']
                ));

                if($factura_datos == false){

                    $this->load->model('CaFormasPago_model');
                    $forma_pago_datos = $this->CaFormasPago_model->get([
                        'dms_id' => $this->input->post('forma_pago_id')
                    ]);

                    $this->load->model('CaMetodosPago_model');
                    $metodo_pago_datos = $this->CaMetodosPago_model->get([
                        'dms_id' => $this->input->post('metodo_pago_id')
                    ]);

                    $this->load->model('CaUsoCFDI_model');
                    $uso_cfdi_datos = $this->CaUsoCFDI_model->get([
                        'dms_id' => $this->input->post('receptor_uso_cfdi')
                    ]);

                    $this->load->model('Asientos_model');
                    $asientos_datos = $this->Asientos_model->getAll([
                        'transaccion_id' => $transaccion_datos['id']
                    ]);
                    
                    $conceptos = [];
                    if(is_array($asientos_datos)){

                        // $this->load->model('CaUnidades_model');

                        foreach ($asientos_datos as $key => $value) {

                            // $unidades_datos = $this->CaUnidades_model->get([
                            //     'id' => $value['unidad_id']
                            // ]);
                            
                            $conceptos[] = [
                                'producto_servicio_id' => $value['producto_servicio_id'],
                                'cantidad' => $value['cantidad'],
                                'unidad_id' => $value['unidad_id'],
                                'concepto' => $value['concepto'],
                                'precio_unitario' => $value['precio_unitario'],
                                'total' => $value['abono']
                            ];
                        }
                    }
                    

                    
                    $this->load->library('parser');

                    $data = array(
                        # [CFD33]
                        'folio' => $this->input->post('folio'),
                        'fecha' => $transaccion_datos['fecha'],
                        'forma_pago_id' => $forma_pago_datos['id'],
                        'metodo_pago' => $metodo_pago_datos['nombre'],
                        'sub_total' => $this->input->post('sub_total'),
                        'tipo_moneda' => DEFAULT_TIPO_MONEDA,
                        'total' => $this->input->post('total'),
                        'tipo_comprobante' => DEFAULT_TIPO_COMPROBANTE,
                        'metodo_pago_id' => $metodo_pago_datos['id'],
                        'lugar_expedicion' => DEFAULT_LUGAR_EXPEDICION,
                        'observaciones' => $this->input->post('observaciones'),

                        # [RECEPTOR]
                        'receptor_rfc' => $this->input->post('receptor_rfc'),
                        'receptor_nombre' => $this->input->post('receptor_nombre'),
                        'receptor_uso_cfdi_id' => $uso_cfdi_datos['id'],
                        'receptor_email' => $this->input->post('receptor_email'),

                        # [CONCEPTO]
                        'conceptos' => $conceptos
                        
                    );

                    $cfdi = $this->parser->parse('/api/template_facturas', $data,true);

                    $factura_id = $this->DeFacturas_model->insert([
                        'transaccion_id' => $transaccion_datos['id'],
                        'forma_pago_id' => $forma_pago_datos['id'],
                        'metodo_pago_id' => $metodo_pago_datos['id'],
                        'sub_total' => $this->input->post('sub_total'),
                        'total' => $this->input->post('total'),
                        'receptor_rfc' => $this->input->post('receptor_rfc'),
                        'receptor_nombre' => $this->input->post('receptor_nombre'),
                        'receptor_uso_cfdi' => $uso_cfdi_datos['id'],
                        'receptor_email' => $this->input->post('receptor_email'),
                        'cfdi' => base64_encode($cfdi)
                    ]);
                    $cfdi = base64_encode($cfdi);

                    $this->data['message'] = [
                        'FACTURA CREADA'
                    ];
                    
                }else{
                    $cfdi = $factura_datos['cfdi'];
                    $factura_id = $factura_datos['id'];
                    $this->data['message'] = [
                        'FACTURA RECUPERADA'
                    ];
                }
            }
           
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->data['data'] = [
            'factura_id' => $factura_id,
            'cfdi' => $cfdi
        ];
 
        $this->response($this->data);
    }

    
}
