@layout('layout')

@section('contenido')

<style>
    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
    }
</style>
<div class="row">




    <div class="card mt-5" style="width: 100%;">
        <div class="card-body">

            <h4 class="mt-2 mb-2 text-gray-800 text-center">
                MYLSA, QUERETARO S.A. DE C.V.<br />
                POSICION FINANCIERA CON BBVA, S.A.<br />
                CUENTA No. 0134341260 POR EL DIA<br />
                22 DE SEPTIEMBRE DE 2021
            </h4>

            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                        <tr style="height:15.0pt">
                            <th class="xl286" style="height:15.0pt">FECHA</th>
                            <th class="xl287">RENCIA</th>
                            <th class="xl288">NOMBRE</th>
                            <th class="xl289">CONCEPTO</th>
                            <th class="xl247">VERIFICADO</th>
                            <th class="xl290"><span style="mso-spacerun:yes">&nbsp;</span>DEBE<span
                                    style="mso-spacerun:yes">&nbsp;</span></th>
                            <th class="xl291">&nbsp;</th>
                            <th class="xl290"><span style="mso-spacerun:yes">&nbsp;</span>HABER<span
                                    style="mso-spacerun:yes">&nbsp;</span></th>
                            <th class="xl292">SALDO</th>
                            <th class="xl65">&nbsp;</th>
                            <th class="xl65">&nbsp;</th>
                            <th class="xl285">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr class="table-primary">
                            <td height="32" class="xl299"><b>02 DE AGOSTO DE 2021</b></td>
                            <td class="xl300">&nbsp;</td>
                            <td class="xl301">&nbsp;</td>
                            <td class="xl302">&nbsp;</td>
                            <td class="xl303">&nbsp;</td>
                            <td class="xl304">&nbsp;</td>
                            <td class="xl301">&nbsp;</td>
                            <td class="xl304">&nbsp;</td>
                            <td class="xl305"><b>$ 2,800,000.00</b> </td>
                            <td class="xl306">&nbsp;</td>
                            <td class="xl307">&nbsp;</td>
                            <td class="xl308">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">ANDRES GARCIA PAGO CUENTA DE TERCERO/ 0083886016</td>
                            <td class="xl254">UNIDAD 02-08-21</td>
                            <td class="xl281">NAOMI</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp; </span>20,000.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl313" style="border-top:none">&nbsp;</td>
                            <td class="xl283" style="border-top:none">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS DEBITO/146083455</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">
                                </span>2,548.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS CREDITO/146083455</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp; </span>26,544.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS PUNTOS TDC BANCOME/146083455</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">
                                </span>2,125.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS DEBITO/144115630</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">
                                </span>1,255.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS CREDITO/144115630</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp; </span>10,500.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS CREDITO/146083455</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">
                                </span>8,500.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS DEBITO/144115630</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">
                                </span>7,000.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">VENTAS CREDITO/144115630</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp; </span>25,111.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">SPEI RECIBIDOSANTANDER/0191542472 014</td>
                            <td class="xl254">UNIDAD 02-08-21</td>
                            <td class="xl281">JUAN CARLOS</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp; </span>20,000.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl252">&nbsp;</td>
                            <td class="xl253">DEPOSITO EFECTIVO PRACTIC/******1260</td>
                            <td class="xl254">&nbsp;</td>
                            <td class="xl281">&nbsp;</td>
                            <td class="xl255"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">
                                </span>5,000.00 </td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl312">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl129">&nbsp;</td>
                            <td class="xl129">CARMELO LAVIN FERNANDEZ</td>
                            <td class="xl65">ART DE LAVADO</td>
                            <td class="xl269">&nbsp;</td>
                            <td class="xl192">&nbsp;</td>
                            <td class="xl268">&nbsp;</td>
                            <td class="xl268"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>11,480.09
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
<!--
                        <tr style="height:15.0pt" style="display:none;">
                            <td class="xl251" style="height:15.0pt"></td>
                            <td class="xl129">Total</td>
                            <td class="xl129"></td>
                            <td class="xl65"></td>
                            <td class="xl269">&nbsp;</td>
                            <td class="xl65"><b>$726,583.00</b></td>
                            <td class="xl192">&nbsp;</td>
                            
                            <td class="xl314"><b>$1,500,000.00</b></td>
                            <td class="xl65"><b>$2,026,583.00</b></td>
                            
                            <td class="xl268">&nbsp;</td>
                            <td class="xl268"><span style="mso-spacerun:yes"></td>
                           
                            <td class="xl285">&nbsp;</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="card mt-5" style="width: 100%;">
        <div class="card-body">

            <h4 class="mt-2 mb-4 text-gray-800 text-center">
                CHEQUES EN TRANSITO
            </h4>

            <div class="col-sm-12">
                <table class="table table-striped">
                    <tbody>-->
                    <tr class="table-primary">
                            <td colspan="12"><b>CHEQUES EN TRANSITO</b></td>
                        </tr>


                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">02/08/2021</td>
                            <td class="xl129">&nbsp;</td>
                            <td class="xl129">MARIA DEL ROPSARIO LOPEZ</td>
                            <td class="xl65">MATERIALES</td>
                            <td class="xl269">&nbsp;</td>
                            <td class="xl192">&nbsp;</td>
                            <td class="xl268">&nbsp;</td>
                            <td class="xl268"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes"> </span>685,088.21 </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">09/09/2019</td>
                            <td class="xl252">33474</td>
                            <td class="xl253">DEMETRIO ARTURO CHAVEZ GUZMAN</td>
                            <td class="xl309">TOMA DE UNIDAD</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl323"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes"> </span>120,000.00 </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">18/09/2019</td>
                            <td class="xl252">33536</td>
                            <td class="xl253">ASOCIACION DE CHARROS RANCHO EL PITAYO</td>
                            <td class="xl309">RENTA DE LOCAL</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl323"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>20,000.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">16/03/2021</td>
                            <td class="xl252">35571</td>
                            <td class="xl129">SILVESTRE MONTES MONTES</td>
                            <td class="xl65">DEVOLUCION</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl192"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;
                                </span>1,000.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">08/04/2021</td>
                            <td class="xl309">35597</td>
                            <td class="xl253">FELIX RUIZ TAPIA</td>
                            <td class="xl295">DEVOLUCION</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl192"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;
                                </span>1,000.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">08/04/2021</td>
                            <td class="xl309">35596</td>
                            <td class="xl253">AGLAYA SAMANTA ESTRADA TAPIA</td>
                            <td class="xl295">DEVOLUCION</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl192"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;
                                </span>1,000.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">15/06/2021</td>
                            <td class="xl309">35686</td>
                            <td class="xl253">OPERADORA CORPORATIVA NOTARIEL 33 DE QRO</td>
                            <td class="xl295">HONORARIOS</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>23,200.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">15/06/2021</td>
                            <td class="xl309">35684</td>
                            <td class="xl253">OPERADORA CORPORATIVA NOTARIEL 33 DE QRO</td>
                            <td class="xl295">HONORARIOS</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>23,200.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">16/07/2021</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl129">DEPOSITO PENDIENTE</td>
                            <td class="xl65">PEDIENTE</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes"> </span>803,800.00 </td>
                            <td class="xl251">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl129">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">28/07/2021</td>
                            <td class="xl309">35728</td>
                            <td class="xl253">PEDRO<span style="mso-spacerun:yes">&nbsp; </span>URIEL JAGUEY</td>
                            <td class="xl295">DEVOLUCON</td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl315">&nbsp;</td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;
                                </span>1,000.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">06/08/2021</td>
                            <td class="xl309">35743</td>
                            <td class="xl253">JOSE CARLOS MUÑOZ RODRIGUEZ</td>
                            <td class="xl295">TOMA DE UNIDAD<span style="mso-spacerun:yes">&nbsp;</span></td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl311">&nbsp;</td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes"> </span>210,000.00 </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">29/07/2021</td>
                            <td class="xl309">35737</td>
                            <td class="xl253">TABITA MARTINEZ HERNANDEZ</td>
                            <td class="xl295">DEVOLUCION</td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl315">&nbsp;</td>
                            <td class="xl310">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;
                                </span>5,000.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">12/08/2021</td>
                            <td class="xl309">35751</td>
                            <td class="xl253">OPERADORA CORPORATIVA NOTARIEL 33 DE QRO</td>
                            <td class="xl295">HONORARIOS</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>23,200.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl251" style="height:15.0pt">12/08/2021</td>
                            <td class="xl309">35752</td>
                            <td class="xl253">OPERADORA CORPORATIVA NOTARIEL 33 DE QRO</td>
                            <td class="xl295">HONORARIOS</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl311"><span style="mso-spacerun:yes">&nbsp;</span>$<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>23,200.00
                            </td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl314">&nbsp;</td>
                            <td class="xl65">&nbsp;</td>
                            <td class="xl285">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6 float-right">
                <table class="table">
                    <tbody <tr height="26" style="height:19.5pt">
                        <td class="xl324 bold">SALDO SEGUN POSICION BANCARIA AL<span
                                style="mso-spacerun:yes">&nbsp;</span>
                        </td>
                        <td class="xl325">02 DE AGOSTO DE 2021</td>
                        <td class="xl326">
                            </span><b>$ 3,283,325.00</b> </td>
                        </tr>
                        <tr height="25" style="height:18.75pt">
                            <td class="xl328">SALDO SEGUN BBVA AL DIA</td>
                            <td class="xl325">02 DE AGOSTO DE 2021</td>
                            <td class="xl329" width="137" style="width:103pt"><b>$1,267,730.41</b></td>
                        </tr>
                        <tr height="21" style="height:15.75pt">
                            <td class="xl306">&nbsp;</td>
                            <td class="xl306">&nbsp;</td>
                            <td class="xl331"><b>$2,015,594.59</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




<div class="row mt-5 mb-5">
    <div class="card" style="width: 100%;">
        <div class="card-body">

            <h4 class="mt-4 text-gray-800 text-center">
                MYLSA QUERETARO<br />
                22 DE SEPTIEMBRE DE 2021
            </h4>

            <div class="col-sm-12 float-right">
                <table class="table table-striped  mt-5">
                    <tbody>

                        <tr style="height:15.0pt">
                            <td class="xl65" style="height:15.0pt">&nbsp;</td>
                            <td class="xl334">&nbsp;</td>
                            <td class="xl335">BANCOMER</td>
                            <td class="xl336"><b>2,026,583.00</b></td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl65" style="height:15.0pt">&nbsp;</td>
                            <td class="xl334">&nbsp;</td>
                            <td class="xl337">BANAMEX</td>
                            <td class="xl336"><b>$1,291,581.61</b></td>
                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl65" style="height:15.0pt">&nbsp;</td>
                            <td class="xl334">&nbsp;</td>
                            <td class="xl337">BANORTE</td>
                            <td class="xl336"><b>$500,000.00</b></td>

                        </tr>
                        <tr style="height:15.0pt">
                            <td class="xl65" style="height:15.0pt">&nbsp;</td>
                            <td class="xl334">&nbsp;</td>
                            <td class="xl337">HSBC</td>
                            <td class="xl336"><b>$4,228.40</b></td>

                        </tr>
                        <tr height="21" style="height:15.75pt">
                            <td height="21" class="xl65" style="height:15.75pt">&nbsp;</td>
                            <td class="xl334">&nbsp;</td>
                            <td class="xl65">SCOTIABANK</td>
                            <td class="xl338"><b>$19,733.37</b> </td>

                        </tr>
                        <tr height="21" style="height:15.75pt">
                            <td height="21" class="xl65" style="height:15.75pt">&nbsp;</td>
                            <td class="xl339">&nbsp;</td>
                            <td class="xl340">TOTAL</td>
                            <td class="xl341"><b>$3,842,126.38</b></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection