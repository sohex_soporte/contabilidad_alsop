@layout('layout')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    INGRESOS
</h4>

<div class="row">
    <div class="col-sm-12">
    <div class="card">
  <div class="card-body">
        <?php echo isset($crud['output'])? $crud['output'] : ''; ?> 
    </div>
    </div>
    </div>
</div>

<?php if($segment != 'add' && $segment != 'edit'){ ?>
    <div class="row">
    <div class="col-sm-12">
    <div class="card">
  <div class="card-body">
<a href="{{ site_url('formatos_efectivo/ingresos') }}" class="btn btn-dark mt-4 mb-3"><i class="fa fa-arrow-left"></i> Regresar</a>
</div>
    </div>
    </div>
</div>
<?php } ?>


@endsection

@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
<?php foreach($crud['css_files'] as $file){ ?>
    
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    
<?php } ?>

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
    
    tfoot{
        display:none
    }
</style>
@endsection

@section('script')
<script src="{{ base_url('assets/components/jquery/dist/jquery.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="{{ base_url('assets/components/DataTables/datatables.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="{{ base_url('assets/components/bootstrap-4.6/js/bootstrap.min.js') }}"></script>

<?php foreach($crud['js_files'] as $file){ ?>
    <?php if (!preg_match("/chosen/i", $file)) { ?>
        <script src="<?php echo $file; ?>"></script>
    <?php } ?>
<?php } ?>

<script>
$(function(){
    if($('.add_button').length){
        $('.add_button').addClass('btn').addClass('btn-primary');
    }

    if($('div.form-button-box input.ui-input-button').length){
        $('div.form-button-box input.ui-input-button').addClass('btn').addClass('btn-secondary');
    }

    if($('input.form-control').length){
        $('input.form-control').css('height','36px');
    }

    if($('input.form-control#field-fecha').length){
        $('input.form-control#field-fecha').attr('type','date');
    }
    if($('input.form-control#field-monto').length){
        $('input.form-control#field-monto').attr('step','0.01');
    }
    
    if($('td.actions a').length){

        $( "td.actions" ).each(function( index ) {
            $(this).find('a:eq(0)').addClass('btn').addClass('btn-primary');
            $(this).find('a:eq(1)').addClass('btn').addClass('btn-danger');
        });
        
    }
    
})
</script>
@endsection