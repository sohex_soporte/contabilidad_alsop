<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ingresos_diarios extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){ 
        // $this->load->helper(array('form', 'html', 'validation', 'url'));
        $this->blade->render('ingresos_diarios/index');
    }
    
}
