<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_flujo_efectivo extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        exit();
    }

    public function buscar_cuenta($importe_list,$cuenta = false,$origen = false){
        $response = 0;
        if(is_array($importe_list)){
            foreach ($importe_list as $key => $value) {
                if($value['cuenta'] == $cuenta && $value['origen_transaccion_id'] == $origen ){                    
                    $response = $value['monto'];
                    break;
                }
                
            }
        }
        return $response;
    }

    public function primer_reporte(){ 

        $response = array();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required|valid_date');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $this->load->model('Asientos_model');
            $this->load->model('CaCuentas_model');
            $this->load->model('CaOrigenTransaccion_model');

            $fecha = $this->input->post('fecha');

            $listado_cuentas = $this->CaCuentas_model->get_cuentas_asientos_list();
            $listado_origenes = $this->CaOrigenTransaccion_model->get_list();

            $importe_list = $this->Asientos_model->get_total_por_cuenta_origen([
                'asientos.fecha_creacion' => $fecha
            ]);
            // utils::pre($importe_list);

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >'
            );
            $this->table->set_template($template);

            $set_heading = array( 'Cuenta' ,'Descripcion');
            if(is_array($listado_origenes) && count($listado_origenes)>0){
                foreach ($listado_origenes as $key_origen => $origen) {
                    $set_heading[] = $origen['nombre'];
                }
            }
            $set_heading[] = 'Total ingreso';
            $this->table->set_heading($set_heading);
            
            

            if(is_array($listado_cuentas) && count($listado_cuentas)>0){
                foreach ($listado_cuentas as $key_cuenta => $cuenta) {

                    if(is_array($listado_origenes) && count($listado_origenes)>0){
                        $set_row = array(
                            $cuenta['cuenta_codigo'],
                            $cuenta['cuenta_nombre']
                        );
                        $set_row_total = 0;

                        foreach ($listado_origenes as $key_origen => $origen) {
                            $origen_transaccion_id = $origen['id'];
                            $cuenta_id = $cuenta['cuenta_id'];
                            
                            $monto = $this->buscar_cuenta(
                                $importe_list,
                                $cuenta_id,
                                $origen_transaccion_id
                            );
                           
                            $set_row_total = $set_row_total + $monto;
                            $set_row[] = utils::formatMoney($monto,2);
                        }
                        $set_row[] = utils::formatMoney($set_row_total,2);
                        $this->table->add_row($set_row);
                    }

                }

                if(is_array($listado_origenes) && count($listado_origenes)>0){
                    $set_row = array(
                        '',
                        'TOTALES'
                    );
                    $set_row_total = 0;

                    foreach ($listado_origenes as $key_origen => $origen) {
                        $origen_transaccion_id = $origen['id'];
                        
                        $importe = $this->Asientos_model->get_total_cuentas_ingreso([
                            'origen_transaccion_id' => $origen_transaccion_id,
                            'asientos.fecha_creacion' => $fecha
                        ]);
                        $monto = 0;
                        if(is_array($importe)){
                            $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
                        }
                        $set_row_total = $set_row_total + $monto;
                        $set_row[] = utils::formatMoney($monto,2);
                    }
                    $set_row[] = utils::formatMoney($set_row_total,2);
                    $this->table->add_row($set_row);
                }
            }
            
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate()),
                'num_cuentas' => is_array($listado_cuentas)? count($listado_cuentas) : 0
            );

        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }

    public function segundo_reporte(){
        $response = array();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required|valid_date');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $this->load->model('Asientos_model');
            $this->load->model('CaCuentas_model');
            $this->load->model('CaOrigenTransaccion_model');

            $fecha = $this->input->post('fecha');

            $listado_cuentas = $this->CaCuentas_model->get_cuentas_asientos_list();
            $listado_origenes = $this->CaOrigenTransaccion_model->get_list();

            $importe_list = $this->Asientos_model->get_total_por_cuenta_origen([
                'asientos.fecha_creacion' => $fecha
            ]);

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >'
            );
            $this->table->set_template($template);

            $set_heading = array( 'Fecha' ,'Descripcion');
            if(is_array($listado_cuentas) && count($listado_cuentas)>0){
                foreach ($listado_cuentas as $key_cuenta => $cuenta) {
                    $set_heading[] = $cuenta['cuenta_nombre'];
                }
            }
            $set_heading[] = 'Total ingreso';
            $this->table->set_heading($set_heading);
            
            
            if(is_array($listado_origenes) && count($listado_origenes)>0){
                foreach ($listado_origenes as $key_origen => $origen) {

                    if(is_array($listado_cuentas) && count($listado_cuentas)>0){
                        $set_row = array(
                            $origen['cuenta_id'],
                            $origen['nombre']
                        );
                        $set_row_total = 0;

                        foreach ($listado_cuentas as $key_cuenta => $cuenta) {
                            $origen_transaccion_id = $origen['id'];
                            $cuenta_id = $cuenta['cuenta_id'];
                            
                            $monto = $this->buscar_cuenta(
                                $importe_list,
                                $cuenta_id,
                                $origen_transaccion_id
                            ); 
                            
                            $set_row_total = $set_row_total + $monto;
                            $set_row[] = utils::formatMoney($monto,2);
                        }
                        $set_row[] = utils::formatMoney($set_row_total,2);
                        $this->table->add_row($set_row);
                    }

                }
                
                if(is_array($listado_cuentas) && count($listado_cuentas)>0){
                    $set_row = array(
                        '',
                        'TOTALES'
                    );
                    $set_row_total = 0;

                    foreach ($listado_cuentas as $key_cuenta => $cuenta) {
                        $cuenta_id = $cuenta['cuenta_id'];
                        
                        $importe = $this->Asientos_model->get_total_cuentas_ingreso([
                            'cuenta' => $cuenta_id,
                            'asientos.fecha_creacion' => $fecha
                        ]);
                        $monto = 0;
                        if(is_array($importe)){
                            $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
                        }
                        $set_row_total = $set_row_total + $monto;
                        $set_row[] = utils::formatMoney($monto,2);
                    }
                    $set_row[] = utils::formatMoney($set_row_total,2);
                    $this->table->add_row($set_row);
                }
            }
            
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate()),
                'num_cuentas' => is_array($listado_cuentas)? count($listado_cuentas) : 0
            );

        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();

    }
    
}
