<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_ingresos extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    // public function buscar_cuenta($importe_list,$fecha = false,$origen = false){
    //     $response = 0;
    //     if(is_array($importe_list)){
    //         foreach ($importe_list as $key => $value) {
    //             if($value['fecha_creacion'] == $fecha && $value['origen_transaccion_id'] == $origen ){                    
    //                 $response = $value['monto'];
    //                 break;
    //             }
                
    //         }
    //     }
    //     return $response;
    // }

    public function buscar_cuenta($fecha = false,$origen = false){
        $response = 0;
        $rst = $this->DeIngresos_model->getSum(array(
            'id_origen_transaccion' => $origen,
            'fecha' => $fecha
        ));

        if(is_array($rst)){
            $response = $rst['monto'];
        }
        return $response;
    }

    public function buscar(){ 

        $response = array();
        $this->load->model('DeIngresos_model');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('semana', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $semana = $this->input->post('semana');
            $dias = @json_decode($this->input->post('dias'));

            $this->load->model('Asientos_model');
            $this->load->model('CaOrigenTransaccion_model');

            // $this->db->where_in('asientos.fecha_creacion',$dias);
            // $importe_list = $this->Asientos_model->get_total_por_fecha_origen();

            $listado_origenes = $this->CaOrigenTransaccion_model->get_list();

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped" >'
            );
            $this->table->set_template($template);

            $columna = ['BANCOS'];
            if(is_array($dias) && count($dias)>0){
                foreach ($dias as $key_dias => $value_dias) {
                    $columna[] = utils::aFecha($value_dias,true);
                }
            }
            $columna[] = 'TOTAL SEMANA';
            $this->table->set_heading($columna);

            $columna = [];
            if(is_array($listado_origenes) && count($listado_origenes)>0){
                foreach ($listado_origenes as $key_origen => $value_origen) {

                    $columna = array($value_origen['nombre']);
                    $monto_total = 0;
                    if(is_array($dias) && count($dias)>0){
                        foreach ($dias as $key_dias => $value_dias) {
                           
                            $monto = $this->buscar_cuenta(
                               
                                $value_dias,
                                $value_origen['id']
                            ); 

                            $columna[] = utils::formatMoney($monto,2);
                            $monto_total =  $monto_total + $monto;
                            
                        }
                    }
                    $columna[] = utils::formatMoney($monto_total,2);
                    $this->table->add_row($columna);

                }
            }

            // if(is_array($dias) && count($dias)>0){
            //     $set_row = array(
            //         'TOTAL'
            //     );
            //     $set_row_total = 0;

            //     foreach ($dias as $key_dias => $value_dias) {

            //         $importe = $this->Asientos_model->get_total_cuentas_ingreso([
            //             # 'cuenta' => $cuenta_id,
            //             'asientos.fecha_creacion' => $value_dias
            //         ]);
            //         $monto = 0;
            //         if(is_array($importe)){
            //             $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
            //         }
            //         $set_row_total = $set_row_total + $monto;
            //         $set_row[] = utils::formatMoney($monto,2);
            //     }
            //     $set_row[] = utils::formatMoney($set_row_total,2);
            //     $this->table->add_row($set_row);
            // }
            
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }
}
