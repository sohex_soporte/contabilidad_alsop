<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false
        );
    }

    public function listado_personas_get(){
        $this->load->model('CaPersonas_model');
        $this->data['data'] = $this->CaPersonas_model->getAll();
        $this->response($this->data);
    }

    private function cache_listado_proveedores_dms(){

        $listado_proveedores = [];
        $this->load->driver('cache');
        $listado = utils::api_get([
            'url' => URL_DMS.'api/catalogo-proveedor'
        ]);

        if(is_array($listado) && count($listado)>0){
            foreach ($listado as $key => $value) {
                $listado_proveedores[ $value['proveedor_rfc'] ] = [
                    'id' => $value['id'],
                    'rfc' => $value['proveedor_rfc'],
                    'nombre' => $value['proveedor_nombre'],
                    'apellido_paterno' => $value['apellido_paterno'],
                    'apellido_materno' => $value['apellido_materno'],
                    'direccion' => $value['proveedor_calle'],
                    'numero_int' => $value['proveedor_numero'],
                    'numero_ext' => '',
                    'colonia' => $value['proveedor_colonia'],
                    'municipio' => '',
                    'estado' => $value['proveedor_estado'],
                    'codigo_postal' => $value['codigo_postal'],
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo'],
                    'origen' => 'DMS'
                ];
            }
            $this->cache->file->save('listado_proveedores', json_encode($listado_proveedores), 120);
        }
        return $listado_proveedores;
    }
      
    private function cache_listado_clientes_dms(){

        $listado_clientes = [];
        $this->load->driver('cache');
        $listado = utils::api_get([
            'url' => URL_DMS.'api/clientes'
        ]);

        if(is_array($listado) && count($listado)>0){
            foreach ($listado as $key => $value) {
                $listado_clientes[ $value['rfc'] ] = [
                    'id' => $value['id'],
                    'rfc' => $value['rfc'],
                    'nombre' => $value['nombre'],
                    'apellido_paterno' => $value['apellido_paterno'],
                    'apellido_materno' => $value['apellido_materno'],
                    'direccion' => $value['direccion'],
                    'numero_int' => $value['numero_int'],
                    'numero_ext' => $value['numero_ext'],
                    'colonia' => $value['colonia'],
                    'municipio' => $value['municipio'],
                    'estado' => $value['estado'],
                    'codigo_postal' => $value['codigo_postal'],
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo_electronico'],
                    'origen' => 'DMS'
                ];
            }
            $this->cache->file->save('listado_clientes', json_encode($listado_clientes), 120);
        }
        return $listado_clientes;
    }

    public function listado_clientes_get(){

        $listado_clientes = [];

        $this->load->driver('cache');
    
        $listado = $this->cache->file->get('listado_clientes');
        if($listado == false){
            $listado_clientes = $this->cache_listado_clientes_dms();       
        }else{
            $listado_clientes = @json_decode($listado,true);
        }

        $this->load->model('CaPersonas_model');
        $listado_personas = $this->CaPersonas_model->getAll([
            'tipo_persona_id' => 1
        ]);

        if(is_array($listado_personas) && count($listado_personas)>0){
            foreach ($listado_personas as $key => $value) {
                $listado_clientes[ $value['rfc'] ] = [
                    'id' => $value['persona_id'],
                    'rfc' => $value['rfc'],
                    'nombre' => $value['nombre'],
                    'apellido_paterno' => $value['apellido1'],
                    'apellido_materno' => $value['apellido2'],
                    'direccion' => $value['domicilio'],
                    'numero_int' => '',
                    'numero_ext' => '',
                    'colonia' => '',
                    'municipio' => '',
                    'estado' => '',
                    'codigo_postal' => '',
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo_electronico'],
                    'origen' => 'SQL'
                ];
            }
        }

        $this->data['data'] = $listado_clientes;
        $this->response($this->data);
    }

    public function listado_proveedores_get(){

        $listado_proveedores = [];

        $this->load->driver('cache');
    
        $listado = $this->cache->file->get('listado_proveedores');
        if($listado == false){
            $listado_proveedores = $this->cache_listado_proveedores_dms();       
        }else{
            $listado_proveedores = @json_decode($listado,true);
        }

        $this->load->model('CaPersonas_model');
        $listado_personas = $this->CaPersonas_model->getAll([
            'tipo_persona_id' => 2
        ]);

        if(is_array($listado_personas) && count($listado_personas)>0){
            foreach ($listado_personas as $key => $value) {
                $listado_proveedores[ $value['rfc'] ] = [
                    'id' => $value['persona_id'],
                    'rfc' => $value['rfc'],
                    'nombre' => $value['nombre'],
                    'apellido_paterno' => $value['apellido1'],
                    'apellido_materno' => $value['apellido2'],
                    'direccion' => $value['domicilio'],
                    'numero_int' => '',
                    'numero_ext' => '',
                    'colonia' => '',
                    'municipio' => '',
                    'estado' => '',
                    'codigo_postal' => '',
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo_electronico'],
                    'origen' => 'SQL'
                ];
            }
        }

        $this->data['data'] = $listado_proveedores;
        $this->response($this->data);
    }

    public function agregar_cheque_post(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required');
        $this->form_validation->set_rules('cliente_id', 'RFC', 'trim|required');

        $this->form_validation->set_rules('referencia', 'Referencia', 'trim|required');
        $this->form_validation->set_rules('comentarios', 'Comentarios', 'trim');

        $this->form_validation->set_rules('nomenclatura_id', 'Nomenclatura de la poliza', 'trim|required');
        $this->form_validation->set_rules('fecha_poliza', 'Fecha', 'trim|required|valid_date');
        
        if ($this->form_validation->run($this) != FALSE) {

            $poliza_id = null;
            $cheque_id = null;
            $persona_id = null;

            $fecha_poliza = $this->input->post('fecha_poliza');
            $tipo_poliza_id = $this->input->post('tipo_persona_id');

            # buscar poliza
            $this->load->model('CaPolizas_model');
            $poliza_datos = $this->CaPolizas_model->get([
                'ca_polizas.PolizaNomenclatura_id' => $this->input->post('nomenclatura_id'),
                'ca_polizas.fecha_creacion' => $fecha_poliza
            ]);

            if($poliza_datos == false){
                $d = new DateTime($fecha_poliza);
                $poliza_id = $this->CaPolizas_model->insert([
                    'PolizaNomenclatura_id' => $this->input->post('nomenclatura_id'),
                    'ejercicio' => $d->format('Y'),
                    'mes' => $d->format('m'),
                    'dia' => $d->format('d'),
                    'fecha_creacion' => $fecha_poliza,
                ]);
            }else{
                $poliza_id = $poliza_datos['id'];
            }

            #CREACION DEL DE LA PERSONA SI NO EXISTE
            $this->load->model('CaPersonas_model');
            $persona_datos = $this->CaPersonas_model->get([
                'ca_personas.id' => $this->input->post('cliente_id'),
                'ca_personas.tipo_persona_id' => $tipo_poliza_id
            ]);

            $cliente_id = null;
            $proveedor_id = null;

            if($tipo_poliza_id == 1){
                $cliente_id = $this->input->post('cliente_id');
            }else{
                $proveedor_id = $this->input->post('cliente_id');
            }
            
            if($persona_datos == false){
                $persona_datos = $this->CaPersonas_model->get([
                    'ca_personas.cliente_id' => $this->input->post('cliente_id'),
                    'ca_personas.tipo_persona_id' =>$this->input->post('tipo_persona_id')
                ]);
            }
            if($persona_datos == false){
                $persona_id = $this->CaPersonas_model->insert([
                    'cliente_id' => $this->input->post('cliente_id'),
                    'tipo_persona_id' => $this->input->post('tipo_persona_id'),
                    'rfc' => $this->input->post('rfc'),
                    'apellido1' => $this->input->post('apellido1'),
                    'apellido2' => $this->input->post('apellido2'),
                    'nombre' => $this->input->post('nombre'),
                    'telefono' => $this->input->post('telefono'),
                    'domicilio' => $this->input->post('domicilio'),
                    'correo_electronico' => $this->input->post('correo_electronico')  
                ]);
            }else{
                $persona_id = $persona_datos['persona_id'];
            }

            #CREACION DE LA TRANSACCION
            $this->load->model('CaTransacciones_model');
            $transaccion_folio = $this->CaTransacciones_model->get_max_folio([
                'origen' => 'CHEQUE',
            ]);
            $folio_asignado = $transaccion_folio + 1;
           
            $transaccion_id = $this->CaTransacciones_model->insert([
                'cliente_id' => $cliente_id,
                'proveedor_id' => $proveedor_id,
                'persona_id' => $persona_id,
                'folio' => $folio_asignado,
                'origen' => 'CHEQUE',
                'fecha' => $fecha_poliza
            ]);
            
            $this->load->model('DeDatosCheque_model');
            $cheque_id = $this->DeDatosCheque_model->insert([
                'transaccion_id' => $transaccion_id,
                'referencia' => $this->input->post('referencia'),
                'comentarios' => $this->input->post('comentarios')
            ]);

            $this->load->model('ReTransaccionesPolizas_model');
            $rel_poliza_transaccion_id = $this->ReTransaccionesPolizas_model->insert([
                'transaccion_id' => $transaccion_id,
                'poliza_id' => $poliza_id
            ]);
            

            // $this->load->model('ReTransaccionesDatosCheque_model');
            // $rel_id_transacion = $this->ReTransaccionesDatosCheque_model->insert([
            //     'transaccion_id' => $transaccion_id,
            //     'datos_cheque_id' => $cheque_id
            // ]);

            // $this->load->model('RePolizasDatosCheque_model');
            // $rel_id_poliza = $this->RePolizasDatosCheque_model->insert([
            //     'poliza_id' => $poliza_id,
            //     'datos_cheque_id' => $cheque_id
            // ]);
            
            $this->data['data'] = array(
                // 'rel_id_transacion' => $rel_id_transacion,
                // 'rel_id_poliza' => $rel_id_poliza,
                'rel_poliza_transaccion_id' => $rel_poliza_transaccion_id,
                'poliza_id' => $poliza_id,
                'cheque_id' => $cheque_id,
                'folio_asignado' => $folio_asignado
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function agregar_persona_post(){
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cliente_id', 'Cliente', 'trim');
        $this->form_validation->set_rules('rfc', 'RFC', 'trim|required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('apellido1', 'Primer apellido', 'trim|required');
        $this->form_validation->set_rules('apellido2', 'Segundo apellido', 'trim');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('domicilio', 'Domicilio', 'trim|required');
        $this->form_validation->set_rules('correo_electronico', 'Correo electrónico', 'trim|valid_email');
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required|exists[ca_tipo_persona.id]');

        
        
        if ($this->form_validation->run($this) != FALSE) {
   
            $this->load->model('CaPersonas_model');
            $cheque_id = $this->CaPersonas_model->insert([
                // 'cliente_id' => $this->input->post('cliente_id'),
                'tipo_persona_id' => $this->input->post('tipo_persona_id'),
                'rfc' => $this->input->post('rfc'),
                'apellido1' => $this->input->post('apellido1'),
                'apellido2' => $this->input->post('apellido2'),
                'nombre' => $this->input->post('nombre'),
                'telefono' => $this->input->post('telefono'),
                'domicilio' => $this->input->post('domicilio'),
                'correo_electronico' => $this->input->post('correo_electronico')  
            ]);
            
            $this->data['data'] = array(
                'cheque_id' => $cheque_id
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function editar_persona_post(){
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('identificador', 'identificador', 'trim|required|exists[ca_personas.id]');
        $this->form_validation->set_rules('cliente_id', 'Cliente', 'trim');
        $this->form_validation->set_rules('rfc', 'RFC', 'trim|required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('apellido1', 'Primer apellido', 'trim|required');
        $this->form_validation->set_rules('apellido2', 'Segundo apellido', 'trim');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('domicilio', 'Domicilio', 'trim|required');
        $this->form_validation->set_rules('correo_electronico', 'Correo electrónico', 'trim|valid_email');
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required|exists[ca_tipo_persona.id]');
        
        if ($this->form_validation->run($this) != FALSE) {

            $identificador = $this->input->post('identificador');

            $this->load->model('CaPersonas_model');
            $id = $this->CaPersonas_model->update([
                'tipo_persona_id' => $this->input->post('tipo_persona_id'),
                'rfc' => $this->input->post('rfc'),
                'apellido1' => $this->input->post('apellido1'),
                'apellido2' => $this->input->post('apellido2'),
                'nombre' => $this->input->post('nombre'),
                'telefono' => $this->input->post('telefono'),
                'domicilio' => $this->input->post('domicilio'),
                'correo_electronico' => $this->input->post('correo_electronico')  
            ],[
                'id' => $identificador
            ]);
            
            $this->data['data'] = array(
                'persona_id' => $identificador
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function modal_agregar_asiento_get(){

        $this->load->model('CaCuentas_model');
        $cuentas_datos = $this->CaCuentas_model->getAll();

        $this->load->model('CaUnidades_model');
        $unidades_datos = $this->CaUnidades_model->getAll();

        $this->load->model('Asientos_model');
        $this->db->order_by('id','desc');
        $this->db->limit(1);
        $asientos_datos = $this->Asientos_model->get([
            'transaccion_id' => $this->input->get('transaccion_id'),
            'estatus_id' => 'POR_APLICAR'
        ]);

        $data_content = [
            'cuentas' => $cuentas_datos,
            'unidades' => $unidades_datos,
        ];

        $html = $this->load->view('/api/modal_agregar_asiento',$data_content,true);

        $this->data['data'] = array(
            'html' => base64_encode(utf8_decode($html)),
            'asiento' => $asientos_datos
        );
        $this->response($this->data);
    }

    public function modal_editar_asiento_get(){

        $renglon_id = $this->input->get('renglon_id');

        $this->load->model('CaCuentas_model');
        $cuentas_datos = $this->CaCuentas_model->getAll();

        $this->load->model('CaUnidades_model');
        $unidades_datos = $this->CaUnidades_model->getAll();

        $this->load->model('Asientos_model');
        $asiento = $this->Asientos_model->get([
            'id' => $renglon_id
        ]);

        $data_content = [
            'cuentas' => $cuentas_datos,
            'unidades' => $unidades_datos,
        ];

        $html = $this->load->view('/api/modal_editar_asiento',$data_content,true);

        $this->data['data'] = array(
            'html' => base64_encode(utf8_decode($html)),
            'asiento' => $asiento
        );
        $this->response($this->data);
    }

    public function detalleAll_get(){

        $nomenclatura = $this->input->get('nomenclatura');
        $folio = $this->input->get('folio');
        $fecha = $this->input->get('fecha');

        $fecha_ini = $this->input->get('fecha_ini');
        $fecha_fin = $this->input->get('fecha_fin');

        $contenido_busqueda = [
            'origen' => 'CHEQUE'
        ];
        if (strlen($fecha_ini) > 0 && strlen($fecha_fin) > 0) {
            $this->db->where( "ca_transacciones.fecha BETWEEN ".$this->db->escape($fecha_ini)." AND ".$this->db->escape($fecha_fin)." ", NULL, FALSE );
        }
        if (strlen($nomenclatura) > 0) {
            $contenido_busqueda['ca_polizas.PolizaNomenclatura_id'] = $nomenclatura;
        }
        if (strlen($folio) > 0) {
            $contenido_busqueda['ca_transacciones.folio'] = $folio;
        }
        if (strlen($fecha) > 0) {
            $contenido_busqueda['ca_transacciones.fecha'] = $fecha;
        }

        $this->load->model('DeDatosCheque_model');
        $this->data['data'] = $this->DeDatosCheque_model->get_detalleAll($contenido_busqueda);
        // $this->data['sql'] = $this->db->last_query();
        $this->response($this->data);
    }

    public function modal_agregar_asiento_post(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('modal_cuentas', 'Cuenta', 'trim|required');
        
        // $this->form_validation->set_rules('precio_unitario', 'Precio Unitario', 'trim|required');
        // $this->form_validation->set_rules('cantidad', 'Cantidad', 'trim|required');
        $this->form_validation->set_rules('precio', 'Monto', 'trim|required');

        $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');
        // $this->form_validation->set_rules('tipo_unidad', 'Tipo de unidad', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $cargo = 0;
            $abono = 0;
            if($this->input->post('modal_tipo') == 1){
                $cargo = $this->input->post('precio');
            }else{
                $abono = $this->input->post('precio');
            }

            $cuenta_id = $this->input->post('modal_cuentas');

            $this->load->model('Asientos_model');
            $asiento_id = $this->Asientos_model->insert([
                'polizas_id' => $this->input->post('polizas_id'),
                'estatus_id' => 'POR_APLICAR',
                'transaccion_id' => $this->input->post('transaccion_id'),
                'tipo_pago_id' => 'CHEQUE',
                'cuenta' => $cuenta_id,
                'cargo' => $cargo,
                'abono' => $abono,
                'concepto' => $this->input->post('concepto'),
                'fecha_creacion' => utils::get_date(),
                'precio_unitario' => $this->input->post('precio'),
                'cantidad' => 1,
                // 'unidad_id' => $this->input->post('tipo_unidad'),
                'departamento_id' => 9
            ]);

            // if($asiento_id != false){
            //     $this->load->model('MaBalanza_model');
            //     $balaza = $this->MaBalanza_model->aplicar_asiento($cuenta_id, $cargo, $abono, $asiento_id);
            //     if (is_array($balaza) && array_key_exists('balanza_id', $balaza)) {
            //         $this->Asientos_model->update(array(
            //             'estatus_id' => 'APLICADO',
            //             'fecha_asiento_aplicado' => utils::get_datetime()
            //         ), array(
            //             'id' => $asiento_id
            //         ));
            //         $balanza_id = $balaza['balanza_id'];
            //     }
            // }

            $this->data['data'] = [
                'asiento_id' => $asiento_id
            ];
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }


    public function modal_editar_asiento_post(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('modal_cuentas', 'Cuenta', 'trim|required');
        $this->form_validation->set_rules('precio', 'Monto', 'trim|required');
        $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $renglon_id = $this->input->post('renglon_id');
            $cargo = 0;
            $abono = 0;
            if($this->input->post('modal_tipo') == 1){
                $cargo = $this->input->post('precio');
            }else{
                $abono = $this->input->post('precio');
            }

            $cuenta_id = $this->input->post('modal_cuentas');
            
            $this->load->model('Asientos_model');
            $asiento_id = $this->Asientos_model->update([
                // 'polizas_id' => $this->input->post('polizas_id'),
                // 'estatus_id' => 'POR_APLICAR',
                // 'transaccion_id' => $this->input->post('transaccion_id'),
                // 'tipo_pago_id' => 'CHEQUE',
                'cuenta' => $cuenta_id,
                'cargo' => $cargo,
                'abono' => $abono,
                'concepto' => $this->input->post('concepto'),
                // 'fecha_creacion' => utils::get_date(),
                'precio_unitario' => $this->input->post('precio'),
                'cantidad' => 1,
                // 'unidad_id' => $this->input->post('tipo_unidad'),
                // 'departamento_id' => 9
            ],[
                'id' => $renglon_id
            ]);

            $this->data['data'] = [
                'asiento_id' => $renglon_id
            ];
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }


}
