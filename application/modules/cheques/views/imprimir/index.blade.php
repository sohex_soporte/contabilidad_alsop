<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>

<HEAD>
    <style>
        @page {
            margin: 12 0 50 0;
        }

        @font-face {
            font-family: "source_sans_proregular";
            src: local("Source Sans Pro"), url("fonts/sourcesans/sourcesanspro-regular-webfont.ttf") format("truetype");
            font-weight: normal;
            font-style: normal;

        }

        body {
            font-family: "source_sans_proregular", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
        }

        .table {
            color: #004381;
            /* height: 100%; */
            width: 100%;
            /* border-collapse:separate; 
            border-spacing: 0 5px; */
            border-collapse: collapse;
            font-size: 11px;
        }

        .table tr {
            /* padding-top: 20px; */
            /* margin: 0px 10px 10px 0px; */
            /* height: 50px; */
            /* padding-bottom: 1em; */
        }

        .table tr.bordered td {
            border-color: #004381;
            /* padding: 5px 4px 1px 4px; */
            font-size: 11px;
            /* margin: 0px 10px 10px 0px; */
        }


        table.table_rd {
            width: 100%;
            border-spacing: 0; 
            border-collapse: separate;
            color: #004381;
            /* padding: 15px; */
            margin: 1px;
        }
        table.table_rd2 {
            width: 100%;
            border-spacing: 0; 
            border-collapse: separate;
            color: #004381;
            /* padding: 15px; */
            margin: 1px;
            
        }
        table.table_rd td {
            padding: 5px 5px 7px 5px;
            /* margin: 10px; */
            vertical-align: top;
        }
        table.table_rd2 td {
            padding: 10px;
            margin: 10px;
        }

        table.table_rd tr:last-child td:last-child {
            border-bottom-right-radius:7px;
        }

        table.table_rd tr:last-child td:first-child {
            border-bottom-left-radius:7px;
        }

        table.table_rd tr:first-child th:first-child,
        table.table_rd tr:first-child td:first-child {
            border-top-left-radius:7px
        }

        table.table_rd tr:first-child th:last-child,
        table.table_rd tr:first-child td:last-child {
            border-top-right-radius:7px
        }

        /* table.table_rd tr th:first-child, */
        /* table.table_rd tr td:first-child {
        border-left: 1px solid #004381;
        border-bottom: 1px solid #004381;
        }
        table.table_rd tr:first-last {
        border-top: 1px solid #004381;
        border-right: 1px solid #004381;
        } */

    </style>
</HEAD>

<BODY>
    <table style="width: 100%; padding: 10px 0px;">
        <tr>
            <td style="font-size: 20px; color: #004381;" >&nbsp;&nbsp;<b>BBVA</b></td>
            <td style="font-size: 17px; text-align:right" ><?php echo utils::folio($cheque['folio'],7)?>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr style="background-color: #d4edfc;">
            <td colspan="2" style="padding: 7px 15px;">
                <table class="table">
                    <tr class="bordered">
                        <td colspan="9">&nbsp;</td>
                        <td colspan="1" style="border-bottom:groove 1px; text-align:right;width:70px;"><b>Fecha:&nbsp;&nbsp;</b></td>
                        <td colspan="2" style="border-bottom:groove 1px;width:120px;"><?php echo utils::aFecha_slash($cheque['poliza_fecha_creacion']); ?></td>
                    </tr>
                    <!-- <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr> -->
                    <tr class="bordered">
                        <td colspan="12"><b>Páguese este cheque a la orden de:</b>&nbsp;&nbsp;<span style="color:#7b7b7b;">PARA ABONO EN CUENTA DEL BENEFICIARIO</span> 
                        </td>
                    </tr>
                    <!-- <tr>
                        <td colspan="15">&nbsp;</td>
                    </tr> -->
                    <tr class="bordered">
                        <td colspan="10" style="border-bottom:outset 1px;border-right:outset 1px;">Beneficiario:<?php echo trim($cheque['nombre'].' '.$cheque['apellido1'].' '.$cheque['apellido2']); ?></td>
                        <!-- <td style="text-align:right;font-size:11px;">$</td> -->
                        <td colspan="2" style="border: 1px solid;font-size:11px;border-color:white;"><?php echo utils::format($total['abono']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr> 
                    <tr class="bordered">
                        <td colspan="11" style="border-bottom:outset 1px;"><?php echo $letras; ?> 00/100</td>
                        <td colspan="1" style="border-bottom:outset 1px;font-size:9px"><b>Moneda Nacional</b></td>
                    </tr>
                    <tr class="bordered">
                        <td colspan="12" style="font-size:9px;text-align:right;"><b>BBVA Bancomer S.A. Institución de Banca Múltiple. Grupo Financiero.</b></td>
                    </tr>

                    <tr class="bordered">
                        <td colspan="2" >
                            <img width="125" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJUAAAAsCAYAAABsZ/ryAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5QwRBCkXZ5RPmgAAIY5JREFUeNrtXHl0FFXWv6+qunrvTiedfSF7QhJISNhlDfuigoKCgOAwIKgM6rjiMiqOo4yOuOOCKIgLq8gOAmEngYRA9o3snXQ63el9q6563x/dnXRiBJwJ6jkf95w+6dN169a97/3efb9336sgjPEc+H2FAwBUUFzjt+NAXmJuQXVaQ3NbtEZn7WeyMGK3CgaaR7CBCnFLaLBf3YD+kSUzJ2SWzJyQqaYoHgMAJAAGAPQ7u35bbkbQ7wgqDACw+3Be+KZtJ0cXFtdltuks4U4nx8fAAUEQQJEEBwAuACA5jEnWxQEGAIpEnFxCaxJjg8vumjL05CMPTiyXicVOACD+6Aa8Lb+U3wtUXH5xjeK19bsmn71YPV6rtwUihEEm5huDA2UNUWH+1/pFBrWEBMosQj5tcLFY1GEwiRpVuoCGZk2/xhZ9jE5vCWMYFoRCnm1AYnj+o0sm7X3w3nE13jj+6Ia8LV3ye4AKr9+4L+2DL4/Mq2vuSAaEICxQ1jR0UMzpOTOG5U8aPaBFqZA7wD0t/sI/lmPJvMIq/x/2Xkg9nFM05lpje5qT4UiFTNAxI3vAnjfXLDgWHhxgg9vA+tPILQKVl+9gePyVTWM37zy3sMNg91PIhcbxI5IOPbXizuMjMpM0HmXCMzNCFy44HxsEMC6G5FEkV9OgFr+9Yd+Ic/k1aSzLEkIBj7n/rmGnVj00rYhjudug+oMFAwKOw7cKVBwAELDi+Q3jt+7OXWyxucSxkf5Vjy+b9s1ji6eWwk2xbIztDie1cs3GyQVF17IwIjAChAEhzHGdQEQAGCHUa5a7Lb+rIMAYo35h8lbq1jyA4J5/69tR3/2Ut9hsc4nTEoNz339tyebxI9JawU2ub5hV7E6GeuSFjdN27L94v8XOCpDH8a6s1j2g2/LHC0FgHBvhX3ErQMV999OZmC9/OD3faHaI+8cFX9749orPh6bHd8BvWK2ZLHYyOiKocfHcMR8SBMK9A+d2WeHPIhgw8HkEO//uO6r6evrDbTqjYNrCN1YVFDcPCw2SNnz8z8XvzJoyrAluHlAYPLWsP7qhbst/JagPMxUGAITXffxjRklVS5ZQQDgWzB657bcC6uiZq8G7Dl5MLa9u7qc32qVOhiVvo+vPLRgASAJxy+aPObTqoenlfQgqhBtUbeI9Ry5PcTg4anB61PmXn7g3H24OUNjmsJOrXvp6/E9HC+/W6c2hLOdNVrch9ecV7P5gDAq5SBsdGbQNAKAvORX+atvJ5MYWXbJYzLMtnH3HMalI2EvVu5MHeVdsRE1Dq/jJV7dMPnyqeK6TwXSIUlqdmhherPSXtgNg558HVt05XM9CSG/a17v+e3vu9aW37+Dz22+ziwBjLIyJCmqdPCa9DQCIvgQVcexM8RCHw8WLiwooevDe0ZXQa5ZC2GSx8r747kTisTNFaS1thgCd3hzW1GpMxBhgZGb03rdfXvTT8IwEHfS+1LtZ8S12edsKw69nTtZHF/voXg8bXh2uh47vPb2lXF/7N+pHr23Ko+vy/EbCjXHLAQBtsdkpsVDg8MToGz8GAMRhliIQydzA1o0EeW33FahwSVWTrKahPYEgEGQNjCtQyKV2AEz28BGfKyj3f+6f38wpKFGNsdgYAWAOECJAJCStY4bGH9n4ziPbQwP97QCYuE583k7xBoN6Xjx4oiDix8OXUmob28LMZodAKOS7hqb3K3vusXvy/aRixlddqzfS736+f1BBcV2c3e6UCIV8a2JsaP3KBydfvlxU7b/z4MXMFrUx0GpzAMYYEGAQCmgYkZVQ9tITc3I3fn8i6cjJK4M0WouA41wgEfNd07MHFUwZN6hu665TqdV1bZEms13GciwAAPAoyhUZ5teyfMGki8MHJWqv05F4+/7zMdv3nc+qbdRGu1wuvkwq0gQpZTpVa0eoQi7SvfHcvIMDk6P1vdl4bf32oTv25022OxhJgL+0acncMUcfXjCpyqOL9xzJjdi57+wdBEEIlAHy5kcfmnYuJiLE+j8AC/oUVBfyK4L0RluQUEBaRw9NrnY75jtwET59sTRg+dMbVpZf02bwKMKV0E+ZHxcdVB2gkBqHpMc2LF8wsUrIp10AcF1AFZbV+Z3OLVM6GQ4lx4UaZ2Rntvlc5/YeyQt/9MVNz7ZorOFububGXt7lqkkWq33DB2uX5QBwBAACB+Mi//Lkh7OOnK6c7XCyPHdqw3D0dAnUN7btLq9RJZTVtKV1JRUPljGGxhZt2vg70uo+3HRk3rVGbbK7FguAEAaj2R6180CupbC0eTjLAeAeOZdAGFrUev+DW17YCoBRzwRptduJlc9/lr3/eNG9HUa7kuNQpxFEYMCYAJIAsDs2S/Z9/ewmAc1nfds6t7Aq4PPvch5sajGGAUJQVa9NrG3QpAho8t3Fc7OrrpbXKvYezRvS1KrrhxBCBEnat/10NvXZR+7N+9OAqrSqKdjuYERKf7Eqa0BMr6Mv53xJglgs5AalRuanJUcUvPn8AzlhQf62zl6/8XTAncotDf3by18tL69RJ/lJ+W1/f3j6FzOyM9U+96FApdwWGiSrbe+wBHIMor3mLXbgHT5VOrpNp78Q5C93AiDuzY/2ZB07V32n3cnxECIAAQaEAEeGKqqGZyUWxUWH1tkdeXR9c0ci7nQTgKYp16Qx6Qemj89s3jTgRGF9sy6Z5TAo5ALN+BH9f549bejl3YfyssprNAMtNkaEfKKiKcTGRikrxgzvXwLdBp93tgS8+uWvxm3bl7/E7uT4CCEgCIAQpbSOT5N2VZsp3uFkKZZDUFjaNPh0btlPk0ZnqH1scft/zo/T6CxBiOjyuU1rDtnwzbHJi+dm19Q1tMkBYwIh0uBwOCQURTISscjZF2DoM06lN1qlAj7FBgdINFHhyt5SKHr64VmXnll59yUCIeBRPJcHj2RXg/YuHmLEnb5YFvq3l79ccbVcnSYVUx2L54765ukVs0p63IxGZCZ17N/8/IYJ896QFlW0pCPUlWFaNKb4vUfyI5fOm1B5pazW/5tdZ++x2FxChDw5CnMQHiyr2/Sfh/8zZmiqBgDwHUMS6h9Zs+mZFo05DCEvJcE4SCnTaToM/Gv1mliW40AuERgfXTxh49qn5ucBAFo4e3Td1EX/Ehw5XXq310WMAfonhBXu+fLJD6JCA82/DJzEhaW1fgdziqZ7AeUGIsksX5D93ZPLphetXLNx5o+HL99jsToEJIExQt3pmsPJ8A6fvDrc4WQp32sYMFTWtaeeOF+iHH9HWsv+YxftNAWMVCJTC/k80wOzx5RBHxwn+i2guh5pJh5eOPHSkIzYxohgf5NSIXN213cHJuB7gdQNazck4x5Aha16cdOKq+UtaXKp0LTsgbGb3lqz6NKvcC/Ep2mOZXG3CwgBWG1OycEThRlL502oePXdXZNrm3SJ7nbvUg0LkreMGZqqA89m96zJwxpPni/d9unWnEdsTkwjAHA4Wd7W3WdnnzhzdeTVctUQkYB2zLtzyNa1T82/5MMlXX4ykaW7DxhiIpXlUaGBRgCO7KUPcWllU4DJ4gj09cnJsLxvd5+dFhQgsX361rJ90RE/Nh07U5w5ZmhC8ZjhKe1dASDu82+PJZXVtGW6bXetPxBCYLHalSfPF8eMH5Ga+/7a5YcqalQimiZxclykBfpIbhZUuL3DJDRZbDRgjL0jGmMAjDFgjEGpkDqmjx9Uz7IcqrjWLPWmF4yxG12dVMT9xcVywHEckAQBHIc7f+98IO7i4QUltcHvbTywoLiiLU0uFVqXzR+zZd2aRefh17kXrm1QizoMFv8uruLW4ziA3MJrg1eu+cxw/Fz5ZJbF0DWY3c8U8Gk9dK7aEAAA8drT8y5culqXdi6/diIGAIQQNLYYExtVhkQej2Knj0/d+eE/l+ZAt+kMkNPFCX19JBAChVxs88bWm8ikIgdJIicAiHw9q6pvz3j+zR2JW388n7diYfa+15+et8F9CRPeGDuMFv6mbaemm8wOiZ9cYEmMVtYWlqlSGBcmAAhwMhyVf7UmHgDy+DTFDewfbfI8Af2PVOpmQeVecX6z+2T8vzcceMBssQdgQBh1drobDBzGgAB5eGQXiLCnP7vBxZM8WJYFlsNAEgg4Nzo9YOrEEgBGgIEDh4PxM1mdUrmEb33ovlFfr3vhwRM3iAtfKa0NMFsdCrdN9ygFN18CVZspbtO2szFOFyZRj8oBQgjkMomtGxIBQCoSMi8/cc/uRas3JKnbzZFuexwghGBoelTOZ+se3k+RPBZ6TMU6vYWHMQbvNEQQiAtQSAzXARUxbkRKa1JM0JXcK41je1aWDGan6Nyl6nFllU3ppy+U/PDJv5YfJwjSq8C99dGuzLJq1WCCQDAyK/7MK0/O3Tfn4XdfaFDpQxAiAWMOqhvaE7V6oyDAT+rsAlPfVdNuACqEdxw4F/vqO9uW1zYb48CzoO6h88u7uvq2F5VOTtIjh1zHCwJxIYHS1gfuHrbjnZcWn+yy/esrxItXayNtdqcIIQwRwTK1zmCXWewuoWdAIAeDSQGfcoQFiuvrVaZ4DruzHgIOxCLaAL+Y7jExadTAtrBgvzp1uznS/RMGiZi2LJ039qhCJrV1L6FgcLpcpMFoVvpaIUnCFRQgN/Ueg3vlJhEJmVf/Pnfn6pe/Cqys16VwHAY3l/NmVQJ0Rqdi275LCzLS4ppXLppSCgAo90qV8oe9ubNtdhet9JcYH1444diQgbGqmIjA2gZVR4g3pLZ2S/j5gqqAmdlZzXALjmTfaPpDGGPX5HHpO2lagN2B9Ty61L1jEQIgvBzFhyQiQIB+DQMI9Q4Pjx0Bn8eOHZrUNH5kmuYm69RkUVlDoovFiEcR3F2TMvfkXKgYVlLVkg4eoo0QQFZa5Mnk+LDar3acS/CmV4IgwN9PbO/NGb3RzLPaHPIuCBAgFNLOuOgQC3Sf9gAAYVWrTtBh8ILKTRl4FMmEBPn5LGQ4ACA8hVoEW3bmxG/aljNp8piB5775cNW7z73x7azzBbXZVhsjhE7uhwAhDAaLS3rweGH6ykVTSjnMEa+8s21aY4sxDhABHAeweceZ7K27z42tb9ZEe2u0CCEwWx3yM3nlkTOzs5r6Bkbd5ddAhTHmEONi0JwZw5vmzrij4VY8/MaCkYfIe+YoFt3EwMJl1U3S2sb2BMAAfJqyjRicWGUyW6nyGnU6hznAmIDwYEnj2mfu2/3Bl4dHcVwXGggCsf5ysbE3u/XNGqHBaFV0y8UYrGIhv7fjzPhKab3CYHIofS/RFLIHBUh9QEVwm7Ydjz9y6urAadkZV/7z2b45V8pah5ZVt6QOGhDzxtFvX/zqpbd/KNn4/amFre3msC7zCDDGYLHZ5QCA167fkXnmYs0klnMPOp3BItt5sGCql38gossHxsWSl4trEwDgwq3otR6gcnOo/ccvhaxdv/tug9kpRei/mWv/l90Vt3AcRhHBkva3Xly0JystrgOAQzd5vo/ddeB8vEZrDgcEIBXRuoyUflqaRxXv+bnIYDCa5SIhz/HArBE7xg9Pa33xrR8CMe5KqgRBgEQi7Dn1AQDCdY1tEpudkfpe4dO0SyYRurr0vB+SyzlfEmexMnK3bfdUx+dTrvAQhWfLBKGvtx9PWPPW9lVqrSWiqrY1o15liACEoE1rDXv7kz1TpozJ2LL2qXlnjGab8JMtOY+6WOwZVSxQJAnx0aF1p/JKgzZ+nzPPbGXEBCJAqRBoJGJaxWGCQwjA6XT6q7XWCJYFEsBNYq41aD28Sua4mUb9LdIDVAjv/fli2JOvbl5R09iR6l7n/dppy1snGDAo5GLDvdMyPxuUGqN3B31TUz8+cb449PufcqfaGUwDAFA8wsGnedz0CZmq6IgfS0oq7SNHZMb+/NrT8y7sPHg+pq6xPcXXAMMw5KUrNbEYuMuos9yBwWyz08fOFCfZHS5J1zgjAGOWX9ukkcdHh5l9AIXPXioLOXzyymjGxfmMSw4sVka8fuPBYVHhQS3Vda1hx86UTFa3myIQIkEk4Gn5PMIfAOQcxnCuoGHyXQ+9yaSnRledvVg5mOW8gHJX1KPDFeX9E8JUj72waUmT2hyNEAIeRbjumzl01xvPzc9xsSymeTy8eUdO8gvrdvy9w8jIEHKPIFWbod8r72wf/vKTc84HKmQMBtRnyEIYc3N8lt14w5ZDqafyKtMwJrCXP3HcbwMV9gHhfwNJBBhGZCWUPb50ZhHc1Chy+15d3yK566F/P1Veo0nz+sCjCG7K6P479371/A/b95+LOHmuOGn1sjvzTueWBr64bvvqVq013JcwY4xBJhHY1zw2491nV866BACotqlNtOCx9/5aXKEaYrYyQndac08/JIFwZIi04eUn7tnw0H0TKgEAdh/KjXzytS1/a2gxxHKcb1zus/skiTEiCJZlgeI4914izSPZp1dMe9tssYq+3nFuod7oVGBw81OSRJyLxYR3dU3zCJwYHXR51pSMY1/+cGpei8YSCQiBFxiBCmHbPx6f9e4ji6eV7z+eH/63l7/6W12zPtHtS2fVHoR8yrVy0bgP33lpySlwb1D3iVDeDvE26opFU4tXLJpa1FcPgC5M/ZZVRterNL/hHqGA5sQigV4qEZhcLIsAAwgFPAdJEi4XyxJzZ4xsmjtjZAMAwJm8skAej3SJBJSd4zhPxkYsRRGO0EBJQ1R4gK7TMMZgd7oojBEWCGgXYA9SEIEpEtn5fJ5FJhE4vPp6k4VvsTFSmkfZSIJgf8m2vK2PkYvl+CyLqaQY5dUFs0dVJMdFGAf0j276etvJ8ZW1bakmi1PJcRxNkeAQ8HnmkEBJ/bgRKZeeWXnXWY3WQO86eFFjtDB+LheHEEEAzSNdfjKRVioVOQEAWe1OimUxKaApMwYEBIHAvaNBOkIDJbVDMxIaAIDoy4PZCGN8z54jeZGfbPl5osPh4kPn/O/+iwDgurwKARCI6HVlhzEAQRDs7KmDT61YOKXiN4LkpsFkszuo1f/4alJpVUu0TMp33Dkx67xQyLeyLIuiI5S24VlJWrHAu+Ha1bMFxbV+l67UBOlNNhEAID5NOUKD5MbhmYnaqLBAq5vHuQOrV7ULTl8oDVVrjXIXiwEBIJpHukIC5frhWYna6PAgi3shgcDp4tC+n/Mj1BqDQCTqAlv3QiwGjmOJuga10mSxi+67c2Sx+8QCRgCIAwDiUlGNvLC4NkhntEr4PJ4jISZYOzwzQecvl3byoNrGVtGFgqoAg8lGUhQFQQFSJiM1uiMqLNDmzUiXS+rlV0vr5C4OI4qiMM0jUVCA1DYwpZ8+UCF3dMXZN7BC3+05tXrNm98vr20ypMCvDqkbWfHdYe92AQADRIb5Nf/rufs/WjBrdF8DCwNgYunTn0z7bk/eAifjoqeNG7hj76anf/B0jLcnr3eOrucxml+7p6cu9NDtqX+zr435ZnIEgIFxuQid3kwHKGQOiiRZ6H4uDHW/FV3Pp56+X0+nz4Q6daE0IzhQoQnwlx9w14swEJ6005mhEHTSOILouub9TpJkZ6ZCCHlqVQSwHCaLy+szG1Ud4S+u277Cbnd+unTehHLoG2BhxsUQK5//Ytr2ffkP2J0sPWRg1NkPX1/yk6ehb+YZvTU++PzW88zW9Tqh5wzy38SInYyLXLBq/Z3l1arM1Uunbf7r/MlV8Eu+g729At0HwvXklswSvT2bWv/a0kM0RbHQ92+wYACAdZ/syvj3p4cermvqiFrz1g+ryqqbv339mXl5AppmvGeabvxY34FGAACwJVWNfk+t3TIz50LlDIeT5Q9MDs3/6PWlm/uFB1l77GNxAEA0q7V8xumCoEC5SyQQMN03ojEYzFZKrTEIGBdLKf1lVqlY4GpQtYtZF0sGB/pZlApZ58E+i81OGkwWiiRI4DiMWI4DwBiFhfjbiK5TDGB3MoS2w8RjWQwsyyGEEEjEfE6pkDk696s8PljtDsJotlCVta1yo9GiSEuOvpiSGGXouWHOcixqVGlFNM1jCc/eqs3hJJX+cqdMLHT9suU4uNbQJpWKBUxQgJ+95x4fBgwdBgtltthIAAAXyyEeReKgABnDp2m2u59dmVGjMwjUGr1ILhM7IkOVVl9w3eL/peCue63fuC/t7U8PLmlWG2KEfJ4ja0C/048umXhg3p13NHqmqZtFFqfVG4XvbTww8Ls9uXdea2xPIQmEswZEnfpg7ZKtgwfEa6HHcdmvd5yI/Xr76exWTUek3e4kpBKxYe6dQ4+veeyey14AaPVG+qEnPrr/cmlzBkIEPTA5tGDquIF5H399dJ7d4ZJNGp1y6NO3Vh70nlVa9dIXYw8cL5qCCAABn7JTJMFYbU7Jk8unb/bhjtzbG3ZnfPZtzj1WO0f5yfgWJ8PRHIdh+vi0Y++99pczXS/IAl79ypfj9h4tnAiAkFBA2Wke5bBYbX6P/3X61488OLUzuy975pMJx86UTOLRtJNxsYgiATOMS5CSEHZp9xfP7KB5PM637d/7cn//9zce/otYRBteeXLO5numDW9wD2Zv7Cbe4tXv3VdYpkqhKIr1k/KtHQarOCRQ3vbK3+fsnDYus9mnb/CVsjr5vz76cfyVkvp0AoGfzcHZBiRHXH3z+XkH+sdHGgEA3YqU2AMHmHh86cziDW8s+c/gtMjzLMfxzlyqmbhyzeYXxs599aF3Pt0zsKKmSerJRGwvH87mcJAnLxQrV7+yaezYOWuf+Penhx+vrm9LkYkF+nunDvpm1+dPfTF4QLyuC1DutfcL67YO/vvab5/NK6ydNGpIUkFIkKLjannz8Pc2Hv7rvmP5oeDhPQF+ModYLDA2qw1R2g6zbERWQuH9d42qEQr5dgfD4vvvGlXge0Snf0J4i1prCq9t1CZFhPip0pIiyhVycbvBaKV9gkfDspKajRaHvLm1IykmMkBHIE5YU6dJO3a2ZLzeYKF9xhFOTYxs1XZYouqatUnhIfKmtKTwCj+5RG0w2QReJZ3BxCssaUj39xO3syxL19a3J1ME4QoMkKtUrfp+RrPVp+6IsMFk5n+z6+z0hhZ9bFGlatBHXx/NBi/R9UiAn9QZFqLUt2oMiSazIzxrYJxao7MkXy5pHHMopzABurghvlxS4/fg6g9W7jyQv1AiFrFTxw86qtaaYw4cL5rz+vod2d6Gv0WvvXulE7PEzIlDVIPT4z9+9d1tl/cfK5zeojFFn8qrnn7xSu2E9V8eUYWHKGojQxWqoACpXioRYZbDoNUZqdZ2Y3Bzqz5SpdZH6o22YJeLQyIh35aVFnXm4YXZ+5fNn1QNv+AzCJdUNki37Do3V6uzBfRPCKl+95Ulhxes+uBuQCRYrIy8vlEjB4Bmb4OZTDYBxhgUcmHH7KlDawL95XaMsSsixK8m+46Bat9pw99P6iQIhBFBQEVNS0ZZdQu79cNV/xw1uL/GN2iJSMAihAiCAKhrbEts1ZgCpFLaceekrJ8VconD16ZCLrZiABeBEK6oVg0or24ltrz/6D/HDkvttKmQiZnP/r1so93O8OY/9vFTgAiIiQqu2vj28l1NrTqhUiHxPbmJn/3X96M7DNaoKWP7Hzt8smxCZW3bgNKqRllKgjujePQ4kiQdABRwHEvmFpSnOBkO4vspixbdO+aKTzx43cc/jS6pVA8WCPjM4rmj94WF+Ou/+P40Y2Uxr01rVnht3mJQdcYHAECEBCpsn7zx8LG8K1WFn397fPCZvIoRTa36OJXaGNPUYozJLazrrKMAALAcBsy5V9g8EjiFXNSWFBNSfPfUIaeXPzChQiYROT2c4xdTZ5NKIzCa7HJABPjJRCo+n8c1t3bEA8eCUiFtHjk4UeVtBLvTwWtq1ccAByAW8nTx0SGWU3klypY2Q+i4ESlHwZ0xvf+9j7taVh9qd7BiAU1aQgJl1Q6GFR0/UxQroHmuwQPjOzx2uSuldUqzxeFH05Q9OT78okptzrZYbOLaBnUgdL0RAwDAXS2rD3E4WbFQQFmjwgOaTBZGcOJccaxIQDND0hP04Fk3DUqJ1R05XRhmNFn8CQIgJMhPGxLobwkJ9Le7SwMYABD7yZYj/XcdyJ0jEtJku9YUjhDitB3m0EM5lyNTEiKLfJ6NrtWrI1iOg8AASX1QoH9LaY0uWqe3hKjbTUIA0HniQbWNmlCWw0DTpDk9pZ9q18G8DJvdKRKLaMfoYclXvYP7dwIV8v2ChqYn6IamJxxWt+tP7zuWH3kmrzyusrYtUqe3hFmsDj7GWIwxdtE8yi6T8PURoYrmtOSoqiljB9aOG5GqRUB4XzX61RckYqND7Qq50Gww2pUtbbq4+Y++/2B1naZ/RJii+bGHJn2XNSDe4HWMT/O4kCCZuqSqBdTt5n4zFq97QKXWxwpokpt39x2F0LXSwhevViv3/nw5m3GxBE0RVFBggKmtXU99+NXR5QI+/f7ggfE6AIBrjWrZl9/nTLPanCKaRznjo0PrKq6pa3Tl5sGXi+uz8ouqT2UNiO8AAMi7Uq3cczg/2+lkSB7Jo5T+fkYMJvTJ5mPLhXz6/SHpCR6gEtCm0ws/3Xx4vNlqV2BA0KjSRHUYzUKFTMJ4k8r7m/YPXPfJ/mUM45JOnT5kq8PB0GXVqmiTxSnYvu/C1NlTh9XHRAabAQA+/OpgakFx/RDAHBAI4zHD+udfLLw2Um+0B27fe27QtPHprYS7OI5HDk4sKa1SjbLbGdna9bvmVl5TJYuFPNPMiRl7nlpx11Xo41e0/huUoWCln23p/RMqlt7vLjN0GM18o9FCmq0OPkkSrEwiYgIUEoZP0wx0f0/uhlyQQIhLiA4uUvhJWhuadMm7DhZM5VEIUhKCGi9eruhXca25Mik23AIACAHBvfuPxTs+2HToWsU1dbTD6aRTE8Ovzr975MW7Jw32PXOEa+rV0tDggObY6PAWmiKAIBAKDPDTJ8WF7Z4/a5T39SdgWRYSYkNq/PxkZh5FOuKiQzRrHrvru+37L9QJaJ6LYdhOm9fq1fKQYGVNRFhQEUkAsBxCErFQPT17UP4Ds0dX+oxK3KG30DwexQzLiD1AkgREhfm3Wqx2SiGTOL16Wp1JGhsVVCSTCG33zxxxqVWjl6jUBgVFEURQgETHuLrqwAIBzzV6WP9zLtbFUyrkrTMnDqoyW2yfVl5riY8IVegYxoX4PB4GAOKNZ+fnhgb6GU+cLx9gtdtFQzPicmZMyCxcdO/oWgKRndWD3/N/ft6EdC5ZfcEH4FOF7vbz9SxhDlwsixBC6Lsfz/T7/Nvj46rrtUkEAcSg1KjCz9ct3xkSqHD0eHhvZRWiu0qvxUZff31rXNerHiOf5Tq+jr5PjeoXb3f7+NiNVvZW0O3Zrp2cqpcY8HVs91bU7TbI/2SgumXCYeCIqtpWEQBAfHSIjUDE73v04v+R/H8BlVe8R1nQ9d+Avy3/i/xRnOqPEp/q8C3Z9rotcHuo3pZbILdBdVv6XG6D6rb0ufwfOCOv/FFMsXUAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMTItMTdUMDQ6NDA6NTcrMDA6MDCNHyVNAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTEyLTE3VDA0OjQwOjU3KzAwOjAw/EKd8QAAAABJRU5ErkJggg==">
                        </td>
                        <td colspan="4" style="font-size:8px;text-align:center;">
                            <b>MYLSA QUERETARO, S.A. DE C.V.</b>
                            <br/>
                            AV. CONSTITUTYENTES No. 42
                            <br/>
                            ESQ. VILLAS DEL SOL. QUERETARO, QRO.
                            <br/>
                            TEL. 01 442 2 38 74 00
                            <br/>
                            <b>R.F.C. MYB-020125-CM3</b>
                        </td>
                        <td colspan="2" style="font-size:13px;text-align:right;">
                            <img width="115" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAA6CAYAAABS36B3AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5QwRBCsXVaItGAAAO3JJREFUeNrlvXeYHMW1uP1Wd09P3NmctVqtcg4oGkQSUYBMEtHYGBwwxjhcbByvjfG1cQCMcQJjG7CxMUkgssggRJAEimiVpZV2tXl3dndyd1f9/uiZ2dnVCuN7r43v99Xz1NM93T0z3VVvn3Pq1Kkqwf+PklKKnTt3IoRAKfUv+c/sf02cOBEhxIddBP+y9P+JJ21sbCSVSmGaJrquf/CHdytdAzRAz2Qj73P2nMgrK5WXJeDkZTt7TAgh/xF4pZR4vV7S6TSTJ0/+sIv0f5z+T4H1zjvvEAgESKfTCCFGzKZpYtu2F/ADIaAAKAJKBBQjKBaI4sx+gYAAbvZnsgcwhbs11CBU+WWVD1UasDI5ASQUxFHEFaof6FXQiyKioBc3R4A+YABIdHR0pMvKylBKjZiz0E2dOvXDroIPnP5twVq3bp17gyPAo2kagF8IUSyEKNeEqNU0MUoTWp2miVohRLWAMgXFCgqkUn4p8dpKeSxHibQjSTmKtK1IO5K0o0g5krTt7ltSYTkKW2YqFlBZwgRoQqAJ8OgCj6bh0QWmLvDqGqYhMHVt6GdNYOhC6ULYmhApTZDAhaob6JRKtUmpmqWSB5WiRUp5SCnVoZSKSCkTwIiwaZrGvHnzPuyqGjH9W4D15ptv5myRESAygGIhRK0mxDhNExN0TZsghGgQQtQqRKlUFKSkMhOWoj8t6UvadCdsuuM2PXGL7rhFb8KmP2nTn7KJpRwSlkPSkliOxHYktlRIpZAqW4m4NI1YaiJXeFnQhCbQhcDQBR5dw6Nr+D0aAVOnwGsQ9hkU+gxKAx5KAwYlAQ9lAQ/FfoNCr07Q1PDpAkMjraEGlFLdSqkWqdReR6qdSsodUql9SqkWpVREKeUMh03TNGzb5phjjvmwq/TDAWv16tUjgmTbNqZphoUQdZompuhCzNA0bYYQYoJC1FhShWOWMnqTDh1xm0MDFi39aQ4NpGkfsOiKpelL2MTTNmlbQub3TU3g82gETIOQd2gu8HkIeg0CpkHQNPCZhitxDBcOXRMuOBmYlBoE0HIkadsh7UiSlkM8ZRNL28RTNgNJm2jKIpqyiWaOx1IOCdvBchQSha5peA2NkM+gOOChMmRSXWBSEzYZFTapChqUBQzCpoZXx9IFfRnYdkmptjhSblFKbVdKNSulBoaDli3XDwO0fxlYr7zyypBWUXZf07SwEGKsrok5mqYt0ISYrRBj01KV9KeU0Ra3OdBnsbc3xb5Iipa+NJ2xNNGkjXQkmiYIma4EKC/wUV3op6bQT3VhgIpCP1VhPyUhL0UBk7DfJOjz4PPoOXC0f1JLTQGW7YKXsh1iKZuBhEUknqInlqK9P0F7X5K2vjiH+hK09iVo70/SG08TTTs4SmEaGkV+D1UFJvVFXsaVeGkoMqkt8FDq0/Eb2LqgSym1x5Fqg1TybSnVRqXUfqVUNB+yTFlz3HHH/Uvq+58K1ksvvZR7qDzVpgO1mibm6EI7TtPERyRMTNiUdCYcsb/PYkd3mh3dSZoiKbqiFinLQReCIr9BVdhPQ2mQseUFjK8I01BRwKjiIBVhP4UBk6DXOOw+HEeSyEiSeMoinrKIJS2imRxLWSRSNom0RTLtkLYdLFtiOQ6OVEjpSihQGftKoGkCj6HhMXRMXcPrMQh4Dfxeg4DpIeT3EPJ5CPo8BL0e/F4PAa8Hv2ngMbTD7jFpOfTF03QNJGnujbGva4A9HW7e1x3lUF+SSMJCKkXIZ1ATNhlf4mVKmY/xxSa1IYOwKaSh0YVSOxwp35RSvSaV2gi0SillviRzHIdTTjnl/w5Yzz//PMBwNacLIRp0TRyja9qpwMKUpK4zIc09EZutnSm2dSXZ35uiL2EjlKI4YDKmJMDU6kJmjCpmWm0xDeUFVBcFKAp4yRc00USa3miSrv4EHb0xWnuitPXGaO2N0hGJ0zOQoDeapD+WJp5Kk0zbpNIOlu1g2w6OlEipcA0sV4XyfjZWrvTE4FYTIARC09B1gaHreAwdr0fHaxoEfB4K/F6KC3yUhHyUFwWoLg5RWRykpsTdlhUGKA75CAfMIdK9P2HR3h9nf2eUbYcibG7uZVtrH/u6o/TELXQNKkImE0u9zKjwMbXUZFSBQchDSoP9jlRvSSWfk1K9CTRlIcsmKSWnnXbavydYzz777BC7CUDTtFpdE8fpmjhLIRbHbEYdHHC0zV1pNrSn2NmVpDtmIYCqkJcp1WHm1Zcyd0wZU2uLGF0aIuw3XVClojeapK1ngAMd/extjbDnUA/72vpo7uqnMxKjL5oinkyTStvgOCBlBpZ8YDLQkNkn7zMjwPR3DPghRSmGb/PAy37OZk1zs65hmgZ+n4dw0EdZYYDasgLGVBYyrqaEsdVFjK4opKasgNICP7ruSrt42qa5J8b21gjvNnWzdl8377X20TaQQtdgVNhkZqWPOZVeJhV7KPUJxxAccKRc7Uj1pFLqdSll6+DjCKSUnH766f8eYD399NNomoaUMiudfJoQc3VNnCeEWBq3mbB/wDHeaU+zri3Fnp4UiZRNgddgalWYRePKWTyhktmjS6grDeE1dEDR05+gqS3Ctv2dbN3XQWNTJ/tae2nridIXTZFKW+BIF54sMDLzmUEYhFAIMi29HGR5UA2BYnh6P4klRtzNfRD5Jz4gbEOOudB5TIOCgJfKkiBjKouYPLqM6WMrmDqmgobqYsqLAmiahiMVLb0xtjT3smZXO2v2dLG1tY/+pEVZ0GBmhY9FNV6mlXoo9QpLE+xwpHxaSvUosEFKmVJKYRgGtm1zxhlnfDhgPfHEE+i6ng9Usa6JU3VNXGpJcVxrXBa902Gx5lCK7V0pkimH0qCHuaNLOGVqNcdOrGJqbTEFPg8oRWv3AI37O9mwo4X121vYtr+T5s5+ItEk0nYGJUdOymRUl3QAKAia1JYFGV0ZpqY0REnYR8jvwfTo6JrAyfip4kmL/liK3v4E3X0Juvvi9PTF6YsmiSctbMt2fzdf+vw9A38IRMNBzQcp/7fyIcqHLh80zVWxw4HUNcIBLzWlISaPLuOoSbXMnVzDtIZKRlWE0XWdpOWws62PNbvaeW5bK2/v76YzlqI8aLCg2scxNV4mFumEDHqlUq85Uv1FKfWCUqrX/RtX+5x55pn/GrBWrlyJpmn5NlS5romzNSE+GXdYsCMiPa80p1nXlqR7wCLo1Zk/uoQzZ47ilGk1TKkpwjR0ovEUjfs7eGtLE2s27WfjzlYOdvQRT1quoNDyelDUcPvHBco0daY1lHHc7DrmTqqkriJMOOjNwKRlVHK20ZDnMgAcqbBtSTJtMxB3QWvrGuBAW4Sm1ggHWntp6+qnpz9ONJ4eBG44JPlQiSPBBah8CSfQPTq6rpO2nDyg8gAbLr3yVWj2c+5FA69pUF0SYsa4So6ZWc9HZtYzY1wVxeEAUil2t/fz8vY2ntjczBt7u4ilbSaUmJxY52VBpYcKv0gLpdY5Ut0jlXpMKdWVX+/Lli3754C1YsWKIf1wQoiwrolzNSGu7reYt6HL0Z8/mGZzRwor7TCuPMhZM2o5b+4Y5jWUETANIgMJ3mls5sW1O3n13b007u+kN5p0C13PFFZWpeUgGraVCk2DORMrufCkKSyaXktxxvaIpxzaIgmau+O09SWJxN0WpRCCgGlQGDQpC/uoKgpQHvZTFPIS9BkYuo4QZFp/CseRJFMW/dEkXZEohzr6ONgWobm9l9bOfjp7ovQNJIjG0yTTFmlrsPWoBssHTdMwDA2v6SHoNykO+6kqK6C+upjxdSX4vR5+8be32NfS6wKTg3MkuEYALWunZbMiZwqEfB4m1JaweFY9Jy+YwMLp9VSWFmA5ki3NPazccJAVG5vZ3t5PWUDnxDovJ9SajAoKR0Otc6S6IwNYX16d89GPfvR/D6zHHntsMBpAaLqusUQX4qtRmxPXdzqep5vSvNeZRlOKBfUlXLqwgY/OGU19aYhU2mLD9haeXP0ez721g237O4klLdANyIIqpWsvqXybSQ5KqzyowiGTT5wxi+VLplBWFEQIQUt3nJc3H2J1Yzs7W/uIDKRIW/bgb2YrTNMwPAZ+r4eioJeqkgANFWHGVRcyaVQxi6ZUU+DPtMiEa5PJrC9ISledWjaJZJpYIsVALMlALEk0niQWT5NKWziOBIHr/DQNAj6TUMAkFPASCpj4vR4Mw/WfJdI21/7sGdZv2pcpi3zV+wHhyu27NhmantkKV8LaNl5DY3xNESfNHcdZx01j0Yx6CoI+OgeSPLulhT+/tZfVe7vwG3BinZdT6zyMCgoLpV5xpLoFeEEp5RiGgWVZnHvuuf8zsO68807Ky8vzpVSdoYn/sJT45OYeWbRyn8XGjhRCKhaPK+PTx05g2ew6igJeDnX28czr23j4+Y28te0AkWgaPCYYGT+TIzMtNydPzY0AU9amkpLSogBf/8SxnH70BAxDx3IUT7yxl7tXNbLnUC+kU2CnwLbAsQchJU+FiWzhG6B7QOhU1pbz6WVzuHLpTExDZ8WrjYQDXqaMKaWqJITfa6Ayksx1TUiUcl0USkqkdJAyc85xMufVYOOTTBdRZpvtBlr1zkF+8PtXiHV356m3EeyuHFTayLDlIMsAlmltoutu1jS3LGyLoEdj9tgKzjtxOh89YQbj68pJWDYvbmvld6/t4vkd7QQ9sLTeyymjDEq89Eml/iylulUptS+fj/POO+8fB+vRRx/FcVw1ksIgoDlLhRA/aIkzd+V+m1db0iSTNnPqirl2ySTOmzeGQr/JzqYO/vbMeh58bgONzT1IzQDTS0bXgO0MugLU+6g88ltwEr/X4DufXsJ5S6a7fWISfvvou/zuiU2ko1FIx8GxBluFOQOcoYZ//r4QnHr8TL7x+bM5asoo+mMp7lixltv+9gZpW1JVGmJyfRnzJtcwb0oNk0aXURL2Yxga0pHYjoOTyVLK3H7+53x/URaoWNLi2bVN3PnEFjqbD4GV/IBSKv+zNvRYFizy7bA8wIw8yGwbbIsxJQHOPnYKHz9zPnOn1pGyJc9uaeG2F7ezek8X9YU6F4wzmV+u4RFqkyPVDUqpx3EjO1BKsXz58g8O1kMPPZSVUCjweTTxBUuJb77ZIUse3GPR3GtRXmBy9fETuOqEydQUBdhzsJO7H32Tvzyznv2dUfD5wTTdCnacPKCcPIDk4dJpJLAch+WnzeKGq0/Da3rwGAaPvNLIt377IqmBfkjFB4FSrne8tChITyTqqqZ8X5UClMQwDD59yRK+9YVzqCwvZNeBTm6483lWvLwVGekGOw2GCYYXTJNAKEBdZREzJ1Qyf0ot08dVMrqykHDIh6ELlMyAZttDgctAJqWiL5Zi0+4OHn1tF29sPYQ10A+p2GBV/D01+L7qcJhEy5deYpgEMwwXNCkhlaIiaHDesZP57HlHM2fKaPoSaf785h5ueWE7zZE4J9WZLG8wKPfR50h1K3CrUiqaheuCCy74+2D97W9/Q8u1qCg0NHFjvyWuXtHkeJ5pskinbU6cWMH3z57NsROr6O2Lcc/KN/ntg6vZ1dYPgVAGqIx0sjPOSidfQh1BSo0El5QUFfi46/uXMH96PULTiCYsLr/hYdZv2A2p6NDvOg5LT5rDF69Yyue/9Qf2NbUONgoApMLnN7n+8+fwlc8uoyDo4+0t+/mPW1by9ru7oL8XEjHXRsk29XV9EDKPCaaXcIGf2opCxteVMqGulNFVhZQXBSkImHgMF7Rk2qJvIEFLRx/bm7rZtLuDXQd7SEYTkI65qjunooe5FP5RqIao0vcDLE96ZQEDSCapDupcftosPn/RcdRVlfBeSy83PrmFhzc2M7ZQ54pJHqYUIZVUf5JKfR3oyMJ14YUXHhmsBx98MN9zXmxo4pb2JFfcu0vyVquFqSk+d+wEvn3WTCrCfl5dt5Mbf/skL29uQgUKXCnlyIyodVw7J+fEHAksdeR9Mvu2w4LZDdz9o48TDvoxPAa7DnRz3nX30NXc6qq/rHSzHebMHMfdt1yNjeCMy39MR0v7IFhK4feZfPe6i/jSZ5bh83p48a0dfOGmB9nRuJ8AKY6ZM47y0kLeWNvI/qa2PBeCyqgZ4dpnhunaaLoBhoHweDBNA6/pwdA1hADbUaTTNsm0hUpb7r3aabDSGXsyT0qNqPL4YGAddixjvA+3vfIhMzJgZUEzDPcZE3Fm14b59uVLOO/kOSQsyS9fauSmVY3oOFwxycPRFQKUWqHgC0qpnPc+H64hPbZZe0ApFTA08cNDcXXFnTskWzosCkyNG5bN4tqTp+LYDrfcvYqf3vMCHWkBpRVuuWeByocqHyb5PpKKI0gv6TCmuphwKICu62iaTkHAS8gUdKWSbsU4DkLXOOn4Wfz4Wx9j6sQ6vvHzx+joGsj8nmtT6ZrGdVefw1c+dzZe0+Tltxu56sa/sHf7fopNxc3f+yRnnbYIBOzYeYBLP/tTmps7hvqnZOY5U8k8w0lHaRopoZE6zM+U/+wyT+1loFIfBKJhWbmt1uxnn8+Lz+tB4caFpW1FPGXnQauGZhRYGTvUyCtvw4BQARu7La742eO8tWU/3/zUaXxj6QzGlIb4ysPv8tttKZKOhxOrxHko5QCfU0r1DNd8ObAeeOABAKIWotDk2q4Un/n9TsmWDpuQqfGT84/i6hMn09MX4zu3reCuJ9Zhh0ugyJcHlD3MlpLv75P6uyrRVYWmx8AwjFyndm1lMddefBy/uSdGNJpgdE0pFyw7mo+ddxzVlcWsXr+LvzzxpisdZKYyHcnFFx7P1645D69psmFbE9f84K/sbdwHA7186ppzufCc4/jyTQ8gpeRX37mUWdMaaN53aFBdHCllXAxDdcHwrpv8fZWB4wiQ/T2wsueUoKgwwPVXnsj4ujJkRtvcv2oLj73cCLpiMGQ/K3WzDZv80H2Gdrx7DKKmyS3PbmV3Sw8//49zuHhBA0GvwVV/WcvdO9J4NYOjK7hASdUCXA9Y999/P5dccslQsLJdMwUeTkg44msP7pPGxk4bQ1N8e+kMPnfCJHr7Ynz5h3/hzy+9B2VVbrPdsj4AVP8IUIfD1drei2U7+Lyuj0nXNa684FjOWjKLRNKisqyI0pICdE3nUHuEG3/9OO3N7a4Nk1WRcybw/W9cRrggQEd3H1//2YM0btkN/b34TJ2zTlvIhsaDPPzEGv7z88uwHEl3fyJjt8k8YEayJPJaoFmIsn6F3HYEyFQ+ZMO+lw8Q4nDohAZCcc6J0zj1IxPdgERN0Li3k7VbmjLlL0DIzLV522zUjmTkJHClV7CAlY2dDNz0EL//xgUsm1XHQNLmqr+u5U+7bMq8BhPDXOVItV4I8RfbtnM/oYFrsGfsqgLg6290qNJXW13j+6K5o/niKVOxLIcbfvkof355G1TWuLo5Z5jn5X8UqmwEQrYDORu+kj2GYnPjAXbua8MwdDRNQ9N0DMNgdG05UybUUVJcgK5pdEeifOuWh3lx9WZIRF1/VtqipCTMD799OePGVCOl5I77X+LVN9/D76Qo8JvUVpVSWhKmtCjIL797Gdd+8nSeenkjm7Y1DXqzs9lRbh5yz3LoPR+2n7dVed9RcqgPTw57weSwa4c1UqaPr+LipXPQhEAqxUA8zR8efZtDrb0u7O9b9nn/6eS5gbIt+Gz2+XmpaYCv/OopeiJRLlkwhi+fOInuuOSBfZIBC7+A65VSdR6Ph/vvv39QYmWllaFxWkdSnPhUs8K2HEaXBPj66dMJmAb3rljN7558Byqq3TfGymvtZbfy/SXPiFC9X6swY/N1dvRy+x9Xcet3P05lWSEILddyzfYBNu5p5bs/f4QVT78FyTgBQ1BaXk51ZQmfvPRUTlsyN6O1FMfPn8z8344h5DUI+L0UhPw0jKnG4zGoqSji3kdf58bbV5Do6RtUEYOW6AjSKyN5sqpviJRSwySTyrO/VJ5ay/6eypNQw0WJzJ0LBn186vxFVJYW4Eg3kva5Ndt55e3dg5IvN5hIG9wKCUrLU4NZiAFHgMj0XQrHbQA4AkwvK7d38ouH3+D7nzqZa06cyHONraxt6uGNToNTq5kppbpYCPGzITZWRlp5lOLC9d2YBwZcSC6aX8+MumKaW7u59b6XSBaWupLKsoe9xSO0/FB/BzI17OHeJwt44rn12LbDF644nRmT6wkGfFiWQ3NbD8+8uom7H3qV7t4oZx43nSWLpnDUrPGMHlVBaUmYcEHAjal3HCL9MeprSon0x+mJRNnX2kN3JMobm/ay/IyFbHhvP/91+yM0b9836A44UlJ5OzmbSg2CJvKBGf7dvO8M+cHs78CRo3YUZ580g2OOGpuDal9zD/c+uhYrbblqTOVBlYV+SOxZ9jwuxFK50MmMI1uKjO0oQXPAMPnda7v46OIpzJ1cx6eOGcfaph5eaVUsKhOEDJYrpf4A9OTAUkphCGrjtpj/Trcr6sN+g4/OqgMEz67ezJauJJSUu+pviPjPE9McCRz4u/BwJNDc31ZK8fRz63j97UbGNtRQVBRiIJrkwKEu+gfinHv6Ar505RnMmlpPIpmmtSNCcXEBheFgLu771/eu4q77X6SvP040niSRTGOlbaTjoOsaa9Y28s1rzuUrHz+F6776y1www/vDpQYbCFq2mT+Mk5zdlE/MSOQMOzbSJVIxdfIorli+GJ/PxLYdUmmLPz36Fk0Hu8BjjFzuQ2+IQbsw+/KqQaCkBKkNqmmpgaZoi9k8+PZe5k4exSlTqmgo9rO/P8neAZ3ZxUx1YCrweg4sAE3Q0JOmsjkGKMmYkjATqwpxHIeX392DCoYZDF/Jb7XlgTG8EPJbGkMeLu+tyV2Td13GWPZ5PUjpkE6mMwUt6I8MsPGd7YP/4TgsP/dY/vizq2lq6eR7Nz/AM69spLgwyD23XkNxYQghBOs27eYnv36U1qZW9+XIV8WZDubf3/Eor76xFYHb+TwEGn1YnHrmfssripk0aTTBoI8dOw+yf3/bYFlICYZGIOBDAYlEisHQl4xqzPVjZp7fyTPe3d7swf9WEAj4+MLlJ9MwuoJ02kLXdJ5/vZFVr7036NDNSaV8iPL2s8dVnpTNwjXcJJFanuYQrN7dSV8sTW2hn+mlPvZ1x9kzoDOrmJBSapoQ4vX77rtvECwhqOhL443Z7o9UFvoJ+zzEEymaehKux1kNhyEPqHwRO8QuUcNqg6Ew5Z/LwDd5Qi3LTprNglljiSfS/PAXK9i5u2WwwLV8WBW797Xy/Vsf4JGn32L3rmYCAS933/4lxtZXARBPpLj5t4/ReqANkgnXKM2TNqFwkOlTGzjqqAnU1lfz4mub2Gk7eH0ePrJoGrFYknXrtg8CICUFhSE+fumpXHH5GUyaWIfHNNizp4Wrr/05q1/bCLrG/AVTuPTik5k9czxSSp589i3uuudpotHEIFyI3H2EiwqYOX0s0ybXU1Dg51BbD2+9u5O9TR25+73gzIUsPWE2oPB6BfsPdnL3Q2tIJtJun+wQ3Zu3r7IqegToMubGYS+4yquXTN0cisTpiaVoKC9glBewHTqSnqx0r3b/Sg1xkOqWQsjMDxuayMUo2R4TNCsjnYZLpZEAGukz739eKQJ+L5edvYArlh/L6NoyvF6Tzu4BKssL2bmreQSbBNA0Nm7ezcat+0FJhHT40hXncN4Zi3K9CCueepOVz74N6XTGbhDgOASCfk4/dT4fu2QJc+dMpKgoRDDg56jpY9i0dhtf/+olfObKM2k60MHZy79D095DoBSj6qv46U2fY/n5x+MxjJxjedrUBj77mY+ydv12rrx8Kd/55sepqS7L3erxx87CY3r46a1/G7x/KfH6vZx1+iKuuuIM5s4aTzDgQwhIpy327G/lpttX8MDKNUyZVMc1nzwVv9+HZVmk0pI/P7KG7btaMh39+WV7hPiCEW27/PpQIx/OJMdRONL12xnpJDgOaZm7zHCrRMuTWBDxaliGwLSEoCeaImE5BHwmFWVF0BwbfGNHjPc+zEN4ZLiyzsA8CRYM+vjqFSdx8VkLcrbD6rc3c9tdz7Bu0x5XHagj/RaZ7hKHZWcdzXXXLsfQdRCC/Qfb+dlvHiXVF3VVoAAcScPYGr7xtYs58/SFBIM+NE3DshxeWL2ZPz2yGt1jcNzimRQVFhCaGuCkk+fxx98+St3YGu789XUsPX0RAJZl53xrAOPHj+KL1y7ne9/8OMGgf0h0g67rnLtsMXf84Un6+2MgFbV1lfzn9R/jsguXEAz6kFJi2w6JRArTNJg8fhQ3Xn8xTS1dfPKiExhbX4ltO5imhxdf38rDT611/YmaNqxQjpCOGGYtDv+uOPz6kpCXQr+JY0s6O3tBKXxGrhq6AS699NIhEmt/oUlvgSkqEynB/p4YzT0xpo8qYeH0ep7YfMg17Ib3b+VyFhYxqM9Vfs2Loft5akBoGp84ewHLT5/j9rHZDo8+8zY/+dVKursHMgbpMHtk+BvlSObMmcBPbvgUpSXhXNjKbXeuZPPm3W4XU8brPGlyPbf+5LMsnD85I+EFO/cc4hd3PcXjq9bS3zOAsCx27mpm4YJpGIbB7NkTKB9dye0//yJLT1+E4zg888I7/PH+5znhmBlc+2k3dHf6lHqmTrqMYNDPG2sbqasto662PAeY5Ui3LSYls+ZM5LYfX80Ji2flzm/f1cwf/vI8727Zw+KFU/nSZ86iurKUn37n44ytr0ApVyK0dfTyqz88S3QgAX7/EaAaXl8jQZTfpcTQuj2sLxPmjCmnpMBHd08/jfvaIFRLtR90tydpJ7jRMTmwFDSVmLw3JkRlR1SjvT/JS42tTB9Vwlnzx/Hr57bS2tE32Ot/pH6tXGXnATbEWBx+TDFzci0XnDY713p79c1t/OgXK4j0J8Br5qTaYTBlC8OR1I+p5tYfXsWkCaNwHAdd13n6hXXc89fnB411BVU1Zfzo+59kwbxJSKnQdZ1X1mzhWz+8j8Zt+3MOQuVIOjojub+ZPnUMv7jlWs756LHEE0lu/tUKbvnNo/R39DC6pjQXTx8KupV8sKWT62/4Az/41sepqy3PDU54/Nm3iPbHOO74Ofz65muZPnVMZoIPwbMvvct//OddNDa6jtn1m3Zz6vGzmT9nPHNmNGBZdi7G694HXmb9xt3g85GLwxrxpR8BJDj8/PDIiMPqGfwBk/MXjkXXNNZu2MWO9gF8FV4mFABwQAixBdzOaANcEW1JGQsaPDavVCxZ36UhHcF9b+7hwnn1zBhdyuUnTOHHD6/N+Ds00GRevLUclGYqa5TmOfqyDyXU0IdDIHSdM46bQnGhH8dxSCYt7rn/ZSK9UQgEhjYScmWU91Y6krKKYn76/Ss5ZuFULMtG13UONnfwXzffT193X+57htfDNVedxdGLpuI4EsPQeWv9dr78rbto2tc66Ogd9mJLKVm8cBqGoROLJfnPm/7E7b97HCeWoKI0zMVnH5dhf3BM5atrtrDmra388o7HKCkqIBj08fDK17nrnqc5e9librvpc4ypr8rNGvPCqxv47Jd/wcGDHa6EzrQAAwFvxrZ3Xzpd13hz/Xbuvf+lwUjY4VJmuDQ6Ul8jMCTcJusuyXakZ/c1F9xlcxtYMq2WeCLFPQ+9QjJQyMwinfqgQipWKaUOZp9/0PMOaIJHZ5Xw6UmFYmZjt876ph7uen4L31m+gGuXzmTtzjZe2rAvY+/omdZMJtRXy7gesgFnCkDm6ejM8az3GEAJiosCzJlS61YKcKClk207DoLXlwmpzYqnYd7ijDopLi3kpu98grNOnY9t22iaRiqV5me/fJi31za6haMUSPjIwsmcu+wjKCURQqejM8IPbn4gZ5S7f+P+j6brVJQX5xgzDJ102uKHt/yV2+94DCeVBsfhsktPYcH8ybn7z6b2jh6wbB594nXWvL0NXdfoG4hz5WWn8b3rP0ZZaSHZGWJ27m7mum/fycED7WB63EqVkumT66kfVZ6LSBUCeiNRfnHH43R190NBeIiaOjxUOa+vcUTAtMFzQ8KZs/HzWs52m1JfxnfOnUvQZ3Lfwy/z1Pq9GPMWcVI1BAzaHcm9QuQ8sW5f4SWXXILh8ZBWorncy21n1WnpgFdDaTq/eGEbT76+jZqSELd8cjGzx1eR869o+TeiDx2aNGI89rBjCMqLQ5QVBXMx4slkGstR4PHkvTUjiOaMD+nH37mM889ahGVZ2LaNkpKHHnuNP/752SFvsjfg45LlxxEO+bFtByHgyVXreHvd9jw3hpb7j6KSAiZOGIWUTk5F33n3U9z660dyUE2eOoarP3N2pnsJkimL1rZuAObNnkhZeZHbHO/spbAwyK9+eg033/gZykoLc3HxqbTFz25/iM2b9wxClVFFlmVj23YmAtW9/oEVr/HK6i3gD7gvNALT66G4KDRUlQ3foh2hToZHmA7bCsGoyiJu/cRiZtSX886m3dx428Mka8bwkTo/R5WAUvxeCLE+f1qAnI1l2zZCgC7E/UeVcMwZdfqnHtmn6E4ZXHffGor9HhbPn8Tvrj6Ja373MusaW/LCSYZ1GUg1KJyG91XlXy8UPp+Jx6OTHXRQURamqqqEgdaBwQIZPjujVDQ0VPP965az9ITZuTfaYxis27CDG39yH7FY0oUT1+E6Y1o98+eMI21Z6LpONBpn1YvvoCzbfY5cK1WA7TB79gTGj6vBtl177alVb3PjT+4jFU+5atVn8sXPn8f4cbU5dfbok2vYs/cQ37n+YxyzaBr33nE9r7+1lYb6Kk45cS5jRleSnTTNFY4a697dxiMrX3PVn6bnpI/h9XLOGYsI+L1u3QBbG5u4449P4wg9517wek0+d/Fi5k6t44bfrmJvc8/hUGnDQRp2LhuyPDwLjbG1JfziyuM5fU49O3Y386Vv38kux0/D1NFcUC/waepFW3F71i2bDZvJtVEvvvhiQJBWJIMG3ztzlHhxSa0BpoddaZPP/mYVL6/Zyvzxlfz5S6dy7uJJaB7P0BDX7FbXQegZFamP8ECDOW27E5+5DURFaUmI85ctxPR7yYWa5HXU+vw+zjzlKO646UpOOXZ6Lr5cKcWB5g6+8/172Lun1Q2PzkhQzfSw9OQ5BP0mlmXhODY9vQM0HWjPsxMHK8AM+bnsoiUE/CZKSba8t5dvfPcuujsiObfHKSfN4+LlJ6KUa3jvP9DGTTf/lSeeeZOu7j4MQ+eMUxfwo+9eyWcuP4Mxoyvp6u4jkUzluqikdHj62bfp7e53HdBZABQsPWkOF5y1MCexYokkv7nrSQ4c6IRg0L3PDFRXnreQkClQiUSm3IeNNxweQZpfN/kRpPlRpbrOMTPq+PMXT+WseWPY0tjEZ79yO2ta4lTMn8WVkzzU+Nkm4ToBHVm1fpjEyiapwFKipczLNRc3aH9Qyjjm5RYfjXHFFbc/w486erngzEX8/pqTmTuugl8+8S7tHX3uDQ0xDh1XSMnhQmeoZ767P0lkIElhyOc2uRWcffJMPF6Tx17cQkt7HzIT9z59fDVnHD+dxfMmUBDyk0qlM4NCdSJ9UW740X28tmaLa/Treq57aPHCSSxZPIVUOo2u65mhXDZ6vgpk8N4uOvc4Tl0yh3Q6TTSW4oYf3su29/bnWqhlVcV89drlhAtctSql5LZfPcyWLXvQ/T5+8NO/8PUvX0h5WSFSKto6eln1/DpWvbCOn/3oKkaPKkdKVw1u2rybXCw6bmfwtGn1fOPzZxP0mxlNInj+xXd5/Km3IBQCw4PXa3LVhUfzyXPm09bWw49veYR9u1uhvOrIpkd+7PsQqPKA0jQKwgE+edI0vnb2UYwqDfHCq5v42vf+wMaIpPLYRVw108+UQrVLSvU5YJPP5yOdTh85NPmiiy7igQcewHHh2lEd4MrLxuu/CXnESc8cFDRpGp+7ezWbdhzkS5efyjfOm8exU2q45bH1rFq/j1QsCZ68h3FEJkYr0zqUIkNa1lAWdPWn2LSznTE1xbjKEExD59wl01mycDzdvTEcqSgq8FNaHMLn9bj2TDKFpml4DINIX5Sf3PowK1a+7toe2e4nJZk/dzz/cdXpFIUDOVtFSonf52HBURPYsa+drD/NME3OO2MBX//SeXg9rrF+5++f4Mmn33KhMlyVfeVlp7DgqAmkUmkMXefZ59Zy95+eBcODo2n86vdP8uqrG5gyaTSpVJptjU3s3NPCuAl1aJrAsiwXrGSKeDwxKNWBuUeN46brL2RiQyXptIVSiq7uPn5715PEUw6EAhQUBPjchUdzydLZ9HT385NbV7D23b1QWTVM7Y1gT2kZqIYPqNA0NK/J0VNrue7so1h61BgSiRS33bGSn/7mMdoKyhl38hw+Nc3PpLDa7Eg+D6zRNO0wqMCdcnpIevjhh7ngggtw2ziiu9gUL04o1MqKvNr0/Qldi3gKWLO9hbWr36EiaHL0zHrOnNvAxNpiuuJp2iIJ14bIN7SHzLoy1LhXUtEXTXDsrDoCPnNIx4LPNCgpDFBaFCDgc2FxHJnXShJ0dEb4yc0P8eCK1ahg0HUWZv77+I9M4lvXnEF9bSnvbtrLvqYORo8qyw0Jmzi+Gik0hKEzZVIdV338JD5z2ckUhv1IpXji6bf44Y//SsKW4HNV89yjJvD9r11EwO9FSsmhti6u+/od7NnXBsEA6G6jp729l61b9rB9x0G6BxLg9RIoCXP+GQsJBTJ2k1BEeqNs2n2IkvISLly2iBu+dA6TGipJpa0MgA6vrt7CXfesQoWLGD22mq9dfjxnL5lKT88AP73tUV54ZQtUVEIwPEwN5qk8TcsN/MCTt9V1hOlhxrhKrj93Lt+7cAEzR5ewfuMuvv69P/CrB19lYNwUjjlhJp+d7qMhxLNSqc+CelfLjGQfDhV5Nc5IgAHomiBoiGDc4fNbe9VXH9lnV2zstFGRCIVdLSyfVc2nLjiOqZNG0RNN8+y7Tdy/egfrdrSRjCYz8VpOXjCgMzTUJtO/97Hjx/L5Sxfj83ncSdDeJ2Vj33fsbOFXdzzFmrd3QDjsSquM01UAX//0SVyybD5vvL2Dm372MNU1pdz8w8szviGVc1omUzZer4eg35eZOE3j1dXv8d0b/0xrVz8UhAAIBn38+r+u4Iwlc9zoB+CW2x7mZ7c97F7j8eS9QENuGBB4vR7++NNPc9rxM7EsN4w3lbbYd6ADj8egtqoIXRMcau2mIORH1937277jIHfc+wJlo6s5e8k0JtWXc6Cli1tuW8krr2+DiioIFx1ZUg2XUhmVZ/hNpo8p58JjxnPOggZqS4M0NXdx3wMv86dHVtNmhCiZP5tzZpVzUrWIBjR5py35iVKy84ILLuCRRx75xwasZtOKFStcX4sQ/Hyrxbdmm8e3JfnPVw45S545aIv2SAraWxmV7OH8hQ0sP2MBE8ZWM5CwWLO9lcfX7uf19w7R0RMdDA4cAlg23kpipBOcN6+Gyy9aTHV1Sa4DPL9usiB0dPaz6vkN/O2h1bR29kNJsev3ytpJma6b0SV+jptRx6oXNtAZieMpLuQLHzuOS5cfja5rud8XGaerrmskkxbPPPcuv7nzaTojcQgXuAa7VFx6zke44cvn4PEYGIbO2vU7ueqa2+mKpV2whneZiGFbqTjr2Knc/N1L8fu9uUaHyBgBlmXz6utbuffPL/LNr51PfV05duYa16HrRs1u3Lyf2375OJveOwhVNVBQOMyeep+h9oZOYTjAgkmVfHT+GE6YXkNF2E9zazePP7OWvz26hh09KYyp01g4t4Fzx3kZF1TvgPqhrcTjQkkna6iff/75R375+Tvp5ZdfJhKJAK6GKTC1kpjNFbv71TUvNNsNq1st+iNxaG1hlBXhjDmjWXbSbKZOGgWaxs5DEV7ZcogXN7ew/WAP0YGkG7aSCxbM2+/rYXxIccrxU5l/1HgqK4vxeg2kVERjSVoOdbNhw15Wr3mP3fs6UP4AFBcPRkzmh/WAO9Cju8cV+eEwaIKAY3HJ0tmcs2whlRVF7nB5qYjFU2zf0cyKx97k5de2khZ6BiodpKKyvICff3M54+srAIgn0nz3+/fx2huNUF6a8SkxFK7D9gWGleITS+dw+WUnUVbqSsJ02qa5pYvHn1rLQ4+8TiSluOZzS7nigqMxDC3zaIr2jj6eWfUOf3toNR19KaiqhWAoz0+Vbd1mJwbRc+4EX9BkXE0Rx0+r5qQZtUwfXYxpaOzd38EzL7zD46vWs7MnBWMnMHXOeM6aWMDcEtr9uvqjLeVvpFTN2VZfPB7nsssue3+t8vfAyqaVK1dm9hQxW1AV0Cb3W1zdGJEXv9RsVaxtt+jrjUHbIUpi3RzdUMzpi6cwb844SopC9CfSbDsYYc32dtbt7GBXSx8D0eGQSYj2Q3cHQdIUFXjxmR4cKYnFkvQNJN1ptv1BKCx0B8jmS6nh8WIoF9z8Hnrbht5e6koDTJpQS2lJiGTKYn9TB7v3tBJL2i5QgUCu6Q8QMDUaghqaO3qPRCLN3qZ2nMKijArOL9H8CIF8qSVAOoiebibUFDFr5hiCAR+tbT1s2drEoY6MN724lJCwWTZ/NHNmNWDbku07m3njzUb2NHWiCoqhvDITf5XfBZOXDR1/0EtDZZi548tYPLmSmfXFlBT4iMWSbN52gFUvbuSVt3fQmtYQY8cxccZYTp0QZmG51lfkkSsdqX7lKShZl+zrJpFIEAwGOeeccz4QLx8YrGx6/PHH3T4rTTChyNAOxdVRfWk+tbtfnvP6IavqrbY0bb0J6OjA093GpKBi8fRRHLNgAhPGVhEM+BhIWOxu62fjvh427O1mR3MfnZE4VjIz/aNjQyoBibgbQ6Uy/ZKm6WbdyAMoP3p1hM9DwMsccxyIxSAec0FDuFLN73dz/no8WW0sJfT3ud/NFpvXdJv/Q/rejgBWfie948JNIu7em25AIOhKVa+fbLwYne1oyRhIiUSDYAGUlLnTGGSdqdlWoK6jez2UFvoYV13I7IYS5jQUM6mmkOKQl3TaYv+BLt5ct4PX3tzO1qZuEqFivOPHMX1qHUsagswuEb1FHvWMUvL3Dtrr0rEtXXfjzc4+++x/iJN/GCxwh+IHAoFMX5dgQpFHa4vLmVGbiw9G5bnrO+wJb7amxa7uFKmeCHS0URDrZVKxyaLpo5g3awwN9ZWEC3ykHUVHJMHutgG2NPWy/WAfTR0DdPclSSYzYxZHHHeYN2Dj/cAacjyfFIaGHudCf4Zdk/14WEkdoeiGxC8Niy4YweYCBo3r4dINXHUOYHhcN0pW3ek6Hq+HkrCPuooQk2rDzBhdzMSaMNXFfnwenXgixYHmbjZs3seb63fx3t5OepQJtaOomFjPvPFlHFvrZUJYNAd1+ZSS6j4H8bbjOFZW7Qkh/uHZ/P7bYGXTk08+mZs2UhOCXX0O8yrN+qQjzuxKqvN3Ruz5a9utgo0daVp6EzjdvdDZTijRR0NYZ/b4SmZNHcWEsVVUlIXxmAZpS9I1kOJAZ5Q9bQPsOtRPU3uUjt44kWiKVMoCe4QBHIdJpyMAlrsmn5ojAMXhlx25xMSw88MgGg7M8FCjIZ/zthm15jF1wkEvFUV+6ipCTKguYHx1mPryIOWFPvymjmNLunuj7NnXzub3mti49QC7WiJEhAkV1RQ0jGLK2AoWjQoyq0xPVnrVJlOoxxylVmoe745kIi6zDmTDMFi6dOl/m43/EVjZ9Oyzz2bqKxM2IgSlfj1kSebFbJa1x+XJ23vtyRs6LPO9rhQtvQmsnj7o7sLo76FCtxhbHmDq2EqmTqihvq6M0pIQPp+JVBBP2fREU7T2xGnpjnOwK0ZzZ4z23ji9AykG4mlSKTdWCScrzRgK3PCtGnZsePz9f7cIP4jEGikuStPQDB3T1An5TYoKXIhGlQWpKwtSWxqgpsRPWdhH0Gega4J02qY3EuNgSzeNO1rYtqOFXQe6aI3aWL4QVFVTNLqaCfVlzKkJMKvMcEYFxN6ALl9GqSccqdakLKtX190pDKSU6Lr+vzLn+/8KWPlp1apV+P1+kskkmhA0xxyml3orbcXCmM2pnUl5/J6IPX5rt+V7ryvF/p4k/ZEY9EagtxtPvJ9S3WZUkY9xtcWMrS+nflQZ1ZVFFIb9eL0ehBBYjiSesumPW3T1J+nuT9HZl6QjkqB7IEXvQIpINEU04a46kUq7iwXYjhxhWu48oEZkanhk4Ugfj2Cwa5lFBbLzkXp0/F6DoN9DYcikOOSlJANRRaGP0rCX0rCPooC7qoVpuP1h6bRDfzRBe0cfB5q72be/nd37OzjQ3k9nwiHl8UNJGUZVBdW1ZUyuLWRGhY/JxbpV7Rf7A7paI5R8zlGsCYYLD/Z0dytd1wkGgySTSU499dT/VQ7+18HKTy+88EKmfN1lQq5/vY87Ty6tlIjZSUcd15dWx7ZEnSl7IlZpY3da7OpJc6gvSV9fDCL90BeBgT4CdoISE6oKfdRVhKmrKaamqoiK8kKKi4KEgl68Xnf6IBA4UmE57ozI8ZTjLm+SsN3lTbLbpE0s6S6Dkki5izqlLIe05XaKO9IdNOAOElY5/kT2eTR3nlFdE+i6wDR0TI8Ljs/UCXgNgr7MihQ+F6SQzyDk97jHvQZ+U8djuNAJFI7j9h/G42kifTE6uvppbYtwsKWbg4d6aO2O0hWziOJxDfjiUsyKMsorixlTWcDUCj+Tij3UhUSkxGSXqak1KPWqLeU7hulricViUtfd6QmyvqglS5b8U+r+nwpWNr300ku5yMqs19xRUBowCkCMtRVHJR2OjqTknI64M2Zfn12yJ5IW+3rTHIik6B5IkuiPQTQKAwMQG0BPxQkqi7BHUBzwUF7op6I0RFlJiNLiECXFIcJhP8GAl4DfxGu6Tk1d1xCaGDr/ihqcMdkdKpnZl+TNH5pXaBn15gZEZKb7FuTW2BF514A7m7IjJbYtSaczk+PGUwwMJOnpjdLdE6W7Z4D2LndG5t6BFH1Jm6jUsA2f66sqLEQvLqKwtJDqsgLGlPoZV+JlXKFBTVCLFHo44NPUZk2oN5RU623F7t7eSG8gGETTNEzTzESg6v+ShZr+JWDlp9WrV7sVqoYuKTe3Oij29KaKEKLeUWKmJZkds9WM3qQzviPulLcM2MH9fWkO9KVpG0jTOZCiP5Z0O75jcdd1EI+74wbTSbzSwickfp3M8nEZ6REwKQj5CPpNAn6TQMDE5/XgzWSPoWHoGrquo2dmN87eYza0x22Uqsx0kK6Es2yHVMomlXLtvXgiTTyeIpZIE40m6Y+5+7GExUDSJp52SEpBUmRmC/T5XZdDKIRWECQYDlFSHKSq0E9NoY8xRSajwwaVAS1R4hXdAZ29Hk1tQcpNSqlNjlJ7NcPsicVi0o34MPB4PLkAwX/Vql8fGljD0xtvvJHrqhmydqGCEr/h14SoRIh6qcQESzEt5agJMUvVR5JOVWfCCXfGbLMtanEoatEVs+iOWfTG0wzE0ySTadLJNCqVglQaUil3PgbLcmfVy86u7DhoSg7N4vDFoN2kBkMOM9HYEuFmIZDZeCc9E4/uybgJTBO8XvD5MAI+vH4vgYCXwqCP0pCX0pBJZcikpsCgImBQ5tesYq8YCHlEhyk4qCP3CNRWpdQOKdV+W6rW3r6+mN/vdyd0MQYNcE3TOProoz/Uev3QwRqe3n33XbLzLA1fbdVWUOwzTE3TCoUQVQjqFKLOUTTYkjFpSVXSVuXRtFPSn3JCA2nH25twPL0Jd2XV3qRDJJFdmNJxpYaVWcwyu4ycI3EclZuQNrfmX16BZW1GLWNfaZor5UzDXQfR69HxmzpBUyfoNQh79cxKqgaFPp1iv07Y1O0CU0uFPCLuN0SvR6PTEKpdKNkkUHulVE2O4zRLRatURJoPtSZLS0sAd/CLYRi52LJ/xyV8/+3AOlLauHHjYVItPwdMXRMIn6aJkKZpJUAJilKJqlKKcqlUiSMpt6QqcZQKWY4KWo4KWVIGbKlMy1GmJZXhSGXYUmm2VLojlcgM/ha5sRaZaB8N0DWhDE1IXeAYGo6hCdujibShibShkTA0ETUEcV0Q1QS9mqATpbqFoAtok1J2SaV6HKl6pFQDQtOSA9GYnb8CSFYSZZ8zmUyycOHCD7s6/m76PwPWSOm9994bvvLYoD00QjINTSilDE0IQ4AJ+ED5UHgReFH4FMqLwqNQHhQedzxbdiRCZr0KN0lUbvX6tFLKUkolFSSBlCNlUkFSKSxHKsuRjq1puszGu8OgnZm1ifK93VJKpk2b9mEX8X87/Z8G64OkHTt25Gy4kVL+8bzJfQ87nr8m9PDzw38n2xvhTsar5Y7ln5s4ceKHXTT/1PT/ABNXrgiGFC1rAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTEyLTE3VDA0OjQzOjE1KzAwOjAwdf2BnQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0xMi0xN1QwNDo0MzoxNSswMDowMASgOSEAAAAASUVORK5CYII=">
                        </td>
                        <td colspan="4" >
                            &nbsp;
                        </td>
                    </tr>

                    <tr class="bordered">
                        <td colspan="8" style="font-size:9px;text-align:center;" >
                            <b>SUC. 1835 EMPRESAS QUERETARO ALAMOS</b>
                            <br/>
                            <b>CTA. No. 00134341260</b>
                        </td>
                        <td colspan="4" style="font-size:9px;text-align:right;">
                            <br/>
                            <hr/>
                            <center>FIRMA(S)</center>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table style="width: 100%;">
        <tr>
            <td style="font-size: 11px; text-align:right" >No. <?php echo utils::folio($cheque['folio'],7)?>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr >
            <td style="padding: 0px 15px;" >
                <table class="table_rd" border="0">
                    <tr class="" >
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;border-left: 1px solid #004381;font-size:9px;padding-bottom:0px;"><b>CONCEPTO DEL PAGO</b></td>
                        <td colspan="1" style="width:300px; border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;padding-bottom:0px;"><b>FIRMA CHEQUE RECIBIDO</b><br/><br/></td>
                    </tr>
                    <tr class="" >
                        <td colspan="1" style="border-left: 1px solid #004381;border-right: 1px solid #004381;border-bottom: 1px solid #004381;"><?php echo $cheque['referencia'] ?></td>
                        <td colspan="1" style="border-right: 1px solid #004381;border-bottom: 1px solid #004381;"><br></td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td >&nbsp;</td>
        </tr>
    </table>
    <table style="width: 100%;font-size:10px;">
        <tr >
            <td style="padding: 0px 15px;" >
                <table class="table_rd" border="0">
                    <tr class="" >
                        <td colspan="1" style="border-top: 1px solid #004381;border-left: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>CUENTA</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>SUB-CUENTA</center></td>
                        <td colspan="4" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>NOMBRE</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>PARCIAL</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>DEBE</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>HABER</center></td>
                    </tr>
                    <?php if(is_array($asientos)){ ?>
                        <?php foreach ($asientos as $key => $value) { ?>

                            <tr>
                                <td colspan="1" style="border-left: 1px solid #004381;border-right: 1px solid #004381;"><?php echo $value['cuenta'].'<br/><span style="font-size:8px">'.$value['cuenta_descripcion']; ?></span></td>
                                <td colspan="1" style="border-right: 1px solid #004381;"><center>-</center></td>
                                <td colspan="4" style="border-right: 1px solid #004381;"><?php echo $value['concepto']; ?></td>
                                <td colspan="1" style="border-right: 1px solid #004381;"><center>-</center></td>
                                <td colspan="1" style="border-right: 1px solid #004381;"><?php echo utils::format($value['abono']); ?></td>
                                <td colspan="1" style="border-right: 1px solid #004381;"><?php echo utils::format($value['cargo']); ?></td>
                            </tr>

                            
                        <?php } ?>
                    <?php } ?>
                    
                    <tr>
                        <td colspan="1" style="border-top: 1px solid #004381;border-left: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;">
                            <center>POLIZA No.</center>
                            <br/>
                            <center><div style="font-size:15px;"><?php echo $cheque['PolizaNomenclatura_id'].$cheque['dia']; ?></div></center>
                        </td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>HECHO POR</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>REVISADA POR</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>AUTORIZADA POR</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>AUXILIARES</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center>DIARIO</center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center><b>SUMAS IGUALES</b></center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center><?php echo utils::format($total['abono']); ?></center></td>
                        <td colspan="1" style="border-top: 1px solid #004381;border-right: 1px solid #004381;font-size:9px;border-bottom: 1px solid #004381;"><center><?php echo utils::format($total['cargo']); ?></center></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>



</BODY>

</HTML>