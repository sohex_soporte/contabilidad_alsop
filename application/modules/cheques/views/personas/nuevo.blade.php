@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Alta de Clientes / Proveedores
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <form id="data_form">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="card-title mb-4">DATOS DEL CLIENTE</h5>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="tipo_persona_id" class="col-sm-2 col-form-label"><span class="text-danger">*</span> Tipo:</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" id="tipo_persona_id" name="tipo_persona_id">
                                                <?php if(is_array($listado)){ ?>
                                                    <?php foreach ($listado as $key => $value) { ?>
                                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['nombre'] ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <small id="msg_tipo_persona_id" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row" >
                                        <label for="rfc" class="col-sm-4 col-form-label"><span class="text-danger">*</span> RFC:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="rfc" name="rfc" />
                                            <small id="msg_rfc" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nombre" class="col-sm-4 col-form-label"><span class="text-danger">*</span> Nombre:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="nombre" name="nombre" />
                                            <small id="msg_nombre" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="apellido1" class="col-sm-4 col-form-label"><span class="text-danger">*</span> Primer apellido:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="apellido1" name="apellido1" />
                                            <small id="msg_apellido1" class="form-text text-danger"></small>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="apellido2" class="col-sm-4 col-form-label">Segundo apellido:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="apellido2" name="apellido2" />
                                            <small id="msg_apellido2" class="form-text text-danger"></small>
                                        </div>
                                    </div>
 
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="telefono" class="col-sm-4 col-form-label"><span class="text-danger">*</span> Telefono:</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="telefono" name="telefono">
                                            <small id="msg_telefono" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="correo_electronico" class="col-sm-4 col-form-label">Correo
                                            electronico:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="correo_electronico"
                                                name="correo_electronico">
                                            <small id="msg_correo_electronico" class="form-text text-danger"></small>
                                        </div>
                                    </div>                                   
                                    <div class="form-group row">
                                        <label for="domicilio" class="col-sm-4 col-form-label"><span class="text-danger">*</span> Domicilio:</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" id="domicilio" name="domicilio" ></textarea>
                                            <small id="msg_domicilio" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class=" col-sm-12">
                            <button class="btn btn-primary ml-2 my-2 my-sm-0 float-right" onclick="APP.guardar();"
                                type="button"><i class="fas fa-save"></i> Guardar</button>
                            <a type="button" href="<?php echo site_url('cheques/personas'); ?>"
                                class="btn btn-secondary ml-2 my-2 my-sm-0 float-right"><i class="fa fa-times"></i>
                                Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<?php $this->carabiner->display('select2','css') ?>
<?php $this->carabiner->display('sweetalert2','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<script>
    var identificador = "<?php echo $identificador; ?>";
</script>


<?php $this->carabiner->display('datatables','js') ?>
<?php $this->carabiner->display('select2','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/personas/nuevo.js'); ?>"></script>
@endsection