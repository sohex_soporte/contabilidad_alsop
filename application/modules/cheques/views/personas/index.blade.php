@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Administrador de Clientes / Proveedores
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title ">Listado de clientes/proveedores</h5>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class=" col-sm-12">
                        <button class="btn btn-sm btn-success ml-2 my-2 my-sm-0 float-right" onclick="APP.agregar();"
                            type="button"><i class="fas fa-plus"></i> Agregar</button>
                    </div>
                </div>

                <div class="row">
                    <div class=" col-sm-12">

                        <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-sm-12">
                        <hr/>
                        <a type="button" href="<?php echo site_url('cheques'); ?>"
                            class="btn btn-secondary ml-2 my-2 my-sm-0 float-left"><i class="fas fa-chevron-left"></i>
                            Regresar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<?php $this->carabiner->display('datatables','js') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/personas/index.js'); ?>"></script>
@endsection