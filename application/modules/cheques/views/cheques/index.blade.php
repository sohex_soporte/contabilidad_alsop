@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Administrador de cheques
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <form id="form_filter" type="get" href="<?php echo site_url('cheques/index'); ?>">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="card-title mb-5">Filtros</h5>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="folio" class="col-form-label">Folio:</label>
                                <input type="number" class="form-control" name="folio" id="folio" value="">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="nomenclatura" class="col-form-label">Nomenclatura:</label>
                                <select class="form-control" id="nomenclatura" name="nomenclatura">
                                    <?php if(is_array($nomenclaturas) && count($nomenclaturas)>0){ ?>
                                    <option value="">Todos</option>
                                    <?php foreach($nomenclaturas as $nomenclatura){ ?>
                                    <option value="<?php echo $nomenclatura['id']; ?>">
                                        <?php echo '['.$nomenclatura['id'].'] '.$nomenclatura['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="fecha" class="col-form-label">Fecha de inicio</label>
                                <input type="date" class="form-control" id="fecha_ini" name="fecha_ini"
                                    value="<?php echo $fecha_ini; ?>" max="<?php echo utils::get_date(); ?>">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="fecha" class="col-form-label">Fecha fin</label>
                                <input type="date" class="form-control" id="fecha_fin" name="fecha_fin"
                                    value="<?php echo $fecha_fin; ?>" max="<?php echo utils::get_date(); ?>">
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class=" col-sm-12">
                            <button class="btn btn-primary btn-sm ml-2 my-2 my-sm-0 float-right"  type="submit"><i class="fas fa-search"></i> Buscar</button>
                            <button class="btn btn-secondary  btn-sm ml-2 my-2 my-sm-0 float-right" onclick="APP.limpiar_busqueda();"
                                type="button"> Limpiar</button>
                        </div>
                    </div>
                </form>

                <hr class="mt-3" />

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title ">Listado de cheques</h5>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class=" col-sm-12">
                        <button class="btn btn-sm btn-success ml-2 my-2 my-sm-0 float-right" onclick="APP.agregar_cheque();"
                            type="button"><i class="fas fa-plus"></i> Agregar cheque</button>
                        <button class="btn btn-sm btn-secondary ml-2 my-2 my-sm-0 float-right" onclick="APP.administrar_personas();"
                            type="button"><i class="fas fa-users"></i> Administrar Clientes / Proveedores</button>
                    </div>
                </div>

                <div class="row">
                    <div class=" col-sm-12">

                        <!-- <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table> -->
                        <div class="table-responsive">
                        <?php echo $tabla; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="modal_pdf" tabindex="-1">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Documento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection




@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<?php $this->carabiner->display('datatables','js') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/cheques/index.js'); ?>"></script>
@endsection