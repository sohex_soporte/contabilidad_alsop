@section('contenido')
<script>
    const datos_cheque_id = "<?php echo $datos_cheque_id; ?>";
    const polizas_id = "<?php echo $polizas_id; ?>";
    const transaccion_id = "<?php echo $transaccion_id; ?>";
</script>

<h4 class="mt-4 mb-4 text-gray-800 text-center">
    CHEQUE <br />
    <small>FOLIO: <?php echo $cheque['folio']; ?></small>
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title mb-4">DATOS DEL CHEQUE</h5>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="rfc" class="col-sm-4 col-form-label">RFC:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext" id="rfc" name="rfc" disabled
                                            value="<?php echo $cheque['rfc']; ?>" />
                                        <small id="msg_rfc" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nombre" class="col-sm-4 col-form-label">Nombre:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext" id="nombre" name="nombre" disabled
                                            value="<?php echo $cheque['nombre']; ?>" />
                                        <small id="msg_nombre" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="apellido1" class="col-sm-4 col-form-label">Primer apellido:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext" id="apellido1" name="apellido1" disabled
                                            value="<?php echo $cheque['apellido1']; ?>" />
                                        <small id="msg_apellido1" class="form-text text-danger"></small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="apellido2" class="col-sm-4 col-form-label">Segundo apellido:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext" id="apellido2" name="apellido2" disabled
                                            value="<?php echo $cheque['apellido2']; ?>" />
                                        <small id="msg_apellido2" class="form-text text-danger"></small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="telefono" class="col-sm-4 col-form-label">Telefono:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext" id="telefono" name="telefono" disabled
                                            value="<?php echo $cheque['telefono']; ?>">
                                        <small id="msg_telefono" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="domicilio" class="col-sm-4 col-form-label">Domicilio:</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control-plaintext" disabled id="domicilio" name="domicilio"><?php echo $cheque['domicilio']; ?></textarea>
                                        <small id="msg_domicilio" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="correo_electronico" class="col-sm-4 col-form-label">Correo
                                        electronico:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext" id="correo_electronico"
                                            name="correo_electronico" disabled
                                            value="<?php echo $cheque['correo_electronico']; ?>">
                                        <small id="msg_correo_electronico" class="form-text text-danger"></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <hr />
                    </div>

                    <div class="col-sm-12">
                        <h5 class="card-title mb-4">POLIZA</h5>
                    </div>
                    <div class="col-sm-12">

                        <div class="form-group row">
                            <label for="nomenclatura_id" class="col-sm-2 col-form-label">Poliza:</label>
                            <div class="col-sm-10">
                                <input class="form-control-plaintext" id="nomenclatura_id" name="nomenclatura_id" disabled
                                    value="<?php echo $cheque['PolizaNomenclatura_id'].$cheque['dia']; ?>" />
                                <small id="msg_nomenclatura_id" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_poliza" class="col-sm-2 col-form-label">Fecha:</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control-plaintext" id="fecha_poliza" name="fecha_poliza" disabled
                                    value="<?php echo $cheque['poliza_fecha_creacion']; ?>">
                                <small id="msg_fecha_poliza" class="form-text text-muted"></small>
                            </div>
                        </div>

                        </form>
                    </div>

                    <div class="col-sm-12">
                        <hr />
                    </div>

                   
                    <div class="col-sm-12">

                        <div class="form-group row">
                            <label for="referencia" class="col-sm-2 col-form-label">Referencia:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control-plaintext" disabled id="true" name="referencia"><?php echo $cheque['referencia']; ?></textarea>
                                <small id="msg_referencia" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="comentarios" class="col-sm-2 col-form-label">Comentarios:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control-plaintext" disabled id="true" name="comentarios"><?php echo $cheque['comentarios']; ?></textarea>
                                <small id="msg_comentarios" class="form-text text-muted"></small>
                            </div>
                        </div>

                        </form>
                    </div>


                </div>

                <hr class="mt-3" />

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title ">LISTADO DE ASIENTOS</h5>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class=" col-sm-12">
                        <button class="btn btn-sm btn-success ml-2 my-2 my-sm-0 float-right"
                            onclick="APP.modal_agregar_asiento();" type="button"><i class="fas fa-plus"></i> Agregar
                            asiento</button>
                    </div>
                </div>

                <div class="row">
                    <div class=" col-sm-12">
                        <?php if($acumulado_abono <> $acumulado_cargo){ ?>
                            <div class="alert alert-danger" role="alert">
                                Los montos de cargos y abonos con coinciden
                            </div>
                        <?php } ?>

                        <div class="table-responsive">
                        <?php echo $tabla; ?>
                        </div>
                        <!-- <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <a type="button" href="<?php echo site_url('cheques'); ?>" class="btn btn-secondary mt-4 mb-3"><i
                class="fa fa-arrow-left"></i> Regresar</a>
    </div>
</div>


<div class="modal fade" id="modal_agregar_asiento" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="modal_agregar_asientoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_agregar_asientoLabel">Agregar asiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="modal_agregar_asiento_button" class="btn btn-primary" onclick="APP.modal_agregar_asiento_guardar();"><i
                        class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<?php $this->carabiner->display('select2','css') ?>
<?php $this->carabiner->display('sweetalert2','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')

<?php $this->carabiner->display('datatables','js') ?>
<?php $this->carabiner->display('select2','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/cheques/asientos.js'); ?>"></script>
@endsection