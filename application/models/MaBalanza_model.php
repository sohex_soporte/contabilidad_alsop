<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MaBalanza_model extends CI_Model {

    public function get_list($where = false)
    {
        $this->db->select([
                'id', 
                'cuenta_id', 
                'cuenta', 
                'mes', 
                'anio', 
                'saldo_inicial', 
                'cargos', 
                'abonos', 
                'saldo_mes', 
                'saldo_actual', 
                'ultimo_asiento_aplicado'
            ])
            ->from('ma_balanza')
            ->where('ma_balanza.deleted_at IS NULL',null, false);
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getList_anios(){
        $this->db->distinct();
        $this->db->select('anio');
        $this->db->from('ma_balanza');
        $this->db->order_by('anio','asc');

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getList_mes($anio){
        $this->db->distinct();
        $this->db->select('mes');
        $this->db->from('ma_balanza');
        $this->db->where('anio',$anio);
        $this->db->order_by('mes','asc');

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getList_balanza($mes,$anio){
        $database = $this->load->database('default', TRUE);
        $query = $database->query("call pa_obtener_balanza('".$mes."','".$anio."');");
        $database->close();
        return $query->result_array();
    }

    public function getList_balanza_simple($mes,$anio){
        $database = $this->load->database('default', TRUE);
        $query = $database->query("call pa_obtener_balanza_simple('".$mes."','".$anio."');");
        $database->close();
        return $query->result_array();
    }

    public function aplicar_asiento($_cuenta_id,$_cargo,$_abono,$_asiento_id){

        
        $database = $this->load->database('default', TRUE);

        $_cuenta_id = $database->escape($_cuenta_id);
        $_cargo = $database->escape($_cargo);
        $_abono = $database->escape($_abono);
        $_asiento_id = $database->escape($_asiento_id);

        $query = $database->query("call pa_aplicar_movimiento_balanza($_cuenta_id,$_cargo,$_abono,$_asiento_id);");
        $database->close();
        return $query->row_array();

    }

    

}