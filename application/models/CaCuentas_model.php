<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaCuentas_model extends CI_Model {

    public function get_cuentas_asientos_list($where = false)
    {
        $this->db->from('vw_cuentas_asientos_no_bancos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function get($where = false){
        $this->db
            ->from('cuentas_dms');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('cuentas_dms');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('cuentas_dms.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}