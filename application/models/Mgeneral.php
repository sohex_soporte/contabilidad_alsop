<?php
/**

 **/
class Mgeneral extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    

    public function save_register($table, $data)
      {
          $this->db->insert($table, $data);
          return $this->db->insert_id();
      }

      public function delete_row($tabla,$id_tabla,$id){
		   $this->db->delete($tabla, array($id_tabla=>$id));
    	}

      public function get_table($table){
    		$data = $this->db->get($table)->result();
    		return $data;
    	}

      public function get_row($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->row();
    	}

      public function get_result($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->result();
    	}

      public function update_table_row($tabla,$data,$id_table,$id){
    		$this->db->update($tabla, $data, array($id_table=>$id));
    	}


      function check_fecha($x) {
        if (date('Y-m-d', strtotime($x)) == $x) {
          return true;
        } else {
          return false;
        }
    }

     

    public function factura_subtotal($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal;
    }

    public function factura_iva_total($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal*.16;
    }

    public function factura_descuento($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $descuento = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

    public function factura_iva(){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $iva = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

      

      
}
