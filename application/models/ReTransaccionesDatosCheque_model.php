<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReTransaccionesDatosCheque_model extends CI_Model {

  
    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->from('re_transacciones_datos_cheque');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('re_transacciones_datos_cheque')
            ->order_by('id','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('re_transacciones_datos_cheque.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents,$id = false) {
        $uuid = ($id == false)? utils::guid() : $id;
        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('re_transacciones_datos_cheque', $contents);
        return ($response)? $uuid : false;
    }

}