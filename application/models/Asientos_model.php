<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Asientos_model extends CI_Model {

    const ESTATUS_POR_APLICAR = 'POR_APLICAR';
    const ESTATUS_APLICADO = 'APLICADO';
    const ESTATUS_ANULADO = 'ANULADO';
    const ESTATUS_CANCELADO = 'CANCELADO';
    const ESTATUS_CANCELADO_SIN_APLICAR = 'CANCELADO_SIN_APLICAR';

    public function get($where = false)
    {
        $this->db->from('asientos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('deleted_at IS NULL', null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        return $this->get_list($where);
    }

    public function get_list($where = false)
    {
        $this->db->from('asientos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('deleted_at IS NULL', null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        $this->db->set('created_at',utils::get_datetime());
        $this->db->set('updated_at',utils::get_datetime());
        $response = $this->db->insert('asientos', $contents);
        return ($response)? $this->db->insert_id() : false;
    }

    public function update(array $contents, array $where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::get_datetime());
        $response = $this->db->update('asientos', $contents);
        return ($response)? $this->db->affected_rows() : false;
    }

    public function update_entry()
    {
            $this->title    = $_POST['title'];
            $this->content  = $_POST['content'];
            $this->date     = time();

            $this->db->update('entries', $this, array('id' => $_POST['id']));
    }
    /*
    public function get_cuentas_abiertas($where = false){
        $this->db
            ->distinct()
            ->select([
                'cuentas_dms.id',
                'cuentas_dms.cuenta',
                'cuentas_dms.decripcion as descripcion',
            ])
            ->from('cuentas_dms')
            ->join('asientos','cuentas_dms.id = asientos.cuenta','inner')
            ->where('asientos.deleted_at IS NULL', null, false)
            ->order_by('cuentas_dms.cuenta','asc');
        if(is_array($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
    */

    public function get_total_cuentas_ingreso($where = false){
        $this->db
            ->select_sum('asientos.abono')
            ->select_sum('asientos.cargo')
            ->from('asientos')
            ->where('asientos.deleted_at IS NULL', null, false);
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : 0;
    }

    public function get_total_por_cuenta_origen($where = false){
        $this->db
            ->select('cuenta,origen_transaccion_id,sum(abono) - sum(cargo) as monto',false)
            ->from('asientos')
            ->group_by('cuenta,origen_transaccion_id')
            ->order_by('cuenta','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : 0;
    }

    public function get_total_por_fecha_origen($where = false){
        $this->db
            ->select('fecha_creacion,origen_transaccion_id,sum(abono) - sum(cargo) as monto',false)
            ->from('asientos')
            ->group_by('fecha_creacion,origen_transaccion_id')
            ->order_by('fecha_creacion','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : 0;
    }
    
    public function get_listado_ingresos($where = false){
        $this->db
            ->from('asientos')
            ->where('abono > 0',null,false)
            ->order_by('id','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : 0;
    }

    public function get_listado_egresos($where = false){
        $this->db
            ->from('asientos')
            ->where('cargo > 0',null,false)
            ->order_by('id','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : 0;
    }

    public function get_flujo_bancos($where = false){
        $this->db
            ->select([
                "asientos.id", 
                "asientos.fecha_creacion", 
                "asientos.referencia", 
                "asientos.nombre", 
                "asientos.concepto", 
                "cuentas_dms.cuenta", 
                "cuentas_dms.decripcion",
                "asientos.cargo", 
                "asientos.abono",
                "asientos.created_at",
                "asientos.updated_at",
                "asientos.deleted_at",

            ])
            ->from('asientos')
            ->join('cuentas_dms','asientos.cuenta = cuentas_dms.id')
            ->order_by('asientos.id');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : 0;
    }

    public function get_total_bancos($where = false,$fecha_fin = false){
        $this->db
            ->select_sum('abono')
            ->select_sum('cargo')
            ->from('asientos');
        
        if( $fecha_fin != false ){
            $this->db->where('asientos.fecha_creacion <', $fecha_fin );
        }

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : 0;
    }

    public function get_detalle(array $where = [],array $order_by = []){
        $this->db
            ->select('asiento_id, 
                poliza, 
                PolizaNomenclatura_id, 
                poliza_anio, 
                poliza_mes, 
                poliza_dia, 
                cliente_id, 
                folio, 
                estatus_id, 
                cuenta_id, 
                cuenta, 
                cuenta_descripcion, 
                precio_unitario,
                cantidad_articulos,
                abono, 
                cargo, 
                monto, 
                acumulado, 
                concepto, 
                fecha_creacion, 
                origen_transaccion_id, 
                vendedor, 
                fecha_asiento_aplicado, 
                fecha_validacion, 
                departamento_id, 
                departamento_descripcion, 
                transaccion_origen,
                fecha_registro')
            ->from('vw_asientos_detalle');

        if(is_array($where) && count($where)>0){
            $this->db->where($where);
        }
        if(is_array($order_by) && count($order_by)>0){
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key,$value);
            }
        }

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }
}