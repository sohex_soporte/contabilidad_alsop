<?php

use Illuminate\Database\Capsule\Manager as Capsule;
class Session
{

    public $CI;

    function __construct()
    {
    }

    public function check_auth($params)
    {
        $this->CI = &get_instance();

        // $capsule = new Capsule;
        // $capsule->addConnection([
        //     'driver'    => 'mysql',
        //     'host'      => $this->CI->db->hostname,
        //     'database'  => $this->CI->db->database,
        //     'username'  => $this->CI->db->username,
        //     'password'  => $this->CI->db->password,
        //     'charset'   => $this->CI->db->char_set,
        //     'collation' => $this->CI->db->dbcollat,
        //     'prefix'    => $this->CI->db->dbprefix
        // ]);
        // $capsule->setAsGlobal();
        // $capsule->bootEloquent();

        $acceso = $this->CI->input->get('acceso');
        if(in_array((int)$acceso,array(1,2))){
            $this->CI->session->set_userdata('acceso',$acceso);
            $this->CI->session->set_userdata('logged_in',true);
        }else{

            if ($this->CI->router->class != 'api' && isset($this->CI->type) && $this->CI->type != 'api') {
                $modulos_libres = $params['modulos_libres'];

                if (!$this->CI->input->is_ajax_request() && !in_array($this->CI->router->module, $modulos_libres)) {
                    if (!$this->CI->session->userdata('logged_in')) {
                        redirect('inicio/index');
                    }
                }  
                if ($this->CI->session->userdata('perfil_id') != 1 && $this->CI->router->module == 'administrador' && $this->CI->router->class == 'reportes') {
                    redirect('inicio/index');
                }
            }
        }
    }
}
