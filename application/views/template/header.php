<meta charset="ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<script src="<?php echo base_url('assets/components/jquery/dist/jquery.min.js'); ?>"></script>

<title><?php echo TITULO; ?></title>

<!-- Custom fonts for this template-->
<link href="<?php echo base_url('assets/tema/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css">
<link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

<!-- Custom styles for this template-->
<link href="<?php echo base_url('assets/tema/css/sb-admin-2.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/components/bootstrap-4.6/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" /> 
<link href="<?php echo base_url('assets/css/custom.css');?>" rel="stylesheet" type="text/css" />
   
<script type="text/javascript">
    var PATH = "<?php echo site_url(); ?>";
    var PATH_BASE = "<?php echo base_url(); ?>";
    var API_URL_DMS = "<?php echo API_URL_DMS ?>";
    var PATH_LANGUAGE = "<?php echo base_url('assets/components/DataTables/languaje.json'); ?>";
</script>

<?php echo $contenido; ?>