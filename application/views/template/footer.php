
<!-- Core plugin JavaScript-->
<script type="text/javascript" src="<?php echo base_url();?>assets/tema/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script type="text/javascript" src="<?php echo base_url();?>assets/tema/js/sb-admin-2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/general.js"></script>

<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/tema/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/components/bootstrap-4.6/js/bootstrap.min.js'); ?>" ></script>

<script type="text/javascript" src="<?php echo base_url('assets/libraries/jquery-loading-overlay-master/dist/loadingoverlay.min.js'); ?>"></script>

<?php echo $contenido; ?>

<script>
    $(document).ready(function () {
        $("body").LoadingOverlay("show");
        // Hide it after 3 seconds
        setTimeout(function () {
            $.LoadingOverlay("hide");
        }, 1000);
        $.ajaxSetup({
            beforeSend: function () {
                $("body").LoadingOverlay("show");
                setTimeout(function () {
                    $("body").LoadingOverlay("hide");
                }, 7000)
            },
            complete: function () {
                $("body").LoadingOverlay("hide");
            }
        });
    });
</script>