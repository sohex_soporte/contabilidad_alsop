<div class="nav nav-second collapse nav-cat-2 " id="level_one_7">
    <li class="nav-category">
        cxp </li>
    <li id="submenu_0">
        <a href="#submenu_031" data-toggle="collapse" aria-expanded="false">
            Movimientos<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
        </a>
        <ul id="submenu_031" class="nav nav-second collapse nav-cat-3">

            <li class="" id="vista_0_59">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/cxp/index/#">Cuentas</a>
            </li>

            <li class="" id="vista_1_60">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/cxp/index/relacion_pagos">Relación pagos</a>
            </li>
        </ul>
    </li>
</div>
<li id="menu_8" class="nav-category nav-cat-1 " style="padding-left: 0px;">
    <a href="#level_one_8" data-toggle="collapse" aria-expanded="false">
        <i class="fas fa-chart-pie"></i>&nbsp MONITOREO </a>
</li>
<div class="nav nav-second collapse nav-cat-2 " id="level_one_8">
    <li class="nav-category">
        Monitoreo </li>
    <li id="submenu_0">
        <a href="#submenu_032" data-toggle="collapse" aria-expanded="false">
            Otros<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
        </a>
        <ul id="submenu_032" class="nav nav-second collapse nav-cat-3">

            <li class="" id="vista_0_61">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/metricas/metricas/#">Recursos</a>
            </li>

            <li class="" id="vista_1_62">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/metricas/uso/#">Uso</a>
            </li>

            <li class="" id="vista_2_63">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/metricas/actividades/#">Actividad</a>
            </li>
        </ul>
    </li>
</div>
<li id="menu_9" class="nav-category nav-cat-1 " style="padding-left: 0px;">
    <a href="#level_one_9" data-toggle="collapse" aria-expanded="false">
        <i class="fas fa-user-tie"></i>&nbsp ADMINISTRADOR </a>
</li>
<div class="nav nav-second collapse nav-cat-2 " id="level_one_9">
    <li class="nav-category">
        Roles de Usuario </li>
    <li id="submenu_0">
        <a href="#submenu_08" data-toggle="collapse" aria-expanded="false">
            Roles/modulos<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
        </a>
        <ul id="submenu_08" class="nav nav-second collapse nav-cat-3">

            <li class="" id="vista_0_1">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/usuarios/usuarios/#">Admin modulos</a>
            </li>

            <li class="" id="vista_1_2">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/usuarios/usuarios/verpermisos">Ver permisos</a>
            </li>

            <li class="" id="vista_2_3">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/usuarios/usuarios/roles">Roles</a>
            </li>

            <li class="" id="vista_3_4">
                <a
                    href="https://iplaneacion.com.mx/dms/queretaro_dms/usuarios/usuarios/administrarUsuarios">Usuarios</a>
            </li>

            <li class="" id="vista_4_64">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/usuarios/usuarios/modulos">Modulos</a>
            </li>

            <li class="" id="vista_5_65">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/usuarios/usuarios/permisosVista">Permisos
                    vistas</a>
            </li>
        </ul>
    </li>
    <li class="nav-category">
        Catalogos </li>
    <li id="submenu_0">
        <a href="#submenu_07" data-toggle="collapse" aria-expanded="false">
            Listado<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
        </a>
        <ul id="submenu_07" class="nav nav-second collapse nav-cat-3">

            <li class="" id="vista_0_5">
                <a href="https://iplaneacion.com.mx/dms/queretaro_dms/catalogos/catalogos/#">catalogos</a>
            </li>
        </ul>
    </li>
</div>