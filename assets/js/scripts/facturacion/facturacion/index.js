class CTRL_APP {
    constructor () {
      this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    buscar_tabla(){
        let table =$('table#tabla_listado').DataTable();
        table.ajax.reload();
    }

    consultar_poliza($this){
        var id = $($this).data('piliza_id');
        window.location.href = PATH+"/facturacion/consultar/"+id;
    }

    formatMoney(money){
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta tabla",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            pageLength: 50,
            responsive: true,
            order: [[ 1, "asc" ]],
            'ajax' : {
                'url' : PATH+'facturacion/api/listado_facturacion',
                'data' : function(){
                    return {
                        'fecha': $('input[name=fecha]').val()
                    };
                },
                'type' : 'get'
            },
            'order': [],
            'columns': [
                { 
                    title: 'Fecha',
                    data: 'fecha_creacion',
                    render: function ( data, type, row ) {
                        var fecha = moment( row.created_at );
                        var html = '';
                        if(fecha.isValid()){
                            html = '<center>'+fecha.format('hh:mm')+'<br/><small>'+fecha.format('DD/M/Y')+'</small></center>';
                        }
                        return html;
                    }
                },
                { 
                    title: 'Folio',
                    data: 'folio',
                    render: function ( data, type, row ) {
                        return '<h6>'+((data != null)? data : '-')+'</h6>';
                    }
                },
                { 
                    title: 'Forma de pago',
                    render: function ( data, type, row ) {
                        return '['+row.forma_pago_id+']<br/>'+row.metodo_pago;
                    }
                },
                { 
                    title: 'Metodo de pago',
                    render: function ( data, type, row ) {
                        return '['+row.metodo_pago_id+']<br/>'+row.metodo_pago;
                    }
                },
                { 
                    title: 'Uso de CFDI',
                    render: function ( data, type, row ) {
                        return '['+row.receptor_uso_cfdi+']<br/>'+row.uso_cfdi;
                    }
                },
                { 
                    title: 'Receptor',
                    render: function ( data, type, row ) {
                        return '<small>'+row.receptor_rfc+'<br/>'+row.receptor_nombre+'</small>';
                    }
                },
                { 
                    title: 'Monto facturado',
                    render: function ( data, type, row ) {
                        return '<span style="FLOAT: RIGHT;">'+APP.formatMoney(row.total)+'</span>';
                    }
                },
                { 
                    title: 'Acciones',
                    render: function ( data, type, row ) {
                        console.log(row);
                        var consultar = '<button title="Consultar" data-piliza_id= "' + row.factura_id + '"  onclick="APP.consultar_poliza(this)" class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></button>';

                        return consultar ;
                    }
                }

                
             
                
              ]
        });
    }
  }
  
  const APP = new CTRL_APP();