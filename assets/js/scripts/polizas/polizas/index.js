class CTRL_APP {
    constructor () {
      this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    buscar_tabla(){
        let table =$('table#tabla_listado').DataTable();
        table.ajax.reload();
    }

    consultar_poliza($this){
        var id = $($this).data('piliza_id');
        window.location.href = PATH+"/polizas/consultar/"+id;
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: PATH_LANGUAGE,
            pageLength: 50,
            'ajax' : {
                'url' : PATH+'polizas/api/polizas',
                'data' : function(){
                    var fecha = moment($('input[name=fecha]').val());

                    return {
                        'anio': fecha.format('YYYY'),
                        'mes': fecha.format('MM'), 
                        'dia': fecha.format('DD')
                    };
                },
                'type' : 'get'
            },
            'order': [],
            'columns': [
                { 
                    title: 'Fecha',
                    data: 'fecha_creacion',
                    render: function ( data, type, row ) {
                        var fecha = moment( row.created_at );
                        var html = '';
                        if(fecha.isValid()){
                            html = '<center><h6>'+fecha.format('hh:mm')+'<br/><small>'+fecha.format('DD/M/Y')+'</small></h6></center>';
                        }
                        return html;
                    }
                },
                { 
                    title: 'Poliza',
                    data: 'PolizaNomenclatura_id',
                    render: function ( data, type, row ) {
                        return '<span>'+((data != null)? data+'-' : '')+row.dia+'</span>';
                    },  
                },
                { 
                    title: 'Concepto',
                    data: 'nomenclatura'  
                },
                { 
                    title: 'Acciones',
                    render: function ( data, type, row ) {
                        console.log(row);
                        var consultar = '<button title="Consultar" data-piliza_id= "' + row.id + '"  onclick="APP.consultar_poliza(this)" class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></button>';

                        return consultar ;
                    }
                }

                
             
                
              ]
        });
    }
  }
  
  const APP = new CTRL_APP();