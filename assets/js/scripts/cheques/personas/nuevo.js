class CTRL_APP {
    constructor () {
    //   this.cargar_tabla();
    //   this.cargar_clientes_dms();
    //   this.cargar_nomenclaturas_polizas();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    formatMoney(money){
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla(){
        let table =$('table#tabla_listado').DataTable();
        table.ajax.reload( null, false );
    }

    formatState(data) {
        var element = data.element;
        var html = '<small><b>'+data.text+'</b></small>';
        html += '<br/><span>'+$(element).attr('nombre_completo')+'</span>';
        return $(html);
    }

    formatNomenclaturas(data) {
        var html = '<small><b>'+data.id+'</b></small>';
        html += '<br/><span>'+data.text+'</span>';
        return $(html);
    }

    cargar_clientes_dms(){
        var select = document.getElementById('cliente_id');

        $.ajax({
            type: 'get',
            url: PATH+'/cheques/api/listado_clientes_dms.json',
            dataType: "json",
            success: function(data, status, xhr){
                //$('select#cliente_id').
                $.each(data.data, function( index, value ) {
                    var opt = document.createElement('option');
                    opt.value = value.id;
                    opt.setAttribute('numero_cliente',value.numero_cliente);
                    opt.setAttribute('nombre',value.nombre);
                    opt.setAttribute('apellido1',value.apellido_paterno);
                    opt.setAttribute('apellido2',value.apellido_materno);
                    opt.setAttribute('telefono',value.telefono);
                    opt.setAttribute('direccion',value.direccion);
                    opt.setAttribute('numero_int',value.numero_int);
                    opt.setAttribute('numero_ext',value.numero_ext);
                    opt.setAttribute('colonia',value.colonia);
                    opt.setAttribute('municipio',value.municipio);
                    opt.setAttribute('codigo_postal',value.codigo_postal);
                    opt.setAttribute('correo_electronico',value.correo_electronico);
                    opt.setAttribute('estado',value.estado);
                    opt.setAttribute('rfc',value.rfc);
                    opt.setAttribute('nombre_completo',(((value.apellido_paterno != null)?value.apellido_paterno.toUpperCase()+' ':'') + ((value.apellido_materno != null)?value.apellido_materno.toUpperCase()+' ':'') + ((value.nombre != null)?value.nombre.toUpperCase()+' ':'')));
                    opt.innerHTML = value.rfc ;
                    select.appendChild(opt);
                });
                $('select#cliente_id').select2({
                    theme: 'bootstrap4',
                    templateResult: APP.formatState
                });
                $("select#cliente_id").val('').trigger('change') ;

                $('select#cliente_id').on('change', function(e) {
                    var data = $(this).select2('data');
                    var element = data[0].element;
                    $('input#rfc').val( $(element).attr('rfc') );
                    $('input#nombre').val( $(element).attr('nombre') );
                    $('input#apellido1').val( $(element).attr('apellido1') );
                    $('input#apellido2').val( $(element).attr('apellido2') );

                    $('input#telefono').val( $(element).attr('telefono') );
                    $('textarea#domicilio').val( $.trim($(element).attr('direccion') + ' ' +$(element).attr('numero_int') +' '+ (($(element).attr('numero_ext') != '')? 'ext. '+$(element).attr('numero_ext') :'') + ', '+$(element).attr('colonia')+', '+$(element).attr('municipio')+ ', '+$(element).attr('estado') ));
                    $('input#correo_electronico').val( $(element).attr('correo_electronico').toLowerCase() );
                });
            }
        });
    }

    cargar_nomenclaturas_polizas(){
        var select = document.getElementById('nomenclatura_id');

        $.ajax({
            type: 'get',
            url:  PATH+'/polizas/api/nomenclaturas.json',
            dataType: "json",
            success: function(data, status, xhr){
                $.each(data.data, function( index, value ) {
                    var opt = document.createElement('option');
                    opt.value = value.id;
                    opt.innerHTML = value.descripcion.toUpperCase();
                    select.appendChild(opt);
                });
                $('select#nomenclatura_id').select2({
                    theme: 'bootstrap4',
                    formatResult: APP.formatNomenclaturas,
                    templateResult: APP.formatNomenclaturas
                });
                $("select#nomenclatura_id").val('').trigger('change');
            }
        });
    }

    guardar(){
        $('small.form-text.text-danger').html('');
        setTimeout(() => {
            $.ajax({
                type: 'post',
                url: PATH+'/cheques/api/agregar_persona.json',
                data: $('form#data_form').serializeArray(),
                dataType: "json",
                success: function(data, status, xhr){
                    
                    if (data.status != "error") {
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito!',
                            text: 'El registro se ha guardado correctamente',
                        }).then((result) => {
                            $("body").LoadingOverlay("show");
                            setTimeout(function () {
                                window.location.replace(PATH+"/cheques/personas?id="+data.data.cheque_id);
                            }, 250)
                        });
                    } else {
                        $.each(data.message, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    }

                }
            });
        },500);
        
    }
}
  
  const APP = new CTRL_APP();