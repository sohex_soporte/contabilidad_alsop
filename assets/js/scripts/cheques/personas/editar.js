class CTRL_APP {
    constructor () {
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    formatMoney(money){
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    guardar(){

        var data_save = $('form#data_form').serializeArray();
        data_save.push({ name: "identificador", value: identificador });

        $('small.form-text.text-danger').html('');
        setTimeout(() => {
            $.ajax({
                type: 'post',
                url: PATH+'/cheques/api/editar_persona.json',
                data: data_save,
                dataType: "json",
                success: function(data, status, xhr){
                    console.log(data);
                    if (data.status != "error") {
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito!',
                            text: 'El registro se ha guardado correctamente',
                        }).then((result) => {
                            $("body").LoadingOverlay("show");
                            setTimeout(function () {
                                window.location.replace(PATH+"/cheques/personas?id="+data.data.cheque_id);
                            }, 250)
                        });
                    } else {
                        $.each(data.message, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    }

                }
            });
        },500);
        
    }
}
  
  const APP = new CTRL_APP();