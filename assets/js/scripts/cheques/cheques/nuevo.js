class CTRL_APP {
    constructor () {
    //   this.cargar_tabla();
        var _this = this;

      this.cargar_nomenclaturas_polizas();

        $('select#tipo_persona_id').select2({
            theme: 'bootstrap4'
        });
        $("select#tipo_persona_id").val('').trigger('change') ;
        $("select#tipo_persona_id").attr('onchange','APP.cargar_listado_rfc()');
        
        $('select#cliente_id').select2({
            theme: 'bootstrap4',
            templateResult: _this.formatState
        });
        $("select#cliente_id").val('').trigger('change') ;

    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    formatMoney(money){
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla(){
        let table =$('table#tabla_listado').DataTable();
        table.ajax.reload( null, false );
    }

    formatState(data) {
        var element = data.element;
        var html = '<small><b>'+data.text+'</b></small>';
        html += '<br/><span>'+$(element).attr('nombre_completo')+'</span>';
        return $(html);
    }

    formatNomenclaturas(data) {
        console.log(data);
        var html = '<small><b>'+data.id+'</b></small>';
        html += '<br/><span>'+data.title+'</span>';
        return $(html);
    }

    cargar_listado_rfc(){
        var tipo_id = $('select#tipo_persona_id option:selected').val();
        var url = '';
        switch (tipo_id) {
            case 1:
            case '1':
                //Cliente
                url = PATH+'/cheques/api/listado_clientes.json';
                break;

            case 2:
            case '2':
                //Proveedor
                url = PATH+'/cheques/api/listado_proveedores.json';
                break;
        }
        this.cargar_clientes_dms(url);
        
    }

    cargar_clientes_dms(url){
       
        $("select#cliente_id").removeAttr('onchange');

        $('select#cliente_id').find('option').remove();
        $('input.form-control-plaintext').val('');
        $('textarea.form-control-plaintext').val('');
        var select = document.getElementById('cliente_id');

        $.ajax({
            type: 'get',
            url: url,
            dataType: "json",
            success: function(data, status, xhr){
                //$('select#cliente_id').
                $.each(data.data, function( index, value ) {
                    var opt = document.createElement('option');
                    opt.value = value.id;
                    // opt.setAttribute('numero_cliente',value.numero_cliente);
                    opt.setAttribute('nombre',value.nombre);
                    opt.setAttribute('apellido1', (value.apellido_paterno != null)? value.apellido_paterno : '' );
                    opt.setAttribute('apellido2', (value.apellido_materno != null)? value.apellido_materno : '' );
                    opt.setAttribute('telefono', (value.telefono != null)? value.telefono : '');
                    opt.setAttribute('direccion', (value.direccion != null)? value.direccion : '' );
                    opt.setAttribute('numero_int', (value.numero_int != null)? value.numero_int : '');
                    opt.setAttribute('numero_ext', (value.numero_ext != null)? value.numero_ext  : '');
                    opt.setAttribute('colonia',value.colonia);
                    opt.setAttribute('municipio',value.municipio);
                    opt.setAttribute('codigo_postal',value.codigo_postal);
                    opt.setAttribute('correo_electronico',(value.correo_electronico != null)? value.correo_electronico : '');
                    opt.setAttribute('estado',value.estado);
                    opt.setAttribute('rfc',value.rfc);
                    opt.setAttribute('origen',value.origen);
                    opt.setAttribute('nombre_completo',(((value.apellido_paterno != null)?value.apellido_paterno.toUpperCase()+' ':'') + ((value.apellido_materno != null)?value.apellido_materno.toUpperCase()+' ':'') + ((value.nombre != null)?value.nombre.toUpperCase()+' ':'')));
                    opt.innerHTML = value.rfc ;
                    select.appendChild(opt);
                });

                $("select#cliente_id").val('').trigger('change') ;
                $("select#cliente_id").attr('onchange','APP.mostrar_informacion()');
                
                // $('select#cliente_id').on('change', function(e) {
                   
                // });
            }
        });
    }

    mostrar_informacion(){
        var data = $("select#cliente_id").select2('data');
        var element = data[0].element;
        $('input#rfc').val( $(element).attr('rfc') );
        $('input#nombre').val( $(element).attr('nombre') );
        $('input#apellido1').val( $(element).attr('apellido1') );
        $('input#apellido2').val( $(element).attr('apellido2') );

        $('input#telefono').val( $(element).attr('telefono') );
        $('input#correo_electronico').val( $(element).attr('correo_electronico').toLowerCase() );

        if($(element).attr('origen') == 'DMS'){
            $('textarea#domicilio').val( $.trim($(element).attr('direccion') + ' ' +$(element).attr('numero_int') +' '+ (($(element).attr('numero_ext') != '')? 'ext. '+$(element).attr('numero_ext') :'') + ', '+$(element).attr('colonia')+', '+$(element).attr('municipio')+ ', '+$(element).attr('estado') ));
        }else{
            $('textarea#domicilio').val( $.trim($(element).attr('direccion') ));
        }
    }

    cargar_nomenclaturas_polizas(){
        var select = document.getElementById('nomenclatura_id');

        $.ajax({
            type: 'get',
            url:  PATH+'/polizas/api/nomenclaturas.json',
            dataType: "json",
            success: function(data, status, xhr){
                $.each(data.data, function( index, value ) {
                    var opt = document.createElement('option');
                    opt.value = value.id;
                    opt.setAttribute('title',value.descripcion.toUpperCase());
                    opt.innerHTML = '<b>[ '+value.id+' ]</b> - '+value.descripcion.toUpperCase();
                    select.appendChild(opt);
                });
                $('select#nomenclatura_id').select2({
                    theme: 'bootstrap4',
                    formatResult: APP.formatNomenclaturas,
                    templateResult: APP.formatNomenclaturas
                });
                $("select#nomenclatura_id").val('').trigger('change');
            }
        });
    }

    guardar(){
        $('small.form-text.text-danger').html('');
        setTimeout(() => {
            $.ajax({
                type: 'post',
                url: PATH+'/cheques/api/agregar_cheque',
                data: $('form#data_form').serializeArray(),
                dataType: "json",
                success: function(data, status, xhr){

                    if (data.status == "success") {
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito!',
                            text: 'El registro se ha guardado correctamente',
                        }).then((result) => {
                            $("body").LoadingOverlay("show");
                            setTimeout(function () {
                                window.location.replace(PATH+"/cheques/asientos?id="+data.data.cheque_id);
                            }, 250)
                        });
                    } else {
                        $.each(data.message, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    }

                }
            });
        },500);
        
    }
}
  
  const APP = new CTRL_APP();