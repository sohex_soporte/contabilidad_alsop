class CTRL_APP {
    constructor() {
        this.renglon_id = null;
        // this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */
        var zero = "0"; /* String de cero */

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }

    formatMoney(money) {
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla() {
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    formatState(data) {
        var element = data.element;
        var html = '<small><b>' + data.text + '</b></small>';
        html += '<br/><span>' + $(element).attr('desc') + '</span>';
        return $(html);
    }

    formatNomenclaturas(data) {
        var html = '<small><b>' + data.id + '</b></small>';
        html += '<br/><span>' + data.text + '</span>';
        return $(html);
    }

    modal_agregar_asiento() {

        $.ajax({
            type: 'get',
            url: PATH + '/cheques/api/modal_agregar_asiento.json',
            data: {
                transaccion_id: transaccion_id
            },
            success: function (data, status, xhr) {
                $('div#modal_agregar_asiento div.modal-body').html(atob(data.data.html));
                $('div#modal_agregar_asiento').modal();
                setTimeout(() => {

                    $('#modal_agregar_asientoLabel').html('Agregar asiento');
                    $('#modal_agregar_asiento_button').attr('onclick','APP.modal_agregar_asiento_guardar();');

                    $('select#modal_cuentas').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento'),
                        templateResult: APP.formatState
                    });
                    $("select#modal_cuentas").val('').trigger('change');

                    $('select#modal_tipo').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento')
                    });
                    $("select#modal_tipo").val('').trigger('change');

                    try {
                        $('textarea#concepto').html(data.data.asiento.concepto);
                    } catch (error) {
                    }


                    // $('select#tipo_unidad').select2({
                    //     theme: 'bootstrap4',
                    //     dropdownParent: $('div#modal_agregar_asiento'),
                    //     templateResult: APP.formatState
                    // });
                    // $('select#tipo_unidad option[value=act]').prop('selected', true);
                    // $("select#tipo_unidad").trigger('change');

                }, 250);
            },
        });
    }

    modal_editar_asiento(_this) {

        APP.renglon_id = $(_this).data('renglon_id');

        $.ajax({
            type: 'get',
            url: PATH + '/cheques/api/modal_editar_asiento.json',
            data: {
                renglon_id: $(_this).data('renglon_id'),
            },
            success: function (data, status, xhr) {
                $('div#modal_agregar_asiento div.modal-body').html(atob(data.data.html));
                $('div#modal_agregar_asiento').modal();


                setTimeout(() => {

                    $('#modal_agregar_asientoLabel').html('Editar asiento');
                    $('#modal_agregar_asiento_button').attr('onclick','APP.modal_editar_asiento_guardar();');

                    $('select#modal_cuentas').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento'),
                        templateResult: APP.formatState
                    });

                    $('select#modal_tipo').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento')
                    });

                    try {
                        $("div#modal_agregar_asiento select#modal_cuentas option[value="+data.data.asiento.cuenta+"]").prop('selected',true);
                        $("div#modal_agregar_asiento select#modal_cuentas").trigger('change');
                    } catch (error) {
                        
                    }

                    try {
                        console.log(data);
                        if(data.data.asiento.cargo > 0){
                            $("div#modal_agregar_asiento select#modal_tipo option[value=1]").prop('selected',true);
                        }else{
                            $("div#modal_agregar_asiento select#modal_tipo option[value=2]").prop('selected',true);
                        }
                        $("div#modal_agregar_asiento select#modal_tipo").trigger('change');
                    } catch (error) {
                    }

                    try {
                        if(data.data.asiento.cargo > 0){
                            $("div#modal_agregar_asiento input#precio").val(data.data.asiento.cargo);
                        }else{
                            $("div#modal_agregar_asiento input#precio").val(data.data.asiento.abono);
                        }
                    } catch (error) {
                        
                    }

                    try {
                        $("div#modal_agregar_asiento  #concepto").val(data.data.asiento.concepto);
                    } catch (error) {
                        
                    }


                    // $('select#tipo_unidad').select2({
                    //     theme: 'bootstrap4',
                    //     dropdownParent: $('div#modal_agregar_asiento'),
                    //     templateResult: APP.formatState
                    // });
                    // $('select#tipo_unidad option[value=act]').prop('selected', true);
                    // $("select#tipo_unidad").trigger('change');

                }, 250);
            },
        });
    }

    cancelar_asiento($this) {

        var id = $($this).data('renglon_id');

        $.ajax({
            type: 'get',
            url: PATH + '/asientos/api/aplicar_asiento',
            data: {
                'asiento_id': id,
                'estatus': 'CANCELAR'
            },
            success: function (data, status, xhr) {
                
            },
        });
    }

    modal_agregar_asiento_guardar() {
        var data_send = $('form#data_form_modal').serializeArray();
        data_send.push({
            name: "polizas_id",
            value: polizas_id
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });

        $('small.form-text.text-danger').html('');
        $.ajax({
            type: 'post',
            url: PATH + '/cheques/api/modal_agregar_asiento.json',
            data: data_send,
            success: function (data, status, xhr) {
                if (data.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            location.reload();
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function(index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }
            },
        });
    }

    modal_editar_asiento_guardar() {
        var data_send = $('form#data_form_modal').serializeArray();
        data_send.push({
            name: "polizas_id",
            value: polizas_id
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });
        data_send.push({
            name: "renglon_id",
            value: APP.renglon_id
        });

        $('small.form-text.text-danger').html('');
        $.ajax({
            type: 'post',
            url: PATH + '/cheques/api/modal_editar_asiento.json',
            data: data_send,
            success: function (data, status, xhr) {
                if (data.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            location.reload();
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function(index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }
            },
        });
    }

    calcular_total() {
        try {
            var precio_unitario = $('input#precio_unitario').val();
            var cantidad = $('input#cantidad').val();
            $('input#precio').val( precio_unitario * cantidad);    
        } catch (error) {
            $('input#precio').val(0);
        }
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: {
                "url": PATH_LANGUAGE
            },
            responsive: true,
            pageLength: 50,
            'ajax': {
                'url': PATH + 'asientos/api/detalle',
                'data': function () {
                    return {
                        'transaccion_id': transaccion_id
                    };
                },
                'type': 'get'
            },
            'order': [],
            'columns': [{
                    title: 'No. Asiento',
                    data: 'asiento_id',
                    className: 'text-left',
                    render: function (data, type, row) {
                        return APP.zfill(parseInt(data), 6);
                    },
                },
                {
                    title: 'Concepto',
                    data: 'concepto'
                },
                {
                    title: 'Cuenta',
                    render: function (data, type, row) {
                        return '' + row.cuenta + '<br/><small><b>' + row.cuenta_descripcion + '</b></small>';
                    }
                },
                // {
                //     title: 'Precio unitario',
                //     data: 'precio_unitario',
                //     className: 'text-right',
                //     render: function (data, type, row) {
                //         return (data > 0) ? APP.formatMoney(data) : '';
                //     }

                // },
                // {
                //     title: 'Cantidad',
                //     data: 'cantidad_articulos',
                //     className: 'text-right',
                //     render: function (data, type, row) {
                //         return (data > 0) ? data : '';
                //     }

                // },
                {
                    title: 'Cargo',
                    data: 'cargo',
                    className: 'text-right',
                    render: function (data, type, row) {
                        return (data > 0) ? APP.formatMoney(data) : '-';
                    }

                },
                {
                    title: 'Abono',
                    data: 'abono',
                    className: 'text-right',
                    render: function (data, type, row) {
                        return (data > 0) ? APP.formatMoney(data) : '-';
                    }
                },
               
                // { 
                //     title: 'Acumulado general',
                //     data: 'acumulado',
                //     className: 'text-right',
                //     render: function ( data, type, row ) {
                //         //return (data >= 0)? '<span class="text-success float-left"><i class="fas fa-plus-square"></i></span>'+APP.formatMoney(data) :'<span class="text-danger float-left"><i class="fas fa-minus-square"></i></span>'+(APP.formatMoney(data*-1));
                //         return (data >= 0)? ''+APP.formatMoney(data) :'<span class="text-danger float-left"><i class="fas fa-minus-square"></i></span>'+(APP.formatMoney(data*-1));
                //     }  
                // },
                {
                    title: 'Estatus',
                    data: 'estatus_id',
                    render: function (data, type, row) {
                        var html = '';
                        switch (data) {
                            case 'APLICADO':
                                html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                            case 'ANULADO':
                                html = '<h6><span class="badge badge-secondary">Anulado</span></h6>';
                                break;
                            case 'POR_APLICAR':
                                html = '<h6><span class="badge badge-primary">Por aplicar</span></h6>';
                                break;
                            default:
                                html = '<h6><span class="badge badge-warning">Cancelado</span></h6>';
                                //html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                        }
                        return html;
                    },
                },
                {
                    title: 'Fecha de registro',
                    data: 'fecha_registro',
                    render: function (data, type, row) {
                        var fecha = moment(data, 'YYYY-MM-DD HH:mm:ss.SSSSSS');
                        var html = '';
                        if (fecha.isValid()) {
                            html = '<center>' + fecha.format('HH:mm:ss') + '<br/><small><b>' + fecha.format('DD/MM/YYYY') + '</b></small></center>';
                        }
                        return html;
                    },
                },
                // {
                //     title: 'Acciones',
                //     width: '140px',
                //     render: function (data, type, row) {
                //         let cancelar = '';
                //         cancelar = '<button title="Cancelar" data-renglon_id= "' + row.asiento_id + '"  onclick="APP.cancelar_asiento(this)" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                //         return '<center>' + cancelar + '</center>';
                //     }
                // }
            ]
        });
    }

    eliminar_renglon(_this) {
        Swal.fire({
            title: 'Desea eliminar el asiento?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            denyButtonText: 'Cancelar',
        }).then((result) => {
            console.log(result);
            if (result.value == true) {
                $.ajax({
                    url: PATH + '/asientos/api/aplicar_asiento',
                    type: 'POST',
                    data: {
                        asiento_id: $(_this).data('renglon_id'),
                        estatus: "ANULADO"
                    },
                    success: function(response) {
                        if (response.estatus != "error") {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito!',
                                text: response.mensaje,
                            }).then((result) => {
                                $("body").LoadingOverlay("show");
                                setTimeout(function () {
                                    location.reload();
                                }, 250)
                            });

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.mensaje,
                            })
                        }
                    },
                });
            } else {
                return false;
            }
        })
    }
    
    // eliminar_operacion(_this) {
    //     Swal.fire({
    //         title: 'Desea eliminar la operación?',
    //         showDenyButton: true,
    //         showCancelButton: false,
    //         confirmButtonText: 'Aceptar',
    //         denyButtonText: 'Cancelar',
    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             $.ajax({
    //                 url: PATH + '/administrador/api/reportes/delete_operacion',
    //                 type: 'POST',
    //                 data: {
    //                     operacion_id: $(_this).data('operacion_id'),
    //                 },
    //                 success: function(response) {
    //                     if (response.estatus != "error") {
    //                         Swal.fire({
    //                             icon: 'success',
    //                             title: 'Éxito!',
    //                             text: response.mensaje,
    //                         }).then((result) => {
    //                             $("body").LoadingOverlay("show");
    //                             setTimeout(function () {
    //                                 location.reload();
    //                             }, 250)
    //                         });

    //                     } else {
    //                         Swal.fire({
    //                             icon: 'error',
    //                             title: 'Error',
    //                             text: response.mensaje,
    //                         })
    //                     }
    //                 },
    //             });
    //         } else {
    //             return false;
    //         }
    //     })
    // }

}

const APP = new CTRL_APP();